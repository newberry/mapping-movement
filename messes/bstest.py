from bs4 import BeautifulSoup as BSoup
import json


with open('_mains/altered-around-over-through.txt') as webpage:
    html = webpage.read()
    soup = BSoup(html, 'html.parser')

outputObj = {}

all_hone = soup.find_all('h1')
outputObj['title'] = all_hone[0].text

all_bodyt = soup.find_all('p', {'class':'body-text'})
outputObj['bodyt'] = []
for abt in all_bodyt:
    outputObj['bodyt'].append(abt.text)

# p class="author
author = soup.find_all('p', {'class':'author'})
outputObj['author'] = []
for a in author:
    if len(a.text) > 0 :
        print(a.span)
    if a.text != '\n' and a.text != 'by:':
        outputObj['author'].append(a.span.text)

jsonString = json.dumps(outputObj, indent=4)
jsonFile = open("obj.json", "w")
jsonFile.write(jsonString)
jsonFile.close()


# body_text = soup.find_all('p', class_="body-text")
# author = soup.find_all

# print(str(len(body_text)))
# print(str(len(body_text)))