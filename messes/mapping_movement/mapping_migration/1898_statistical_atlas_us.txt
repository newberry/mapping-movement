Statistical Atlas of the United States, 1898
Referenced by Essay: 
Mapping Migration and Settlement
Although there were a few attempts to prepare statistical maps based on the 1860 US Census (Caption 5), the first atlas devoted exclusively to mapping census data started with the 1870 census. Similar, but more comprehensive atlases appeared for the 1880 and 1890 censuses. The latter, noted for its use of innovative graphic presentations such as bar graphs, pie diagrams, and population pyramids, is the most interesting of the three. It was published in 1898 under the direction of Henry Gannett, geographer of both the US Census Office and Geological Survey (Dahmann).
     Especially interesting are the thematic maps and statistical graphs presenting demographic data pertaining to the 1890 immigrant or foreign born population. These include a map showing the distribution of the total foreign born population, as well as individual maps depicting the spatial distributions of foreign born immigrants from Canada, Great Britain, Ireland, Scandinavia, and the Germanic states.
     Using the Germans as an example, it is possible to illustrate the broad patterns of their migration experience during the nineteenth century. They started to come to the US in large numbers in the 1830s and 1840s, and for each decade from 1850 to 1890, they were the leading immigrant group. The 1890 distribution, as presented on Plate 20, depicts the distribution of Germans by both percentage of total population and density. In general terms, the two distributions are similar with fairly dense population concentrations extending in a wide belt from New York and Pennsylvania in the east to North and South Dakota, Nebraska, and Kansas in the central portion of the country. Smaller pockets are found in Texas and California. Throughout the area, the Germans lived in both rural and urban settlements. However, the Germans are conspicuously absent from the southern states which they avoided because of their opposition to slavery and the lack of economic opportunity before the Civil War (Allen and Turner 1988, 50-55).
     The atlas also includes other innovative graphics characterizing the immigrant population. For example, Plate 10 displays a series of pie diagrams showing percentage of foreign born by state. Germans, who were color coded with black, appear to have larger percentages than other immigrant groups in Wisconsin, Ohio, Indiana, Illinois, Kentucky, and Missouri.  Other immigrant groups, that have the largest percentages in their respective states include Irish (green) in Massachusetts, Rhode Island, Connecticut, and Delaware; Scandinavians (blue) in Minnesota, North Dakota, and South Dakota, and Canadians in Maine, New Hampshire, and Vermont.
     Another innovative graphic is Plate 43, which presents a series of bar graphs showing occupations subdivided by color and nationality. Native white of native parentage are color coded with light blue; native white of foreign parentage are coded with dark blue. “colored” with dark pink; and foreign white coded with light pink. Germans, who are coded as #3 in the light pink category, have the largest percentages in the occupations of butchers, cigar makers, and tailors.
 

Selection Gallery
Statistical Atlas of the United States, 1898
Citation:
Gannett, Henry, "Density of Distribution of the Natives of the Germanic Nations, 1890 ; Proportion of the Natives of the Germanic Nations to the Aggregate Population, 1890", in Statistical Atlas of the United States : Based Upon Results of the Eleventh Census (Washington : Government Printing Office, 1898), plate 20. Oversize Govt. I 12.2 :At6


View Full Metadata
Statistical Atlas of the United States, 1898
Citation:
Gannett, Henry, "Composition of the Foreign-Born Population, 1890" in Statistical Atlas of the United States : Based Upon Results of the Eleventh Census (Washington : Government Printing Office, 1898), plate 16.Oversize Govt. I 12.2 :At6


View Full Metadata
Statistical Atlas of the United States, 1898
Citation:
Gannett, Henry, "Distribution of Those Engaged in Certain Selected Occupations, by Color and Nationality, 1890" in Statistical Atlas of the United States : Based Upon Results of the Eleventh Census (Washington : Government Printing Office, 1898), plate 43.Oversize Govt. I 12.2 :At6