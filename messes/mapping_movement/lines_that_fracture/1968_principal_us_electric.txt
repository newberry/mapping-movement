Principal US Electric Transmission Lines, 1968
Referenced by Essay: 
Lines that Fracture and Fade: Two Centuries of Travel through Georgia, 1775-1976
The Rand McNally Corporation compiled this rolled wall map showing the nation’s major powerlines and the regional systems of that operated them. At first appearance, it appears to be a wall-mounted road map of the continental US In 1967. (And Rand McNally) clearly drew upon its experience with road maps to create this image of a linked system of corridors defined by varying degrees of carrying capacity.) Given the rapidity with which America’s power infrastructure changed in the twentieth century, it is likely this map did not long serve its purpose, but it offers an intriguing glimpse at the ways in which US history grew alongside the movement of non-human elements (in this case electrons along high tension wires).

Selection Gallery
Principal US Electric Transmission Lines, 1968
Citation:
Edison Electric Institute, "Principal Interconnected Transmission Lines in the United States" (New York : Edison Electric Institute, [1968]). Maproll G3701.N4 1968 .E3