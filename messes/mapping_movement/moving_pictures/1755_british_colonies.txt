British Colonies in America, 1755
Referenced by Essay: 
Moving Pictures: Maps and Imagination in Eighteenth-Century Anglo-America
Selection Gallery
British Colonies in America, 1755
Citation:
Evans, Lewis, "A General Map of the Middle British Colonies, in America : viz Virginia, Maryland, Delaware, Pensilvania, New-Jersey, New-York, Connecticut, and Rhode Island" (Philadelphia : Lewis Evans, 1755), detached from Geographical, Historical, Political, Philosophical and Mechanical Essays (Philadelphia : Printed by B. Franklin, and D. Hall, 1755). Ayer map4F G3790 1755 .E9