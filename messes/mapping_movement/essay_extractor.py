import json, os, re

outputArray = []

# txtfile = 'amercan_railroad_maps_1828_1876/1828_survey_boston.txt'
rootdir = './'

maybeheaders = []

def funkify(val):
    return val.replace(".txt", "").replace("_MAIN_","").replace("_","-")

for subdir, dirs, files in os.walk(rootdir):
    for txtfile in files:
        essay = {
            "title": '',
            "author": '',
            "path": [],
            "essay": []
        }
        # print(os.path.join(subdir, file))
        print(txtfile)
        with open(os.path.join(subdir, txtfile)) as file:
            if "_MAIN_" in txtfile:
                # essay["path"]  = os.path.join(subdir, txtfile)
                essay["path"]  = [funkify(subdir), funkify(txtfile)]
                for idx, line in enumerate(file):
                    line = line.strip()
                    if line == "Bibliography:" or line == "Further Reading" or line == "Essay Gallery":
                        break
                    if idx == 0: 
                        essay["title"] = line
                # else if "Referenced by Essay" in line:
                    elif idx == 1:
                        essay["author"] = line.replace("by: ", "")
                    elif len(line) > 0:
                        if line[-1:] not in [".",")","\u201d","?",":"] and not re.match(r'([0-9]{1,3})\.',line):
                            maybeheaders.append(line)
                            essay["essay"].append([idx, line, 'h3'])
                        else: 
                            essay["essay"].append([idx, line, 'p'])
                outputArray.append(essay)
                # if essay["path"][0][2:] != essay["path"][1]:
                #     pathkey.append(essay["path"])

with open('maybeheaders.json', 'w') as f:
    json.dump(maybeheaders, f, indent=4)
with open('data_essay.json', 'w') as f:
    json.dump(outputArray, f, indent=4)