regexes-for-messmaking.txt

^(.+)\nReferenced by Essay: \n(.+)$
<h1 class="gallery-page-title">\n\t$1\n</h1>\n<h2 class="parent-essay-title">\n\t$2\n</h2>\n<div class="main-text">\n
<h1 class="gallery-page-title">$1</h1>\n<h2 class="parent-essay-title">$2</h2>\n<div class="main-text">\n


^Selection Gallery$
</div>

\n\n
\n

^(.*?)\nCitation:\n(.*?)$
\n<h3 class="map-title">$1</h3>\n<cite>$2</cite>
\n<h3 class="map-title">\n\t$1\n</h3>\n<cite>\n\t$2\n</cite>

<div class="main-text">(.+)$
<div class="main-text">\n$1


^([^<].+)$
<p class="main-text-p">$1</p>

<p class="body-text">$1</p>

<p class="main-text-p">( {1,100})


MAINSSSSSSSSSSS_______________________

^(.+)\nby: ?(.+)\n
<h1>$1</h1>\n<em>by: </em><p class="author">$2</p>\n\n

class="author">(.+) WITH (.+)</p>
class="author"$1</p> <em>with</em> <p class="author">$2</p>

^Bibliography:? ?$
<h4>Bibliography</h4>

\n\n([A-Z])(.+)\n([A-Z])(.+)\n
\n\n<h2>$1$2</h2>\n<p class="body-p">$3$4</p>\n

periphera"(.+)</p>\n\n([A-Z])(.+)\n
periphera"$1</p>\n<p class="periphera">$2$3</p>\n


\n\n([A-Z])(.+)\n([A-Z])(.+)\n
\n\n<section>\n<h2 class="section-header">$1$2</h2>\n<p class="body-text">$3$4</p>\n


=========================

==-\n(.+).txt\n(.+)\nReferenced by Essay: \n(.+)\n
==-\n{\n\t"textfile":"$1.txt",\n\t"title":"$2",\n\t"parent":"$3",\n

\nSelection Gallery\n(.+)\nCitation:\n(.+)\n
\n\t"maps": [\n\t{\n\t\t"maptitle":"$1",\n\t\t"mapsource":"$2"\n\t},\n

"parent":"(.+)",\n([A-Za-z])(.+)\n
"parent":"$1",\n\t"text":[\n\t\t"$2$3",\n

"text": \[\n\t\t"(.+)",\n([ ]{1,15})([^"])(.+)\n
"text": [\n\t\t"$1",\n\t\t"$3$4",\n


"text": \[\n\t\t"(.+)",\n([ ]{1,15})"(.+)",\n([ ]{1,15})([^"])(.+)\n
"text":[\n\t\t"$1",\n\t\t"$3",\n\t\t"$5$6",\n

                View Full Metadata\n                (.+)\n                Citation:\n                (.+)\n