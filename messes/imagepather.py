import json
f = open('src/data/image_mapping.json')
# querystring
# iiifId
# cortexId

data = json.load(f)
output_list = []
for essay in data:
    for map in essay["maps"]:
        if "path" in map:
            pathh = map["path"]
            otputdict = {
                "path": pathh,
                "imagefn": map["image_filename"]
            }
            output_list.append(otputdict)


with open('./imagemaps.json', 'w') as mf:
    json.dump(output_list, mf , indent=4) 