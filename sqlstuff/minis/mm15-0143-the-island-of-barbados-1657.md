mm15-0143-the-island-of-barbados-1657.md
﻿---
title: The Island of Barbados, 1657
parent: Maps, Movement, and American Literature
parentslug: maps-movement-and-american-literature
---

Throughout the seventeenth century, many maps gave the impression that America was not a continent in the tectonic sense of the term but a loosely configured archipelago of islands ranging in size from Barbados to South America. This kind of perspective was the result of mapmakers who drafted maps to reflect the economic reality of transatlantic traffic patterns, trade routes, and trading posts. Select overview maps connecting the Eastern with the Western hemispheres specialized in the depiction of the trade triangle in which European ships traveled to African markets with manufactured goods; these goods were traded for purchased or kidnapped people from Africa; the African prisoners were then transported across the Atlantic as slaves (the infamous “middle passage”); once in America, the slaves were then sold or traded for raw materials, which would be transported back to Europe to complete the triangular cycle of transatlantic commerce.  
    Having lost his fortune during the English Civil Wars, Richard Ligon (1585?-1662) left England in 1648 to gain his fortune in the New World where he spent two years on the Caribbean island of Barbados, one of America’s most profitable colonies in the sugar trade. While the narrative of Ligon’s _History_ (1657) traced the triangular trade (consisting of English goods, slaves, and sugar), his large foldout map concentrated on the island itself. A reproduction of Captain John Swan’s 1638 map, Ligon’s version continued to identify nearly three hundred plantations by their owners’ names, the majority of which were located along the island’s coast.  
    Drawn in a pictographic style that echoed Smith’s map of New England, Ligon’s map suggested limited mobility within the island’s interior, which the map rendered optically inaccessible by showing forests, mountains, and wild beasts. Strategically placed vignettes depict planters hunting wild pigs or chasing runaway slaves. Surprisingly, Ligon also inserted a pair of camels on which he commented that “these are very useful beasts, but very few will live upon the Island; divers \[planters\] have had them brought over, but few know how to diet them.” Showing the lack of waterways or developed road systems, the map’s inclusion of images depicting four different types of sailing vessels reminded readers that the sea rather than land offered the better path of transportation.

Selection Gallery
-----------------

[The Island of Barbados, 1657](/web/20210130021227/https://mappingmovement.newberry.org/item/topographicall-description-and-admeasurement-yland-barbados-west-indyaes-mrs-names-seureall)

**Citation:**

Ligon, Richard, "A Topographicall Description and Admeasurement of the Yland of Barbados in the West Indyaes : With the Mrs. Names of the Seureall Plantacons", in _True & Exact History of the Island of Barbados_ (London : Printed for Humphrey Moseley, 1657), plate \[1\]. VAULT folio Ayer 1000.5 .B22 L7 1657

[![](https://web.archive.org/web/20210130021227im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_43.jpg?itok=8odnVSXn)](https://web.archive.org/web/20210130021227/https://mappingmovement.newberry.org/item/topographicall-description-and-admeasurement-yland-barbados-west-indyaes-mrs-names-seureall "The Island of Barbados, 1657")

[View Full Metadata](/web/20210130021227/https://mappingmovement.newberry.org/item/topographicall-description-and-admeasurement-yland-barbados-west-indyaes-mrs-names-seureall)
