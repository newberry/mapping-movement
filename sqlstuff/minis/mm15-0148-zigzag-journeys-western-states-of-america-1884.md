mm15-0148-zigzag-journeys-western-states-of-america-1884.md
﻿---
title: Zigzag Journeys, Western States of America, 1884
parent: Maps, Movement, and American Literature
parentslug: maps-movement-and-american-literature
---

The maps illustrating Hezekiah Butterworth’s (1839–1905) young adult fiction are diverse, ranging from travel maps to outline maps to archeological charts. In his _Zig-Zag Journeys in the Western States of America_ (1884), graphic designers used segments of railroad maps for illustrating the volume’s front- and back-papers. Railroad maps had a distinct look because they privileged information pertaining either to the railroad network or to a particular railroad company. Thus they showed stations, intersections, and depots but excluded other mappable transportation networks, such turnpikes and canals, not to mention topographical features like mountains or rivers. As shown by the decorative map segments, railroad maps also display transportation patterns of different density; while line markings document the various train tracks and circles the stations, their size and thickness distinguished primary from secondary train lines.  
    When compared to the rest of the endpapers’ illustrations, the map segments serve as visual substitutes for images showing actual train engines; all other modes of transportation are depicted in realistic drawings, including horse, oxcart, and steamboat. The book illustrator thus anticipated Butterworth’s narrative plot. On one hand, it emphasizes the speed of trains. On the other hand, it documents a rather unevenly paced journey across the west, jumping from episode to episode, as if imitating a train’s rushed journey along the prescribed path of railroad tracks, only to be interrupted by scheduled stops during which passengers (or readers) can examine and reflect on their environment. The illustrator’s choice of map segments furthermore resembles not only the ribbon design of nineteenth-century river maps but the cartographic views offered by today’s handheld electronic devices, such as a GPS device, both of which emphasize transportation routes over other geographic content.  
    Butterworth’s _Zig-Zag Journeys in the Western States of America_ was one of eighteen volumes published between 1879 and 1896, selling for a total of over 250,000 copies. Each volume addressed a young male audience; each volume emphasized a different mode of transportation; and equipped with a distinct cover design and title logo, each volume of the _Zig-Zag Journeys_ offered young adults fantasies of geographic mobility.

Selection Gallery
-----------------

[Zigzag Journeys, Western States of America, 1884](/web/20210303203735/http://mappingmovement.newberry.org/item/composite-illustration-united-states-views-and-railroad-map-segments)

**Citation:**

Butterworth, Hezekiah, "\[Composite Illustration of United States Views and Railroad Map Segments\]", in _Zi__gzag Journeys In the Western States of America : the Atlantic to the Pacific_ (London : Dean & Son, \[1884\]), front endpapers. G 835 .139

[![](https://web.archive.org/web/20210303203735im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_45.jpg?itok=sbEZiEDX)](https://web.archive.org/web/20210303203735/http://mappingmovement.newberry.org/item/composite-illustration-united-states-views-and-railroad-map-segments "Zigzag Journeys, Western States of America, 1884")

[View Full Metadata](/web/20210303203735/http://mappingmovement.newberry.org/item/composite-illustration-united-states-views-and-railroad-map-segments)
