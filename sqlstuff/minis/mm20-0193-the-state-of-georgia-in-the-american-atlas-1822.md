mm20-0193-the-state-of-georgia-in-the-american-atlas-1822.md
﻿---
title: The State of Georgia, in the American Atlas, 1822
parent: Lines that Fracture and Fade: Two Centuries of Travel through Georgia, 1775-1976
parentslug: lines-fracture-and-fade-two-centuries-travel-through-georgia-1775-1976
---

Matthew Carey was a Philadelphia print shop owner and one of the most active map publishers in the early republic, operating from 1785 onward. His first map was published in 1794 and his first _American Atlas_ in 1795 as a supplement to his edition of William Guthrie’s geography textbook. Carey turned his business over to his son Henry and son-in-law Isaac Lea in 1822, who turned away from cartographic publishing after Matthew’s retirement. This edition of the _Atlas_ therefore represents the end of Carey’s cartographic legacy and the transition from Matthew’s stewardship to his son’s.  
     This sheet, detached from the _Atlas_, presents a map of Georgia surrounded by historical, geographical and statistical information about the state. The map itself seems almost entirely copied (albeit simplified) from Daniel Sturges’s 1818 map of Georgia (unsurprising given that Sturges’s map was printed by Philadelphian John Melish and thus would have been known to the city’s printers and mapmakers). The text along the map’s edges names principal towns, political information, and economic data as well as some historical narrative. The goal here was to provide less ambiguous celebration of national expansion than even Sturges provided, directing the _Atlas’s_ users in all the ways to celebrate Georgia’s accomplishments as demonstrated on the map.  
 

Selection Gallery
-----------------

[The State of Georgia, in the American Atlas, 1822](/web/20210119184746/http://mappingmovement.newberry.org/item/georgia)

**Citation:**

Lucas, Fielding, Jr., "Georgia", in H.C. Carey & I. Lea (Firm). _Complete Historical, Chronological, and Geographical American Atlas_ (Philadelphia : H.C. Carey & I. Lea, 1822), no. 25. Greenlee 4891 .C27 1822

[![](https://web.archive.org/web/20210119184746im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_223.jpg?itok=BzyF8lVZ)](https://web.archive.org/web/20210119184746/http://mappingmovement.newberry.org/item/georgia "The State of Georgia, in the American Atlas, 1822")

[View Full Metadata](/web/20210119184746/http://mappingmovement.newberry.org/item/georgia)
