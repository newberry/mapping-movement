mm09-0083-charecke-nation-and-the-path-to-charles-town-1730.md
﻿---
title: Charecke Nation and the Path to Charles Town, 1730
parent: Moving Pictures: Maps and Imagination in Eighteenth-Century Anglo-America
parentslug: moving-pictures-maps-and-imagination-eighteenth-century-anglo-america
---

.

The original of Hunter’s untitled manuscript map is housed with the Library of Congress, cataloged under the title “This represents the Charecke Nation by Col. Herberts Map…” The Newberry houses a published version of the map, traced from the original document. The reproduction itself is a fascinating object, but it should be noted that the original contained two separate sets of annotations, one from Hunter and one from Governor James Glen. The reprint version makes it difficult to see which man wrote what.

Selection Gallery
-----------------

[Charecke Nation and the Path to Charles Town, 1730](/web/20210127193338/https://mappingmovement.newberry.org/item/represents-charecke-nation-col-herberts-map-my-own-observations-path-charles-town-its-course)

**Citation:**

Hunter, George, "This Represents the Charecke Nation by Col. Herberts Map & My Own Observations With the Path to Charles Town, its Course & (Distance Measured by My Watch) the Names of \[the\] Branches, Rivers & Creeks, as Given Them by \[the\] Traders Along that Nation ... May 21, 1730",  in _George Hunter’s Map of the Cherokee Country and the Path Thereto in 1730_ \[Columbia, S.C. : Printed for the Historical Commission of South Carolina by the State Company, 1917.\] Ayer map4F G3860 1730 .H8 1917

[![](https://web.archive.org/web/20210127193338im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_163.jpg?itok=_V7sH05C)](https://web.archive.org/web/20210127193338/https://mappingmovement.newberry.org/item/represents-charecke-nation-col-herberts-map-my-own-observations-path-charles-town-its-course "Charecke Nation and the Path to Charles Town, 1730")

[View Full Metadata](/web/20210127193338/https://mappingmovement.newberry.org/item/represents-charecke-nation-col-herberts-map-my-own-observations-path-charles-town-its-course)
