mm15-0150-road-map-of-the-southwestern-states.md
﻿---
title: Road Map of the Southwestern States
parent: Maps, Movement, and American Literature
parentslug: maps-movement-and-american-literature
---

A Rand McNally road map, like this one from 1954 showing the western states of Arizona and New Mexico, was an iconic object of US culture representing the rise of the automobile as the dominant mode of travel. Moreover, between the Second World War and the advent of electronic navigation systems (GPS), the Rand McNally road map emerged as a ubiquitous artifact:  Sold at gas stations or given out for free by automobile clubs, it informed everyday life while also signifying mobility from the social and professional to the personal and recreational. In design similar to nineteenth-century railroad maps, the road map prioritizes streets, roads, and highways. The roads are color-coded, numbered, and ranked according to levels of national importance from the federally funded highway to the local country lane. Here “red highways” stand for transregional traffic arteries connecting different states, while “blue highways” mark local road systems fanning across the countryside.  
    While copies of Rand McNally road maps were not included into copies of American literature, their presence is implied in fiction and biographies written between the 1920s and 1980s. The road map provides an implied narrative horizon in F. Scott Fitzgerald’s short story “The Diamond as Big as the Ritz” (1922) and his classic novel, _The Great Gatsby_ (1925). It informs the movements taken by the characters in John Dos Passos’s _Manhattan Transfer_ (1925) and acclaimed _USA Trilogy_ (1933-36). Its presence is felt in John Steinbeck’s _The Grapes of Wrath_ (1936) and its realistic depiction of the exodus of the “Okies” during the 1930s Dust Bowl. While the road map informed modernist novels exploring the theme of mobility in lives lived in luxury or poverty, its influence can be further felt in the works of the Beatnik generation, such as Jack Kerouac’s _On the Road_ (1957) and Tom Wolfe’s _The Electric Kool-Aid Acid Test_ (1968). But the road map’s most lasting influence lay in its power of inspiring countless road and camping trips undertaken by the American middle class—from the family vacation to the teenager’s existential journey across the continent—who, with map in hand, experienced the American landscape from behind windshields following models laid out by Steinbeck’s travelogue _Travels with Charlie: In Search of America_ (1968), Robert Pirsig’s _Zen and the Art of Motorcycle Maintenance: An Inquiry into Values_ (1974), and William Least Heat Moon’s _Blue Highways_: _A Journey into America_ (1982).

Selection Gallery
-----------------

[Road Map of the Southwestern States](/web/20210303215859/http://mappingmovement.newberry.org/item/texaco-touring-map-arizona-new-mexico)

**Citation:**

Rand McNally and Company, "Arizona-New Mexico with Colorado-Utah-Wyoming : Tour with Texaco" (Chicago, Illinois : Rand McNally & Company, \[1955?\]). Map4F G4331.P2 1955 .R3

[![](https://web.archive.org/web/20210303215859im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_188.jpg?itok=Dkd7bBd5)](https://web.archive.org/web/20210303215859/http://mappingmovement.newberry.org/item/texaco-touring-map-arizona-new-mexico "Road Map of the Southwestern States")

[View Full Metadata](/web/20210303215859/http://mappingmovement.newberry.org/item/texaco-touring-map-arizona-new-mexico)

[Road Map of the Southwestern States](/web/20210303215859/http://mappingmovement.newberry.org/item/texaco-touring-map-colorado-utah-wyoming)

**Citation:**

Rand McNally and Company, "Arizona-New Mexico with Colorado-Utah-Wyoming : Tour with Texaco" (Chicago, Illinois : Rand McNally & Company, \[1955?\]). Map4F G4331.P2 1955 .R3

[![](https://web.archive.org/web/20210303215859im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_189.jpg?itok=38-vMMCQ)](https://web.archive.org/web/20210303215859/http://mappingmovement.newberry.org/item/texaco-touring-map-colorado-utah-wyoming "Road Map of the Southwestern States")

[View Full Metadata](/web/20210303215859/http://mappingmovement.newberry.org/item/texaco-touring-map-colorado-utah-wyoming)
