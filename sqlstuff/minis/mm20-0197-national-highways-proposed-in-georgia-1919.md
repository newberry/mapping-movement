mm20-0197-national-highways-proposed-in-georgia-1919.md
﻿---
title: National Highways Proposed in Georgia, 1919
parent: Lines that Fracture and Fade: Two Centuries of Travel through Georgia, 1775-1976
parentslug: lines-fracture-and-fade-two-centuries-travel-through-georgia-1775-1976
---

Founded in 1912, the National Highways Association was a political lobbying group whose main objective was to secure a federal system of paved roads throughout the United States. Unlike other advocates of the “good roads” movement, the NHA believed that a scattered system of state paved roads was less serviceable than a federally-funded and -directed system of highways. (Akerman 2006, 177) The NHA’s state maps therefore have two main, somewhat contradictory, purposes: To present state road systems as hopelessly complex and fragmented and to demonstrate at the same time how easily a federal system could be overlaid atop the existing state infrastructure to give order to the chaos.  
     This political agenda is clear throughout the map, explicitly stated in the legends at the top and bottom and clearly a key part of the mapped features. Small dirt roads and old stage routes that possibly had not appeared on Georgia map since the Civil War now became visible again, even small dirt county roads. This map thus offers an incredible amount of visual detail on the roadways and travel routes of Georgia in the early twentieth century. The map also distinguishes between different types of paving surfaces, providing some sense of the actual experience of traveling through these state roads in the years before a federal highway system developed.  
 

Selection Gallery
-----------------

[National Highways Proposed in Georgia, 1919](/web/20210127223207/http://mappingmovement.newberry.org/item/national-highways-map-state-georgia-showing-thirty-three-hundred-miles-national-highways)

**Citation:**

National Highways Association, "National Highways Map of the State of Georgia" (Washington, D.C. : National Highways Association, c1919). RMcN AE 20.10

[![](https://web.archive.org/web/20210127223207im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_135.jpg?itok=ptjIbG1V)](https://web.archive.org/web/20210127223207/http://mappingmovement.newberry.org/item/national-highways-map-state-georgia-showing-thirty-three-hundred-miles-national-highways "National Highways Proposed in Georgia, 1919")

[View Full Metadata](/web/20210127223207/http://mappingmovement.newberry.org/item/national-highways-map-state-georgia-showing-thirty-three-hundred-miles-national-highways)
