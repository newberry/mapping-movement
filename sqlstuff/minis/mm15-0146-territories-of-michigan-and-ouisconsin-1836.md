mm15-0146-territories-of-michigan-and-ouisconsin-1836.md
﻿---
title: Territories of Michigan and Ouisconsin, 1836
parent: Maps, Movement, and American Literature
parentslug: maps-movement-and-american-literature
---

After moving to Michigan in 1821 to teach at the newly founded university, John Farmer (1798-1859) is today credited for having produced about a dozen maps responsible for promoting the settlement of Michigan between 1825 and 1840. Several generations of travelers plotted out routes and staked out land claims using his maps that presented the areas of Michigan, Wisconsin, Lake Superior, and the city of Detroit. Farmer’s _Improved Map of the Territories of Michigan and Ouisconsin_, applying a uniquely slanted orientation, provides a complete overview of the territories under Michigan’s jurisdiction while also anticipating Michigan’s impending statehood and separation from the western areas (which the map’s caption declares to “be shortly set off as Ouisconsin”).  
    What made Farmer’s map so appealing to prospective emigrants—including perhaps the writer Caroline Kirkland—is its detailed references to roads, waterways, and towns. These are supplemented by line markings showing the routes taken by the geologists, Henry Schoolcraft and Lewis Cass who explored the Lake Superior region, as well as Stephen Long’s route in search of the source of the Mississippi River. The prominent display of township and range lines, not to mention the coloring of counties, provides at least three impressions, namely that the map was a reliable wayfinding tool, that large parts of the Michigan territory were surveyed according to the federal Land Ordinance Acts, and that the rectangular land lots were recorded by the General Land Office (an independent agency of the US government created in 1812 and in charge of lands in the public domain).  
    Also of interest to emigrants traveling to the Michigan territory would have been the map’s information about the Native American population, and thus about prospective threats to economic activities and personal safety. Farmer’s map identifies native villages, Indian agencies, and abandoned trading posts. Like many earlier maps intended to promote regions in America, Farmer embellished the map with a little bit of speculative cartography. While not promising the location of a northwestern El Dorado, the map perpetuates the myth of rich mineral deposits, consisting of copper and lead, in the western valley of the Blue Earth River (near today’s Minneapolis, Minn.)

Selection Gallery
-----------------

[Territories of Michigan and Ouisconsin, 1836](/web/20210303215120/http://mappingmovement.newberry.org/item/improved-map-territories-michigan-and-ouisconsin-pronounced-wisconsin)

**Citation:**

Farmer, John, "Improved Map of the Territories of Michigan and Ouisconsin (Pronounced Wisconsin)" (\[New York : J.H. Colton & Co.\], 1836). VAULT map Graff 1290

[![](https://web.archive.org/web/20210303215120im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_186.jpg?itok=DsEPpRVD)](https://web.archive.org/web/20210303215120/http://mappingmovement.newberry.org/item/improved-map-territories-michigan-and-ouisconsin-pronounced-wisconsin "Territories of Michigan and Ouisconsin, 1836")

[View Full Metadata](/web/20210303215120/http://mappingmovement.newberry.org/item/improved-map-territories-michigan-and-ouisconsin-pronounced-wisconsin)
