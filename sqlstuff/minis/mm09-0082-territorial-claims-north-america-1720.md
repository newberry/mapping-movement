mm09-0082-territorial-claims-north-america-1720.md
﻿---
title: Territorial Claims, North America, 1720
parent: Moving Pictures: Maps and Imagination in Eighteenth-Century Anglo-America
parentslug: moving-pictures-maps-and-imagination-eighteenth-century-anglo-america
---

Moll’s map was a direct response to Guillame De Lisle’s _Carte Nouvelle de la Louisiana_, published in Paris in 1718. Moll, a German mapmaker working in England, took exception on behalf of his adopted country to De Lisle’s assertions of French dominion over all of North American west of the Appalachians and north of Spanish Florida. Moll reproduced much, but not all, of the information from De Lisle’s map in order to stir Britain towards a more vigorous defense against France’s ambitious claims.

Selection Gallery
-----------------

[Territorial Claims, North America, 1720](/web/20201125172517/https://mappingmovement.newberry.org/item/new-map-north-parts-america-claimed-france-under-names-louisiana-mississipi-canada-and-new-0)

**Citation:**

Moll, Herman, "A New Map of the North Parts of America Claimed by France Under \[the\] Names of Louisiana, Mississipi, Canada and New France, With \[the\] Adjoyning Territories of England and Spain" (\[London\] : H. Moll; Thos. Bowles; John King; John Bowles, 1720). VAULT drawer Graff 2855

[![](https://web.archive.org/web/20201125172517im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_88.jpg?itok=fMPTSoup)](https://web.archive.org/web/20201125172517/https://mappingmovement.newberry.org/item/new-map-north-parts-america-claimed-france-under-names-louisiana-mississipi-canada-and-new-0 "Territorial Claims, North America, 1720")

[View Full Metadata](/web/20201125172517/https://mappingmovement.newberry.org/item/new-map-north-parts-america-claimed-france-under-names-louisiana-mississipi-canada-and-new-0)
