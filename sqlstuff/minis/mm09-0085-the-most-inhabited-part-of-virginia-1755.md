mm09-0085-the-most-inhabited-part-of-virginia-1755.md
﻿---
title: The Most Inhabited Part of Virginia, 1755
parent: Moving Pictures: Maps and Imagination in Eighteenth-Century Anglo-America
parentslug: moving-pictures-maps-and-imagination-eighteenth-century-anglo-america
---

Fry and Jefferson’s (father of Thomas) map owed its origins to the contest over the Ohio lands. The two Virginia planters had been appointed as commissioners to help extend the Virginia-North Carolina boundary in 1749 and were again called upon by the governor of Virginia to create a map in 1750. Much of the information on the Ohio Valley likely came from Fry as his colonel’s commission in the Virginia militia took him to the western part of the colony in the early 1750s. While stationed at Winchester, Fry had access to the information of military men, land speculators’ surveys, and the occasional Indian scout/diplomat. On Fry’s death in 1755, his quartermaster John Dalrymple took Fry’s updated version of the map to London for publication.

Selection Gallery
-----------------

[The Most Inhabited Part of Virginia, 1755](/web/20210127203841/https://mappingmovement.newberry.org/item/map-most-inhabited-part-virginia-containing-whole-province-maryland-part-pensilvania-new-jersey)

**Citation:**

Fry, Joshua, "A Map of the Most Inhabited Part of Virginia Containing the Whole Province of Maryland : with Part of Pensilvania, New Jersey and North Carolina" (London : Thomas Jefferys, \[1755\]). VAULT map7C G3880 1751 .F7 1755

[![](https://web.archive.org/web/20210127203841im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_107.jpg?itok=SxB5peE7)](https://web.archive.org/web/20210127203841/https://mappingmovement.newberry.org/item/map-most-inhabited-part-virginia-containing-whole-province-maryland-part-pensilvania-new-jersey "The Most Inhabited Part of Virginia, 1755")

[View Full Metadata](/web/20210127203841/https://mappingmovement.newberry.org/item/map-most-inhabited-part-virginia-containing-whole-province-maryland-part-pensilvania-new-jersey)

[![](https://web.archive.org/web/20210127203841im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_154.jpg?itok=vnlcdzVA)](https://web.archive.org/web/20210127203841/https://mappingmovement.newberry.org/item/map-most-inhabited-part-virginia-containing-whole-province-maryland-part-pensilvania-new-jersey "The Most Inhabited Part of Virginia, 1755")

[View Full Metadata](/web/20210127203841/https://mappingmovement.newberry.org/item/map-most-inhabited-part-virginia-containing-whole-province-maryland-part-pensilvania-new-jersey)
