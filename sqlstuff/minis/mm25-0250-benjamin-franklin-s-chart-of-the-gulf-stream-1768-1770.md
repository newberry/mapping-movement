mm25-0250-benjamin-franklin-s-chart-of-the-gulf-stream-1768-1770.md
﻿---
title: Benjamin Franklin’s Chart of the Gulf Stream, 1768-1770
parent: Mapping Communication
parentslug: mapping-communication
---

The Gulf Stream is a powerful ocean current that flows up the coast of North America from Florida towards Newfoundland before bending east to cross the Atlantic. Sailors had noticed the current for centuries—the Spanish explorer Juan Ponce de León observed it in 1513—but Benjamin Franklin was the first to name “the Gulph Stream” and map it in its totality. His position at the center of transatlantic communications networks made it possible for him to do so.  
     On his first voyage from America to Europe, in 1726, Franklin had noticed “gulph weed” floating far from shore in the waters of the North Atlantic. Twenty years later, he observed that letters mailed from America to Europe got there faster than letters mailed from Europe to America. At the time he speculated that this might have to do with the rotation of the earth. In 1753, Franklin went to London to serve as deputy postmaster for Britain’s North American colonies. As postmaster, Franklin came to wonder why British mail ships took as much as two weeks longer to sail west from Cornwall to New York than ships sailing a similar distance east from Rhode Island to London. He consulted with merchant and whaling captains, including his cousin Timothy Folger, and realized the mail ships were fighting against the ocean’s current.  
     Together, Folger and Franklin prepared this map of the Gulf Stream and distributed it to British mail ships in 1769 or 1770, hoping that “such Chart and directions may be of use to our Packets in Shortening their Voyages.” This particular copy was reprinted in 1787. When their map is compared to modern satellite images of ocean temperatures, it is remarkable how accurately Franklin and Folger depicted the Gulf Stream’s path. On his next two voyages across the Atlantic, in 1775 and 1776, Franklin measured water temperatures along the way and confirmed the presence of a wide, warm current (Johnson 2008, 9-11). By then, of course, Franklin was no longer in the service of the British Crown.

Selection Gallery
-----------------

[Benjamin Franklin’s Chart of the Gulf Stream, 1768-1770](/web/20210127233758/http://mappingmovement.newberry.org/item/chart-gulf-stream)

**Citation:**

Folger, Timothy, "Chart of the Gulf Stream", in Benjamin Franklin, _Philosophical and Miscellaneous Papers_ (London : Printed for C. Dilly, 1787), facing page 122. Case AC7 .F7 no.1

[![](https://web.archive.org/web/20210127233758im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_196.jpg?itok=KsIOriRh)](https://web.archive.org/web/20210127233758/http://mappingmovement.newberry.org/item/chart-gulf-stream "Benjamin Franklin’s Chart of the Gulf Stream, 1768-1770")

[View Full Metadata](/web/20210127233758/http://mappingmovement.newberry.org/item/chart-gulf-stream)
