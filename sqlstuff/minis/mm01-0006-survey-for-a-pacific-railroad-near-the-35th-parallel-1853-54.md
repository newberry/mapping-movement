mm01-0006-survey-for-a-pacific-railroad-near-the-35th-parallel-1853-54.md
﻿---
title: Survey for a Pacific Railroad near the 35th Parallel, 1853-54
parent: American Railroad Maps, 1828-1876
parentslug: american-railroad-maps-1828-1876
---

Popular opinion in the US in agreed by the mid-1850s held that private interests should build and operate any transcontinental railroad, with the federal government supplying generous land grants, loans, and military support. The expenses were apparently so staggering, and the financial returns so meager, that only one such route could be supported. In 1853 Congress authorized the scientific survey of all practical routes between the Mississippi Valley and the nation’s Pacific ports, hoping that a leading candidate would emerge after objective investigation.  
     The five Pacific Railroad Surveys (1853-55) were among the most ambitious and expensive federal projects undertaken before the Civil War. The surveying parties included surveyors and topographers, of course, but also scientists and artists, who recorded observations of the soils, climate, geology, botany, zoology, ethnography, and landscape. Taken together they were the first comprehensive assessment of the entire territory of the newly acquired territories now known as the American West. The compiled observations of the surveys, published in the twelve-volume _Reports of Explorations and Surveys to Ascertain the Most Practicable and Economical Route for a Railroad from the Mississippi River to the Pacific Ocean_ are a landmark in the history of American science, and laid the foundation for still larger surveys of the West that followed the Civil War.  
     The two maps and geological profile reproduced here were prepared by the party led by Lieut. Amiel Weeks Whipple that surveyed a possible route near the 35th Parallel of northern latitude, in 1853. The two large maps (Nos. 1 and 2) show the suggested route and the main topographical features of the surrounding country. The solid line running east-west across the two sheets delineates the suggested route, while the dashed lines identify alternates. No single railroad that was built followed the entirety of any one of Whipple’s suggested routes, though in New Mexico, Arizona, and California, the route surveyed roughly parallels the Atchison, Topeka, and Santa Fe Railroad mainline. Lighter lines mark the trails of reconnoitering parties and of existing wagon roads. Watercourses, wells, springs, trails, routes of previous explorations, and Indian settlements are also noted. Several inset maps provide greater detail of key points along the suggested route, such as the Pecos River crossing in eastern New Mexico and Campbell’s Pass in western New Mexico. The thoroughness of the map, which supports rich comparative reading with more recent maps, might obscure the expedition’s main purpose, which was to identify a feasible railroad route.  
     The third map is a cross-section of the main geological formations underlying the route prepared Jules Marcou, geologist to Whipple’s expedition, gives some indication both of the scientific ambitions of the Pacific Railroad Surveys and of their practical orientation. These were among the first attempts to grasp the geological complexities of the West, a matter of great interest to anyone interested in developing its mineral and agricultural potential. Though the profile greatly exaggerates the relative heights of the mountains and valleys, we gain as well some sense of the difficulties the terrain posed to railroad construction.  

Selection Gallery
-----------------

[From Fort Smith to the Rio Grande](/web/20210119182850/http://mappingmovement.newberry.org/item/explorations-and-surveys-rail-road-route-mississippi-river-pacific-ocean-route-near-35th)

**Citation:**

Whipple, Amiel Weeks, “Explorations and Surveys for a Rail Road Route from the Mississippi River to the Pacific Ocean, War Department, Route Near the 35th Parallel, Map No. 1, From Fort Smith to the Rio Grande, From Explorations and Surveys Made by Lieut. A.W. Whipple…Lieut. J.C. Ives…1853-4", detached from United States. War Department, _Reports of Explorations and Surveys to Ascertain the Most Practicable and Economical Route for a Railroad from the Mississippi River to the Pacific Ocean,_ U.S. Serial Set 768 or 801, 33rd Congress, 2nd session, Senate executive document 78 or House executive document 91 (Washington, D.C. \[etc.\] : A.O.P. Nicholson, printer \[and others\], 1855-1860), v. 11, pt. 2. Map6F G4051.P3 1853 U5, v.11, pt. 2, no. 34

[![](https://web.archive.org/web/20210119182850im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/MM01-0006-01_1_2.jpg?itok=l5-Sy5AE)](https://web.archive.org/web/20210119182850/http://mappingmovement.newberry.org/item/explorations-and-surveys-rail-road-route-mississippi-river-pacific-ocean-route-near-35th "From Fort Smith to the Rio Grande")

[View Full Metadata](/web/20210119182850/http://mappingmovement.newberry.org/item/explorations-and-surveys-rail-road-route-mississippi-river-pacific-ocean-route-near-35th)

[![](https://web.archive.org/web/20210119182850im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/MM01-0006-02.jpg?itok=a1djTWLy)](https://web.archive.org/web/20210119182850/http://mappingmovement.newberry.org/item/explorations-and-surveys-rail-road-route-mississippi-river-pacific-ocean-route-near-35th "From Fort Smith to the Rio Grande")

[View Full Metadata](/web/20210119182850/http://mappingmovement.newberry.org/item/explorations-and-surveys-rail-road-route-mississippi-river-pacific-ocean-route-near-35th)

[From the Rio Grande to the Pacific Ocean](/web/20210119182850/http://mappingmovement.newberry.org/item/explorations-and-surveys-rail-road-mississippi-river-pacific-ocean-route-near-35th-parallel-map)

**Citation:**

Whipple, Amiel Weeks, “Explorations and Surveys for a Rail Road Route from the Mississippi River to the Pacific Ocean, War Department, Route Near the 35th Parallel, Map No. 2, from the Rio Grande to the Pacific Ocean, from Explorations and Surveys Made by Lieut. A.W. Whipple…Lieut. J.C. Ives…1853-4", detached from United States. War Department, _Reports of Explorations and Surveys to Ascertain the Most Practicable and Economical Route for a Railroad from the Mississippi River to the Pacific Ocean,_ U.S. Serial Set 768 or 801, 33rd Congress, 2nd session, Senate executive document 78 or House executive document 91 (Washington, D.C. \[etc.\] : A.O.P. Nicholson, printer \[and others\], 1855-1860), v. 11, pt. 2. Map6F G4051.P3 1853 U5, v.11, pt. 2, no. 35

[![](https://web.archive.org/web/20210119182850im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/MM01-0006-03-1-2.jpg?itok=xbcqfvc5)](https://web.archive.org/web/20210119182850/http://mappingmovement.newberry.org/item/explorations-and-surveys-rail-road-mississippi-river-pacific-ocean-route-near-35th-parallel-map "From the Rio Grande to the Pacific Ocean")

[View Full Metadata](/web/20210119182850/http://mappingmovement.newberry.org/item/explorations-and-surveys-rail-road-mississippi-river-pacific-ocean-route-near-35th-parallel-map)

[![](https://web.archive.org/web/20210119182850im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/MM01-0006-04.jpg?itok=cTPRijBs)](https://web.archive.org/web/20210119182850/http://mappingmovement.newberry.org/item/explorations-and-surveys-rail-road-mississippi-river-pacific-ocean-route-near-35th-parallel-map "From the Rio Grande to the Pacific Ocean")

[View Full Metadata](/web/20210119182850/http://mappingmovement.newberry.org/item/explorations-and-surveys-rail-road-mississippi-river-pacific-ocean-route-near-35th-parallel-map)

[Geological section from the Mississippi River to the Pacific Ocean](/web/20210119182850/http://mappingmovement.newberry.org/item/geological-section-mississippi-river-pacific-ocean-along-route-explored-lieut-aw-whipple-corp)

**Citation:**

Marcou, Jules, “Geological Section from the Mississippi River to the Pacific Ocean : Along the Route Explored by Lieut. A.W. Whipple, Corp. of Topl. Engrs. Near the Parallel of 35⁰ North Latitude, 1853-1854, Prepared to Accompany the Preliminary Report", detached from "Resumé and field notes", in United States. War Department, _Reports of Explorations and Surveys to Ascertain the Most Practicable and Economical Route for a Railroad from the Mississippi River to the Pacific Ocean,_ U.S. Serial Set 760, 33rd Congress, 2nd session, Senate executive document 78 (Washington, D.C. \[etc.\] : A.O.P. Nicholson, printer \[and others\], 1855-1860), v. 3, pt. 4. Map4F G4051.P3 1853 .U5 v. 3, pt. 4, no. 1

[![](https://web.archive.org/web/20210119182850im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/MM01-0006-05.jpg?itok=8heKnZLQ)](https://web.archive.org/web/20210119182850/http://mappingmovement.newberry.org/item/geological-section-mississippi-river-pacific-ocean-along-route-explored-lieut-aw-whipple-corp "Geological section from the Mississippi River to the Pacific Ocean")

[View Full Metadata](/web/20210119182850/http://mappingmovement.newberry.org/item/geological-section-mississippi-river-pacific-ocean-along-route-explored-lieut-aw-whipple-corp)
