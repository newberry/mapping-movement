mm09-0089-the-seat-of-war-in-new-england-1775.md
﻿---
title: The Seat of War in New England, 1775
parent: Moving Pictures: Maps and Imagination in Eighteenth-Century Anglo-America
parentslug: moving-pictures-maps-and-imagination-eighteenth-century-anglo-america
---

The map provides no author, saying only that it was the product of “An American Volunteer” presumably present for the attack on Breed’s (Bunker) Hill. Despite its authorship, the map was printed in London in 1775. The map thus obscures authorship but does make sure its readers know that, for example, the map of Boston harbor in the upper right was from “an actual survey.” The map features some impressive artwork, particularly in the dramatic scene of British ships bombarding Charlestown.

Selection Gallery
-----------------

[The Seat of War in New England, 1775](/web/20210119192625/https://mappingmovement.newberry.org/item/seat-war-new-england)

**Citation:**

"The Seat of War in New England" (London : Robert Sayer & John Bennett, 1775). Ayer 133 .S46 1775

[![](https://web.archive.org/web/20210119192625im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_52.jpg?itok=RVxi9GH_)](https://web.archive.org/web/20210119192625/https://mappingmovement.newberry.org/item/seat-war-new-england "The Seat of War in New England, 1775")

[View Full Metadata](/web/20210119192625/https://mappingmovement.newberry.org/item/seat-war-new-england)
