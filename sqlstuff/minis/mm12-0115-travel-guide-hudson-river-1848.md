mm12-0115-travel-guide-hudson-river-1848.md
﻿---
title: Travel Guide, Hudson River, 1848
parent: Waterways Cartography, Part I: The Mapping of North America's Internal Navigation Systems
parentslug: waterways-cartography-part-i-mapping-north-americas-internal-navigation-systems
---

The Hudson River provided a long reach into North America for ocean-going ships from the dawn of European exploration until the early nineteenth century. It was no accident that the advent of steam navigation should take place on Henry Hudson’s river or that Robert Fulton, having turned his interest from canal engineering to steamboats, should be the creative genius who demonstrated how steam could power commercial vessels. It soon became possible for steamboats to follow regular schedules up and down the Hudson River. Shortly thereafter, people wanted guidebooks to help them appreciate the passing scenery, turning passengers into tourists. The completion of the Erie and Champlain canals in 1825 accelerated this trend, and the subsequent development of railroads in the 1830s and 1840s extended the new tourist industry into the America interior. _Pratt’s River and Railroad Guide_ documents this situation in 1848, the very year that the opening of the Illinois and Michigan Canal completed the promise of the canal age and hinted, at the same time, that railroad tracks would soon command over the nation’s transportation system.  
      _Pratt’s Guide_ extended its coverage beyond the Hudson River and the Erie Canal to provide maps of the Mississippi, Missouri, Ohio, and Illinois Rivers in addition to the Hudson. Railroads had by then grown in importance so that they received equal billing in the title of Pratt’s book, but they still were called “connecting lines” between waterways, which took the major responsibility for enabling a traveler to go from Boston to New Orleans, the publisher’s way of indicating on the title page that his guide was national in scope.  
      Actually, the Hudson River maps for the booklet were adapted from an 1846 broadside map and guide to the river that “may be had at the Bar of the Steam-Boat Troy.” This earlier sheet featured a map of the Hudson at the center of a three-column format, with schedules and a guide to cities and towns on the outer two columns. Pratt simply cut the strip map of the Hudson into sections, updated it, and then developed similar maps for his other key routes.  
      The map focuses our attention on the great river, marking its flow down the center of the image, with a bold stroke. The stream’s major tributary, the Mohawk River, received a bold line as well, but it remained nameless on the page because it had been supplanted as a transportation artery by the Erie Canal and two railroads. The competition between the two railroads suggests an urban rivalry indicated in their names. Both lines cut the travel time on the Erie Canal by avoiding the long delay caused by the series of locks at the eastern end of the waterway. Note that one railroad headed toward Troy and the other connected Schenectady with Albany. In the competition between the two Hudson River ports, Albany soon won the battle due to its better rail connections and navigation facilities. (See Focus Map 6)  
      Focus Map 5 is from the first edition of Pratt’s _Guide,_ which was expected to be revised and enlarged each year with a new edition appearing every January 1st. The next issue, in 1849, for example, would include the 463-mile New York and Erie Railroad “with a description of every place on the entire route.” Advertising would hold down the price of the expanded _Guide_. In 1848, about sixty firms from New York and sixteen from Philadelphia ran advertisements placed throughout the booklet.  
      The first content page of this practical publication was a chart giving the most expensive fares between cities, starting with the route between New Orleans and Boston via Chicago and Detroit in sixteen days at the first-class rate of $58. But, the table notes “that those who travel in the second train, or on deck of the steamers, and second class boats, etc. could go for much less expense.” (3) The _Guide_ then provided brief paragraphs on the leading towns along each route, followed by a series of maps similar to the one shown here but at a much smaller scale. The first five maps, for example, cover the Mississippi River from Lake Pepin to New Orleans. The Erie Canal takes up three maps on pages 46 to 48, while the Hudson River needed six pages. Detailed distance charts cover all the major rivers, with 34 landings given for the route by steamboat on the Missouri River between Council Bluffs and St. Louis.

Selection Gallery
-----------------

[Travel Guide, Hudson River, 1848](/web/20201126100502/https://mappingmovement.newberry.org/item/map-hudson-river)

**Citation:**

Felter, John D., "Map of the Hudson River" in _Pratt’s River and Railroad Guide_ (New York : F.M. Pratt, \[1848\]), p. 55-60. Baskes E158 .P91 1848

[![](https://web.archive.org/web/20201126100502im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_34.jpg?itok=5c5tm0ne)](https://web.archive.org/web/20201126100502/https://mappingmovement.newberry.org/item/map-hudson-river "Travel Guide, Hudson River, 1848")

[View Full Metadata](/web/20201126100502/https://mappingmovement.newberry.org/item/map-hudson-river)

[![](https://web.archive.org/web/20201126100502im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_35.jpg?itok=KDfOm2D_)](https://web.archive.org/web/20201126100502/https://mappingmovement.newberry.org/item/map-hudson-river "Travel Guide, Hudson River, 1848")

[View Full Metadata](/web/20201126100502/https://mappingmovement.newberry.org/item/map-hudson-river)

[![](https://web.archive.org/web/20201126100502im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_36.jpg?itok=tZ75b1A-)](https://web.archive.org/web/20201126100502/https://mappingmovement.newberry.org/item/map-hudson-river "Travel Guide, Hudson River, 1848")

[View Full Metadata](/web/20201126100502/https://mappingmovement.newberry.org/item/map-hudson-river)

[![](https://web.archive.org/web/20201126100502im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_37.jpg?itok=TeLyHE-Q)](https://web.archive.org/web/20201126100502/https://mappingmovement.newberry.org/item/map-hudson-river "Travel Guide, Hudson River, 1848")

[View Full Metadata](/web/20201126100502/https://mappingmovement.newberry.org/item/map-hudson-river)
