mm07-0070-the-gold-and-coal-fields-of-alaska-1898.md
﻿---
title: The Gold and Coal Fields of Alaska, 1898
parent: Maps of Trails and Roads of the Great West
parentslug: maps-trails-and-roads-great-west
---

Already well established by the end of the nineteenth century, The US Coast and Geodetic Survey (1878) and the US Geological Survey (1879) were civilian governmental successors to the US Army Corps of Topographical Engineers. This Geological Survey map is a close copy of Chart T of the Coast and Geodetic Survey map and accompanying description of Alaska published in 1897 and was prepared in accordance with a resolution of the US Congress of 1898.

Selection Gallery
-----------------

[The Gold and Coal Fields of Alaska, 1898](/web/20210119183602/http://mappingmovement.newberry.org/item/gold-and-coal-fields-alaska-together-principal-steamer-routes-and-trails)

**Citation:**

Geological Survey (U.S.), "The Gold and Coal Fields of Alaska : Together with the Principal Steamer Routes and Trails" (\[Washington, DC\] : U.S. Geological Survey, 1898). Map4C G4371.H1 1898 .U5

[![](https://web.archive.org/web/20210119183602im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_167.jpg?itok=KWMcowgE)](https://web.archive.org/web/20210119183602/http://mappingmovement.newberry.org/item/gold-and-coal-fields-alaska-together-principal-steamer-routes-and-trails "The Gold and Coal Fields of Alaska, 1898")

[View Full Metadata](/web/20210119183602/http://mappingmovement.newberry.org/item/gold-and-coal-fields-alaska-together-principal-steamer-routes-and-trails)
