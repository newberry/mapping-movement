mm08-0077-new-voyages-in-northern-america-1703.md
﻿---
title: New Voyages in Northern America, 1703
parent: European Maps for Exploration and Discovery
parentslug: european-maps-exploration-and-discovery
---

The baron de Lahontan (1664-c. 1715) was a French noble who came to Canada with the colonial troops in 1683, and remained there in various capacities until 1693, when he abandoned his post and returned to Europe. After ten years of obscurity, in 1703 he published the three works for which he is famous :  
  
_•    Nouveaux Voyages dans l’Amérique septentrionale  
•    Mémoires de l’Amérique septentrionale  
•    Supplément aux voyages._  
  
Our map appears in the first of these, translated as “New Voyages in Northern America.” Lahontan claimed to have drawn this map after a long and arduous voyage between September 1688 and May 1689, during which he covered about 1,000 miles of the “Rivière Longue,” or “Long River.” Some scholars have found it absurd to claim that so demanding a voyage could be made in the dead of winter, and have consequently denied that the voyage ever took place. Moreover, Lahontan’s illustrations of a longhouse and a canoe seem to be a combination of objects found among Eastern tribes and in the Great Lakes. However, there is no denying that the “Rivière Longue” generally follows the line of the Missouri River, or that the map shows across the western mountains (near the “Pays des Gnasitares”) another great river (presumably the Columbia) leading to the Pacific Ocean. Lahontan clearly did not personally explore all these water-courses, but he may well have learned of them from informants among the Indian peoples, with whom he was notably familiar.  
     Lahontan’s work was highly influential. His books were translated into English, Dutch, German and Italian, and elements of this map were copied by the leading cartographers of the day. In his admiration for some of the practices of the Indian peoples he was among the earliest to put forward the idea of the “noble savage.” Lahontan’s work may be criticized in detail, but there is no denying its widespread influence.  
 

Selection Gallery
-----------------

[New Voyages in Northern America, 1703](/web/20210120041159/https://mappingmovement.newberry.org/item/carte-de-la-riviere-longue-et-de-quelques-autres-qui-se-dechargent-dans-le-grand-fleuve-de)

**Citation:**

Lahontan, Louis Armand de Lom d’Arce, baron de, "Carte de la Riviere Longue : et de Quelques Autres, qui se Dechargent dans le Grand Fleuve de Missisipi ; Carte que les Gnacsitares ont Dessiné sur des Paux de Cerfs ....",  in _Nouveaux Voyages de Mr. le Baron de Lahontan dans l’Amerique Septentrionale_ (La Haye : Chez les fréres l’Honoré, 1703), v. 1, between table of contents and p. 1. Graff 2365

[![](https://web.archive.org/web/20210120041159im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_152.jpg?itok=HSHkB25M)](https://web.archive.org/web/20210120041159/https://mappingmovement.newberry.org/item/carte-de-la-riviere-longue-et-de-quelques-autres-qui-se-dechargent-dans-le-grand-fleuve-de "New Voyages in Northern America, 1703")

[View Full Metadata](/web/20210120041159/https://mappingmovement.newberry.org/item/carte-de-la-riviere-longue-et-de-quelques-autres-qui-se-dechargent-dans-le-grand-fleuve-de)
