mm09-0086-the-carolinas-and-their-indian-frontiers-1775.md
﻿---
title: The Carolinas and their Indian Frontiers, 1775
parent: Moving Pictures: Maps and Imagination in Eighteenth-Century Anglo-America
parentslug: moving-pictures-maps-and-imagination-eighteenth-century-anglo-america
---

Selection Gallery
-----------------

[The Carolinas and their Indian Frontiers, 1775](/web/20210127203342/https://mappingmovement.newberry.org/item/accurate-map-north-and-south-carolina-their-indian-frontiers-shewing-distinct-manner-all)

**Citation:**

Mouzon, Henry, "An Accurate Map of North and South Carolina with their Indian Frontiers" (London : Robert Sayer and John Bennett, 1775). Ayer 133 .M888 1775

[![](https://web.archive.org/web/20210127203342im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_256.jpg?itok=77daJfxA)](https://web.archive.org/web/20210127203342/https://mappingmovement.newberry.org/item/accurate-map-north-and-south-carolina-their-indian-frontiers-shewing-distinct-manner-all "The Carolinas and their Indian Frontiers, 1775")

[View Full Metadata](/web/20210127203342/https://mappingmovement.newberry.org/item/accurate-map-north-and-south-carolina-their-indian-frontiers-shewing-distinct-manner-all)
