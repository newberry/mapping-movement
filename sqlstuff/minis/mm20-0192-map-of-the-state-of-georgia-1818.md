mm20-0192-map-of-the-state-of-georgia-1818.md
﻿---
title: Map of the State of Georgia, 1818
parent: Lines that Fracture and Fade: Two Centuries of Travel through Georgia, 1775-1976
parentslug: lines-fracture-and-fade-two-centuries-travel-through-georgia-1775-1976
---

It would be forty years before another map of Georgia appeared approaching the level of detail contained in the Stuart-Purcell map. Much of this delay had to do with the uncertainty of Georgia’s boundaries and its claims to western lands, not settled until the end of the Yazoo land controversies in the late 1700s and the series of Georgia-Indian wars that bled into the War of 1812.  
     Daniel Sturges had been Surveyor General of Georgia since 1797 but it was only by 1816 that he began advertising his forthcoming general map of Georgia, with the assistance of the Savannah merchant Eleazar Early. Engraved in Philadelphia by Samuel Harrison and printed out of John Melish’s shop, the map finally appeared in 1818.  
     The map’s goal was to celebrate the expansion of Georgia westward after years of uncertainty. The bright watercolor lines signify Georgia’s counties highlight Georgians’ development of the region west and south of the Altamaha River. The map even encourages the reader to participate in the celebration. The table listing county populations provides numbers from the 1810 federal census and then provides a column of blanks for the forthcoming 1820 census numbers, presumably to be filled in by the map’s users.  
 

Selection Gallery
-----------------

[Map of the State of Georgia, 1818](/web/20210127233559/http://mappingmovement.newberry.org/item/map-state-georgia)

**Citation:**

Sturges, Daniel, "Map of the State of Georgia" (Savannah, Ga. ; Philadelphia, Penn. : Eleazer Early and John Melish ; Samuel Harrison, 1818). Ayer 133 .S93 1818

[![](https://web.archive.org/web/20210127233559im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_265.jpg?itok=d6wpWclY)](https://web.archive.org/web/20210127233559/http://mappingmovement.newberry.org/item/map-state-georgia "Map of the State of Georgia, 1818")

[View Full Metadata](/web/20210127233559/http://mappingmovement.newberry.org/item/map-state-georgia)
