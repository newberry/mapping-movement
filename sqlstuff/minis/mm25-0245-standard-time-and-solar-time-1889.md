mm25-0245-standard-time-and-solar-time-1889.md
﻿---
title: Standard Time and Solar Time, 1889
parent: Mapping Communication
parentslug: mapping-communication
---

Before the middle of the nineteenth century, every town and city kept its own local time, based on the rising and setting of the sun. Travel and communication were slow enough that time discrepancies between different locations rarely mattered. Sailors and mapmakers needed the exact time to determine their longitude, but most other people hardly noticed differences in times. Indeed, in the eighteenth century and before, many clocks did not even have minute hands.  
     The railroad and the telegraph changed all this. For both efficiency and safety, railroads adopted single time standards to coordinate their operations. Britain’s railroads were the first to make this change. In 1847, all British railways agreed to adopt Greenwich Mean Time—that is, the mean solar time observed at the Royal Observatory in Greenwich, London—which soon became known throughout Britain as “railway time.”  
     In North America, consolidation came slower. Each railroad typically adopted the local time of the largest city it served. Thus, the Pennsylvania Railroad ran on Philadelphia time, the New York Central ran on New York City time, and so on. Railway guides in the 1850s and 1860s listed between fifty and a hundred distinct railroad time systems. But as the industry consolidated, with smaller railroads merging or being taken over by larger firms, time zones gradually consolidated too.  
     By the 1880s, several plans for time reform had been advanced. Some reformers urged Americans to adopt a single national time zone for the entire country. (Tsar Alexander III decreed one single time for all of Russia in 1888, despite a difference in solar time of nearly nine hours from one end of the Russian Empire to the other.) In 1883, a convention of railroad presidents, managers, and superintendents voted to adopt five standard time zones for North America, each an hour apart. A map not unlike this one played a role in their deliberations. William F. Allen, the secretary of the railroads’ General Time Convention, displayed two maps: one showing the then-existing time systems as a multicolored patchwork quilt, the other showing the proposed time zones in five simple stripes (as shown here). Both maps were available for sale, Allen said, but the one depicting “current chaos” was too complicated to be mass produced and would therefore cost twice as much (Galison, 123-124).  
     This map, published six years after the adoption of standard time, provides a list of conversions back to the old solar times for many American cities. We see, for instance, that Eastern Standard Time is sixteen minutes slower than Boston’s solar time, but eight minutes faster than Washington, DC’s. The railroads could not require government or ordinary citizens to adopt their time standards (except when catching a train) and for a few years, some Americans did keep track of both solar and railroad times. But this was cumbersome, and people found little advantage to preserving two systems. In 1918, Congress passed the Standard Time Act, making official what was already true in practice. Railroad schedules had come to trump the sun in defining time itself.  
 

Selection Gallery
-----------------

[Standard Time and Solar Time, 1889](/web/20201126104627/https://mappingmovement.newberry.org/item/map-showing-divisions-standard-time-united-states)

**Citation:**

Cram, George Franklin, "Map Showing the Divisions of Standard Time \[in the United States\]", in _Cram’s Unrivaled Atlas of the World, Indexed_ (Rochester, N.Y. : W.H. Stewart, 1889), p. 5. Baskes folio G1019 .G46 1889b

[![](https://web.archive.org/web/20201126104627im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_194.jpg?itok=PMQiJuQs)](https://web.archive.org/web/20201126104627/https://mappingmovement.newberry.org/item/map-showing-divisions-standard-time-united-states "Standard Time and Solar Time, 1889")

[View Full Metadata](/web/20201126104627/https://mappingmovement.newberry.org/item/map-showing-divisions-standard-time-united-states)
