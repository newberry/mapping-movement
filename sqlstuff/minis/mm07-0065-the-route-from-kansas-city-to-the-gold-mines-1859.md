mm07-0065-the-route-from-kansas-city-to-the-gold-mines-1859.md
﻿---
title: The Route from Kansas City to the Gold Mines, 1859
parent: Maps of Trails and Roads of the Great West
parentslug: maps-trails-and-roads-great-west
---

This map covers Kansas and parts of eastern Colorado and southern Nebraska and folded into a _Guide to the Kansas Gold Mines at Pike’s Peak_ by William Gilpin. He went on to become the first territorial governor of Colorado in 1861 and publish his own Colorado gold rush map in 1862. John Williams Gunnison was a captain in the US Army Corps of Topographical Engineers, which emerged from the Army Bureau of Topographical Engineers in 1838, an American inheritor of the scientific mapmaking tradition of the Spanish Royal Corps of Engineers. Although Gunnison died surveying in the field in 1853, his information was deemed critical and current enough to incorporate in this map. Edward Mendenhall worked directly for the publisher, Middleton, Strobridge & Co., in Cincinnati, which specialized in guidebooks and maps of the American West from the 1850s to the 1870s.

Selection Gallery
-----------------

[The Route from Kansas City to the Gold Mines, 1859](/web/20210127230328/http://mappingmovement.newberry.org/item/map-kansas-route-kansas-city-gold-mines)

**Citation:**

Middleton, Strobridge & Co., "Map of Kansas with Route from Kansas City to the Gold Mines", in John Williams Gunnison, _Guide to the Kansas Gold Mines at Pike’s Peak_ (Cincinnati, OH : E. Mendenhall, 1859), inside back cover. Graff 1693

[![](https://web.archive.org/web/20210127230328im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_173.jpg?itok=D0gbeygm)](https://web.archive.org/web/20210127230328/http://mappingmovement.newberry.org/item/map-kansas-route-kansas-city-gold-mines "The Route from Kansas City to the Gold Mines, 1859")

[View Full Metadata](/web/20210127230328/http://mappingmovement.newberry.org/item/map-kansas-route-kansas-city-gold-mines)

[![](https://web.archive.org/web/20210127230328im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/MM07-0065-01.jpg?itok=Y9TL0lV4)](https://web.archive.org/web/20210127230328/http://mappingmovement.newberry.org/item/map-kansas-route-kansas-city-gold-mines "The Route from Kansas City to the Gold Mines, 1859")

[View Full Metadata](/web/20210127230328/http://mappingmovement.newberry.org/item/map-kansas-route-kansas-city-gold-mines)
