mm25-0249-rivers-roads-and-rates-of-travel-1932.md
﻿---
title: Rivers, Roads, and Rates of Travel, 1932
parent: Mapping Communication
parentslug: mapping-communication
---

This famous set of maps from Charles O. Paullin’s _Atlas of the Historical Geography of the United States_ shows the increasing speed of travel and communication in the United States between 1800 and 1930. Paullin, a historian at the Carnegie Institution of Washington, and John K. Wright, a geographer at the American Geographic Society, spent more than twenty years producing the _Atlas_, completed in 1932. Eighty years after its publication, it remains a classic work of historical geography, with more than 700 attractive and authoritative maps on a wide range of historical topics.  
     Perhaps the most commonly reproduced maps in the Atlas are the five maps here showing “Rates of Travel.” The contour lines on each map are called “isochrones,” lines connecting points at which things arrive or occur at the same time. Paullin and his team used dozens of historical sources to construct these maps, along with some artful estimation. They reveal not one but several revolutions in communication and transportation: the expansion of postal roads, the development of the steamboat, and the construction of the Erie Canal between 1800 and 1830; the spread of the railroads east of the Mississippi between 1830 and 1857; the completion of the transcontinental railroads and the beginning of air travel between 1857 and 1930. Other maps on the page depict in geographic space what the isochrones describe in time. (Plate H, showing postal roads and stage routes in 1774, was based primarily on Hugh Finlay’s maps and journals, described above (Map #2).) The arrangement of these maps suggests the successive, cumulative force of change. In 1857, for example, one could travel from New York to St. Louis in about three days—a pace that would have seemed slow by the standards of Paullin’s day but astonishingly fast a century before.  
     Paullin’s splendid maps do distort the past in at least one way: New York City in the early 1800s was not the center of American communications in the way it would be by 1932. In the early nineteenth century, the US communications infrastructure was far more polycentric than it would later become, with distinct regional networks and sharp variations from place to place. But an isochrone map needs a center, and such simplification hardly detracts from this classic piece of data visualization.  
 

Selection Gallery
-----------------

[Rivers, Roads, and Rates of Travel, 1932](/web/20201126103801/https://mappingmovement.newberry.org/item/transportation)

**Citation:**

Paullin, Charles Oscar, "Transportation", in _Atlas of the Historical Geography of the United States_ (\[Washington, D.C.\] : Published jointly by Carnegie Institution of Washington and the American Geographical Society of New York, 1932), plate 138. Map Ref folio G1201.S1 P3 1932

[![](https://web.archive.org/web/20201126103801im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_231.jpg?itok=oXgSghjJ)](https://web.archive.org/web/20201126103801/https://mappingmovement.newberry.org/item/transportation "Rivers, Roads, and Rates of Travel, 1932")

[View Full Metadata](/web/20201126103801/https://mappingmovement.newberry.org/item/transportation)
