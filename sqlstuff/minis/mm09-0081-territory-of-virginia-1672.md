mm09-0081-territory-of-virginia-1672.md
﻿---
title: Territory of Virginia, 1672
parent: Moving Pictures: Maps and Imagination in Eighteenth-Century Anglo-America
parentslug: moving-pictures-maps-and-imagination-eighteenth-century-anglo-america
---

Lederer’s map was attached to the published account of his journey, _The Discoveries of John Lederer,_ published in London in 1672. Lederer’s original journal (in Latin) and map were transmitted and prepared for publication by William Talbot, Provincial Secretary for Maryland. Originally part of a company of Virginia colonists interested in exploring trade opportunities with Southeastern Indians, Lederer found himself alone after the Virginians abandoned him. He then continued his long trek southward accompanied by a Susquehannock Indian guide familiar with, but not native to, the region. Lederer’s map influenced the mapping of the Southeast for many years to come.

Selection Gallery
-----------------

[Territory of Virginia, 1672](/web/20210120042229/https://mappingmovement.newberry.org/item/map-whole-territory-traversed-iohn-lederer-his-three-marches)

**Citation:**

Lederer, John, "A Map of the Whole Territory Traversed by Iohn Lederer in His Three Marches", in _The Discoveries of John Lederer, in Three Several Marches from Virginia, to the West of Carolina, and Other Parts of the Continent : Begun in March 1669, and Ended in September 1670_ (London : Printed by J\[ohn\]. C\[ottrell\]. for S. Heyrick, 1672). VAULT Ayer 150.5 .V7 L4 1672

[![](https://web.archive.org/web/20210120042229im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_24.jpg?itok=nREToOpR)](https://web.archive.org/web/20210120042229/https://mappingmovement.newberry.org/item/map-whole-territory-traversed-iohn-lederer-his-three-marches "Territory of Virginia, 1672")

[View Full Metadata](/web/20210120042229/https://mappingmovement.newberry.org/item/map-whole-territory-traversed-iohn-lederer-his-three-marches)
