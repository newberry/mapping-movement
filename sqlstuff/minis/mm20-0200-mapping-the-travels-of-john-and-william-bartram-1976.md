mm20-0200-mapping-the-travels-of-john-and-william-bartram-1976.md
﻿---
title: Mapping the Travels of John and William Bartram, 1976
parent: Lines that Fracture and Fade: Two Centuries of Travel through Georgia, 1775-1976
parentslug: lines-fracture-and-fade-two-centuries-travel-through-georgia-1775-1976
---

_The Atlas of Early American History_ was the brainchild of Lester J. Cappon, former director of the Institute for Early American History and Culture in Williamsburg, Virginia. In the 1960s, Cappon formed the idea of trying to present the social, intellectual, and economic context of the American Revolution through the creation of original maps that would do more than simply chart colonial boundaries and principal towns. These maps would help scholars and the public at large visualize the movement of ideas across the landscape and help explain the origins of the American Revolution.  
     Funded by the National Endowment for the Humanities and created through a decade-long partnership with the Newberry Library, the Atlas arrived in 1975, just in time for America’s bicentennial. Maps such as this one attempted to show the spread of the Scientific Revolution through the colonies. Impossible to actually chart the acceptance of scientific belief, Cappon and his co-editors relied on the travels of the naturalists John and William Bartram to represent the physical movement of ideas. The frustration of the map’s creators and Cappon’s need to justify the map in print indicate a certain lack of satisfaction with the final result, which matched neither the ambitions of the original project nor the amazing and creative maps that filled the rest of the Atlas.  
     The Newberry houses the research materials related to the _Atlas’s_ creation, a rich trove of correspondence and editorial decisions that allow scholars to investigate the process of developing such a volume.  
  
 

Selection Gallery
-----------------

[Mapping Travels of John and William Bartram, 1976](/web/20210128000349/http://mappingmovement.newberry.org/item/travels-john-and-william-bartram-july-1765-april-1766-travels-william-bartram-march-1773)

**Citation:**

Cappon, Lester Jesse, "Travels of John and William Bartram, July 1765-April 1766 ; Travels of William Bartram, March 1773-January 1777", in _Atlas of Early American History : the Revolutionary Era, 1760-1790_ (\[Princeton, N.J\] : Published for the Newberry Library and the Institute of Early American History and Culture by Princeton University Press, 1976), page 33. Map Ref oversize G1201.S3 A8 1976b

[![](https://web.archive.org/web/20210128000349im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_278.jpg?itok=RjBYHBwf)](https://web.archive.org/web/20210128000349/http://mappingmovement.newberry.org/item/travels-john-and-william-bartram-july-1765-april-1766-travels-william-bartram-march-1773 "Mapping Travels of John and William Bartram, 1976")

[View Full Metadata](/web/20210128000349/http://mappingmovement.newberry.org/item/travels-john-and-william-bartram-july-1765-april-1766-travels-william-bartram-march-1773)
