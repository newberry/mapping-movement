#!/bin/bash

# First, let's get that file's name:
for file in *.md; do
echo -e "$file\n$(cat $file)" > $file
# sed -i '.bak' "1s/^/fn: $file\n/g" $file
# echo $file
done
    # FILE=$(ls -rt $file | tail -n1)
    # if [[ 0 -ne $? ]]; then
    #     echo "Unable to locate matching file.  Aborting." 1>&2
    #     exit 1
    # fi

    # # Now, create a new file containing the file's name:
    # echo "$FILE" > NEWFILE.TXT

    # # And append the contents of the old file into the new:
    # cat "$FILE" >> NEWFILE.TXT

# Finally, get rid of the old file: (uncomment if you're sure)
# rm "$FILE"