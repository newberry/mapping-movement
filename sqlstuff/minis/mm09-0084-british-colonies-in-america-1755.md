mm09-0084-british-colonies-in-america-1755.md
﻿---
title: British Colonies in America, 1755
parent: Moving Pictures: Maps and Imagination in Eighteenth-Century Anglo-America
parentslug: moving-pictures-maps-and-imagination-eighteenth-century-anglo-america
---

Selection Gallery
-----------------

[British Colonies in America, 1755](/web/20210127204249/https://mappingmovement.newberry.org/item/general-map-middle-british-colonies-america-viz-virginia-maryland-delaware-pensilvania-new)

**Citation:**

Evans, Lewis, "A General Map of the Middle British Colonies, in America : viz Virginia, Maryland, Delaware, Pensilvania, New-Jersey, New-York, Connecticut, and Rhode Island" (Philadelphia : Lewis Evans, 1755), detached from _Geographical, Historical, Political, Philosophical and Mechanical Essays_ (Philadelphia : Printed by B. Franklin, and D. Hall, 1755). Ayer map4F G3790 1755 .E9

[![](https://web.archive.org/web/20210127204249im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_181.jpg?itok=EP2NI0VM)](https://web.archive.org/web/20210127204249/https://mappingmovement.newberry.org/item/general-map-middle-british-colonies-america-viz-virginia-maryland-delaware-pensilvania-new "British Colonies in America, 1755")

[View Full Metadata](/web/20210127204249/https://mappingmovement.newberry.org/item/general-map-middle-british-colonies-america-viz-virginia-maryland-delaware-pensilvania-new)
