mm15-0145-the-united-states-of-america-1784.md
﻿---
title: The United States of America, 1784
parent: Maps, Movement, and American Literature
parentslug: maps-movement-and-american-literature
---

This hand-colored map by Amos Doolittle (1754-1832), a Boston engraver and silversmith, was one of the first national maps published in the United States immediately following the 1783 Peace of Paris that ended the Revolutionary War and guaranteed independence for the United States. Conceived as a fold-out map to be inserted into the nation’s first bestselling geography schoolbook, Jedidiah Morse’s _Geography Made Easy_ (1784), this map reached thousands of citizens, young and old alike, during the first decades after Independence.  
    Unlike previous maps showing geopolitical sections of North America, the Doolittle map is designed to present exclusively the territory of the nation-state. It therefore traces the international borders from the Atlantic Ocean in the east to the Mississippi River in the west, and from British Canada to the north and Spanish Florida to the South. At the same time it provides information about the interior, which, despite being rudimentary, offered Americans a first synoptic view of the union. Later national maps, often conceived as giant wall maps, were much more detailed. Combined with the textual apparatus and reading protocols of school geographies, these maps accompanied lessons in which students learned how to travel the nation in the virtual setting of a map: Oral recitation required students to match locations on the map with encyclopedic entries describing the geography of the land, rivers, and settlements.  
    Prefacing the map at the lower right is an ornamental cartouche that introduces through its visual narrative the relationship between map and literary mapping. Hovering in a swirl of billowing clouds, the female figure of Liberty (or Columbia) lifts, together with the help of the American eagle, a banner inscribed with the words “Aspera ad Astra”—which translates as “a rough road leads to the stars”—in order to unveil the new nation’s name (in bold print) while the land is shown beneath (in a pictorial sketch). When considered in juxtaposition to the map image, the cartouche suggests that as long as they are viewed separately the two discursive modes of either word or image will foster imaginings that are easily clouded and distorted. If used jointly, however—as the dual gesture of unveiling the geographic name of the nation and of the map image itself indicates—the cartouche celebrates the mixed language of the map as the ideal literary means for representing and traveling the nation in clear and unambiguous terms.

Selection Gallery
-----------------

[The United States of America, 1784](/web/20210127234142/http://mappingmovement.newberry.org/item/map-united-states-america)

**Citation:**

Doolittle, Amos, "A Map of the United States of America", in Jedidiah Morse's _Geography Made Easy_ (New-Haven : Printed by Meigs, Bowen and Dana, \[1784\]). Ayer 109.5 .M7 1784

[![](https://web.archive.org/web/20210127234142im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_44.jpg?itok=S3prWEPt)](https://web.archive.org/web/20210127234142/http://mappingmovement.newberry.org/item/map-united-states-america "The United States of America, 1784")

[View Full Metadata](/web/20210127234142/http://mappingmovement.newberry.org/item/map-united-states-america)
