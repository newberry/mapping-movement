mm20-0195-u-s-army-map-of-northwestern-georgia-1863.md
﻿---
title: U.S. Army Map of Northwestern Georgia, 1863
parent: Lines that Fracture and Fade: Two Centuries of Travel through Georgia, 1775-1976
parentslug: lines-fracture-and-fade-two-centuries-travel-through-georgia-1775-1976
---

The Civil War seriously taxed America’s mapmaking capacities. The rapid advances in cartographic printing under companies like S. Augustus Mitchell’s were still no match for the demands of supplying up-to-date information on American roads and topography to armies in the field. The United States thus had to mobilize whole departments in the Army Corps of Engineers and the War Bureau to produce maps. The development of lithographic printing greatly aided this mobilization, with stone more quickly and easily accepting the information from manuscript maps that the more laborious steel plates of the antebellum age. The United States had its own lithographic presses but still needed to contract with commercial firms such as Julius Bien and Company in New York, who produced this map.  
     Part of a collection of campaign maps the Newberry acquired as part of the collections of Union General Orville Babcock, this map and the others in the Babcock papers demonstrate the speed of such productions, with this map containing printed 1864 updates to a 1863 base map of northwestern Georgia. It was perhaps the most detailed map of this area of Georgia up to this point. Certainly it was the most detailed map of Georgia made since William Bonner’s 1851 _Map of the State of Georgia_. The careful attention paid to secondary roads and small towns provides a rich detail of where people lived and how they moved through the state in the middle decades of the nineteenth century.  
 

Selection Gallery
-----------------

[U.S. Army Map of Northwestern Georgia, 1863](/web/20210119185013/http://mappingmovement.newberry.org/item/northwestern-georgia-portions-adjoining-states-tennessee-and-alabama-being-part-department)

**Citation:**

United States. War Department. Engineer Bureau, "Northwestern Georgia (With Portions of the Adjoining States of Tennessee and Alabama) : Being Part of the Department of the Cumberland" \[Washington, D.C. : Engineer Bureau of the War Department, 1864?\]. VAULT MS map6F G3861.A 1863 .B3 no. 7

[![](https://web.archive.org/web/20210119185013im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_255.jpg?itok=KWQabxlQ)](https://web.archive.org/web/20210119185013/http://mappingmovement.newberry.org/item/northwestern-georgia-portions-adjoining-states-tennessee-and-alabama-being-part-department "U.S. Army Map of Northwestern Georgia, 1863")

[View Full Metadata](/web/20210119185013/http://mappingmovement.newberry.org/item/northwestern-georgia-portions-adjoining-states-tennessee-and-alabama-being-part-department)
