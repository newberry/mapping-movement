mm08-0074-new-france-1612.md
﻿---
title: New France, 1612
parent: European Maps for Exploration and Discovery
parentslug: european-maps-exploration-and-discovery
---

Like the English, the French were long deterred by the presence of Spaniards from attempting to settle in the more welcoming parts of the New World. In the 1530s Jacques Cartier (1489-1557) did make several attempts to establish a colony on the Saint Lawrence River; he was eventually repulsed not by the Spaniards but by the severity of the climate. In the 1560s colonies were briefly established in what is now Florida by Jean Ribaut and René de Laudonnière, but these rapidly failed, in the face of intense Spanish opposition. After that, from about 1560 to 1600 the people in France were much diverted by the wars of religion.  
     Permanent French settlement of the Saint Lawrence valley would be the work of Samuel de Champlain (c. 1570-1635), whose Canadian venture began in 1603, with the powerful support of king Henri IV (1589-1610) He was an explorer of remarkable tenacity, who understood not only the limitations of his French companions, but also the need to adapt to the peculiarly difficult circumstances of the Saint Lawrence valley. He also had to contend against the arguments of men like Sully, Henry IV’s all-powerful minister, who contended that “great riches are never found in places above forty degrees of latitude.”  
     Champlain was a remarkable cartographer, who compiled many detailed maps, some of which were printed in his book, _Les voyages du sieur de Champlain_ (Paris, 1613). He used these small-area manuscript maps to compile maps of much larger areas, like this _Carte de la nouvelle France_. It is extraordinary that a cartographer of the period could in ten years have compiled so extensive and precise a map, largely relying on his own travels and observations by land and sea, supplemented by what he could learn from Indian informants. The map begins to show weakness only in the far west, where Champlain still hoped that the rumored great body of water (“grand lac”) might lead directly to China.  
     This map also testifies to Champlain’s intense interest in the people and products of the country. He inserted four Indian figures, two of whom the engraver has made distinctly European in appearance; the right-hand couple’s facial features unmistakably resemble those of king Henri IV and his wife, Marie de Medici. He also drew and named a remarkable variety of indigenous fruits and vegetables. Champlain had learned his cartographic skills in the service of Henri IV (1589-1610) as lodgings-master, responsible for sketching out the towns and villages in which a French army on the march might be quartered. In this kind of mapping, essentially carried out out using pacing and map-bearings, Champlain would not have used references to latitude and longitude, which indeed are lacking from this great map.  
 

Selection Gallery
-----------------

[Quebec](/web/20210127231038/http://mappingmovement.newberry.org/item/quebec)

**Citation:**

Champlain, Samuel de, "Quebec" in _Voyages du Sieur de Champlain Xaintongeois, Capitaine Ordinaire du Roy, en la Marine_ (Paris : Chez Iean Berjon, 1613), between p. 176 and 177. VAULT Ayer 121 .C6 1613

[![](https://web.archive.org/web/20210127231038im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_19.jpg?itok=xF17nrqW)](https://web.archive.org/web/20210127231038/http://mappingmovement.newberry.org/item/quebec "Quebec")

[View Full Metadata](/web/20210127231038/http://mappingmovement.newberry.org/item/quebec)

[New France, 1612](/web/20210127231038/http://mappingmovement.newberry.org/item/carte-geographique-de-la-nouuelle-franse)

**Citation:**

Champlain, Samuel de. "Carte geographique de la Nouuelle Franse" removed from his Voyages du sieur de Champlain Xaintongeois, capitaine ordinaire du roy, en la marine. Paris : Chez Iean Berjon, 1613.

[![](https://web.archive.org/web/20210127231038im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_149.jpg?itok=XlE1kAgn)](https://web.archive.org/web/20210127231038/http://mappingmovement.newberry.org/item/carte-geographique-de-la-nouuelle-franse "New France, 1612")

[View Full Metadata](/web/20210127231038/http://mappingmovement.newberry.org/item/carte-geographique-de-la-nouuelle-franse)
