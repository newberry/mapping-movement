mm20-0199-principal-us-electric-transmission-lines-1968.md
﻿---
title: Principal US Electric Transmission Lines, 1968
parent: Lines that Fracture and Fade: Two Centuries of Travel through Georgia, 1775-1976
parentslug: lines-fracture-and-fade-two-centuries-travel-through-georgia-1775-1976
---

The Rand McNally Corporation compiled this rolled wall map showing the nation’s major powerlines and the regional systems of that operated them. At first appearance, it appears to be a wall-mounted road map of the continental US In 1967. (And Rand McNally) clearly drew upon its experience with road maps to create this image of a linked system of corridors defined by varying degrees of carrying capacity.) Given the rapidity with which America’s power infrastructure changed in the twentieth century, it is likely this map did not long serve its purpose, but it offers an intriguing glimpse at the ways in which US history grew alongside the movement of non-human elements (in this case electrons along high tension wires).

Selection Gallery
-----------------

[Principal US Electric Transmission Lines, 1968](/web/20210303203611/http://mappingmovement.newberry.org/item/principal-interconnected-transmission-lines-united-states)

**Citation:**

Edison Electric Institute, "Principal Interconnected Transmission Lines in the United States" (New York : Edison Electric Institute, \[1968\]). Maproll G3701.N4 1968 .E3

[![](https://web.archive.org/web/20210303203611im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_224.jpg?itok=GVxRhq4X)](https://web.archive.org/web/20210303203611/http://mappingmovement.newberry.org/item/principal-interconnected-transmission-lines-united-states "Principal US Electric Transmission Lines, 1968")

[View Full Metadata](/web/20210303203611/http://mappingmovement.newberry.org/item/principal-interconnected-transmission-lines-united-states)
