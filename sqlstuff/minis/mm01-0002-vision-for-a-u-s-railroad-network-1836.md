mm01-0002-vision-for-a-u-s-railroad-network-1836.md
﻿---
title: Vision for a U.S. Railroad Network, 1836
parent: American Railroad Maps, 1828-1876
parentslug: american-railroad-maps-1828-1876
---

Michel Chevalier toured the United States in 1835-1836 on assignment from the French Ministry of Public Works. A firm believer in the potential of modern technology and public works to improve the quality of life, Chevalier produced this map, versions of which appeared in several of his publications and reports, to spur railroad construction back home. At the time of its publication French engineers and geographers were pioneering new cartographic techniques, now known as thematic mapping, to help bureaucrats, policymakers, and financial interests better understand the physical and social geography, including patterns of trade and movement. Chevalier’s map was among the first to envision the fragmentary collection of mostly local American railroads east of the Appalachian Mountains as part of a national system, still in the making.  
     Chevalier envisioned a spine of railroads linking major cities and ports along the Atlantic coast, Boston (still isolated from the rest of the system on this map), New York, Philadelphia, Baltimore, Norfolk, and Charleston. On Chevalier’s map there are significant gaps, and he combined railroads and canals, both proposed and existing, to fill out the system. This combination of railroads, canals, and sometimes roads and turnpikes would remain characteristic of American general and transportation maps for another two decades. These, published by Henry Schenk Tanner, Samuel Augustus Mitchell, and others, were gradually replaced by railroad maps as the trains asserted their dominance. Thus one can start with Chevalier’s national frame and periodically update it with similar systematic maps, decade by decade and watch the rails pull the nation together even as other forces were pulling it apart. Chevalier may have consulted William Norris’s _Map of the Railroads and Canals...in the United States_, (1834) “drawn and engraved for D. K. Minor, editor of the Railroad Journal.” This large sheet, however, did not conceive of the individual routes as parts of a whole. Instead it featured insets showing the profiles of the major routes, almost all of them waterways. The French visitor, however, saw the advantage of railroads over canals. “I know of nothing that gives a higher idea of the power of man,” he declared, later pointing out that railroads would transform the United States into a continental power. “Distance,” he concluded “will be annihilated, and this colossus, ten times greater than France, will preserve its unity without an effort.”  

Selection Gallery
-----------------

[Carte des États-unis d'Amérique, 1836](/web/20210119191622/http://mappingmovement.newberry.org/item/carte-des-%C3%A9tats-unis-dam%C3%A9rique-1836)

**Citation:**

Bouffard, L., “Carte Générale des États-Unis d’Amèrique, 1836”, in Michel Chevalier, _Les Voies de Communication aux États-Unis_ (Paris: F.G. Levrault), at end. H 6083 .16

[![](https://web.archive.org/web/20210119191622im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/MM01-0002-01.jpg?itok=mmIKlr4G)](https://web.archive.org/web/20210119191622/http://mappingmovement.newberry.org/item/carte-des-%C3%A9tats-unis-dam%C3%A9rique-1836 "Carte des États-unis d'Amérique, 1836")

[View Full Metadata](/web/20210119191622/http://mappingmovement.newberry.org/item/carte-des-%C3%A9tats-unis-dam%C3%A9rique-1836)
