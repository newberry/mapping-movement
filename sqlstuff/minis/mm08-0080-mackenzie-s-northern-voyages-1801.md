mm08-0080-mackenzie-s-northern-voyages-1801.md
﻿---
title: MacKenzie's Northern Voyages, 1801
parent: European Maps for Exploration and Discovery
parentslug: european-maps-exploration-and-discovery
---

Towards the end of the eighteenth century, the northwest Pacific coast was well mapped, thanks not only to James Cook (1728-1779) but also to George Vancouver (1757-1798), and in the heart of North America a variety of individuals, mostly hunting for fur-bearing animals, had reached the eastern edge of the Rocky Mountains. As yet, however, no European map had succeeded in joining these two regions cartographically.  
     This would be the work of Andrew Mackenzie (1764-1820). Born in Scotland, he emigrated to North America in 1774. Beginning in New York, in 1778 he went to work for a fur-trading company in Montréal. In 1785 he was sent north to Athabasca, and four years later founded Fort Chipewyan, seen on our map where the yellow track joins the red. Mackenzie was very curious to know how the Pacific Ocean might be reached from this site, and in 1789 set out on an exploratory voyage (see the northerly red track), in which he ended up, to his dismay, on the Arctic Ocean.  
     One of his problems on his voyage had been the difficulty of accurately calculating longitude, and in 1791 he went for advice to London, where he bought a chronometer. This was of great use on his second voyage (1792-3), in which he successfully traversed the Rocky Mountains to the Pacific Ocean, emerging at Bella Coola (now in British Columbia). Mackenzie thus showed the way to the Pacific Ocean ten years before the better-known travels of Lewis and Clark (1805-6), publishing his work in 1801 with the title _Voyages from Montreal on the River St. Lawrence_. Unlike many explorers, he successfully returned to his native land, Scotland, and enjoyed a long and prosperous retirement.  
     For Mackenzie, “America” was in effect constituted by what we now know as Canada, and his mapping of this vast area reflects the Europeans’ new-found skill at delineating whole continents, with quite small inlets shown with precision, and the whole structure netted in to the system of latitude and longitude. Few explorers’ travels have been shown with such elegance and precision.  
 

Selection Gallery
-----------------

[MacKenzie's Northern Voyages, 1801](/web/20210127220146/http://mappingmovement.newberry.org/item/map-america-between-latitudes-40-and-70-north-and-longitudes-45-and-180-west-exhibiting)

**Citation:**

Mackenzie, Alexander, "A Map of America Between Latitudes 40 and 70 North and Longitudes 45 and 180 West, Exhibiting Mackenzie’s Track from Montreal to Fort Chipewyan & from Thence to the North Sea in 1789, & to the West Pacific Ocean in 1793", in _Voyages from Montreal on the River St. Laurence, Through the Continent of North America to the Frozen and Pacific Oceans in the Years 1789 and 1793_ (London : Printed for T. Cadell, Jun. and W. Davies ..., 1801), map 1. Folio Graff 2630

[![](https://web.archive.org/web/20210127220146im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_87.jpg?itok=l7gBATfX)](https://web.archive.org/web/20210127220146/http://mappingmovement.newberry.org/item/map-america-between-latitudes-40-and-70-north-and-longitudes-45-and-180-west-exhibiting "MacKenzie's Northern Voyages, 1801")

[View Full Metadata](/web/20210127220146/http://mappingmovement.newberry.org/item/map-america-between-latitudes-40-and-70-north-and-longitudes-45-and-180-west-exhibiting)

[MacKenzie's Northern Voyages, 1801](/web/20210127220146/http://mappingmovement.newberry.org/item/map-mackenzie%E2%80%99s-track-fort-chipewyan-north-sea-1789)

**Citation:**

Mackenzie, Alexander. A map of Mackenzie’s track from Fort Chipewyan to the North Sea, in 1789 (London : Alexander MacKenzie, 1801).

[![](https://web.archive.org/web/20210127220146im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_144.jpg?itok=0VOWVhUV)](https://web.archive.org/web/20210127220146/http://mappingmovement.newberry.org/item/map-mackenzie%E2%80%99s-track-fort-chipewyan-north-sea-1789 "MacKenzie's Northern Voyages, 1801")

[View Full Metadata](/web/20210127220146/http://mappingmovement.newberry.org/item/map-mackenzie%E2%80%99s-track-fort-chipewyan-north-sea-1789)

[MacKenzie's Northern Voyages, 1801](/web/20210127220146/http://mappingmovement.newberry.org/item/map-mackenzie%E2%80%99s-track-fort-chipewyan-pacific-ocean-1793)

**Citation:**

Mackenzie, Alexander. A map of Mackenzie’s track from Fort Chipewyan to the Pacific Ocean, in 1793 (London : Alexander MacKenzie, 1801).

[![](https://web.archive.org/web/20210127220146im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_86.jpg?itok=jiGq5AON)](https://web.archive.org/web/20210127220146/http://mappingmovement.newberry.org/item/map-mackenzie%E2%80%99s-track-fort-chipewyan-pacific-ocean-1793 "MacKenzie's Northern Voyages, 1801")

[View Full Metadata](/web/20210127220146/http://mappingmovement.newberry.org/item/map-mackenzie%E2%80%99s-track-fort-chipewyan-pacific-ocean-1793)
