mm05-0046-the-union-pacific-railroad-under-construction-1866.md
﻿---
title: The Union Pacific Railroad under Construction, 1866
parent: State and Federal Mapping of Infrastructure and Movement
parentslug: state-and-federal-mapping-infrastructure-and-movement
---

This map, made by Grenville Mellen Dodge, captures a critical moment in North American history when US ambitions to build a transcontinental railroad directly clashed with Native people's lives and livelihoods on the high plains. When the Civil War ended in 1865, Dodge was commanding the Department of the Missouri, which was quickly expanded to include several western territories that lay in the path of the railroad. By the summer of 1865 Dodge was made military commander of the Plains, which included Kansas and the territories of Colorado, Utah, Nebraska, Wyoming, and Montana. Dodge set out to map not just the topography of this vast region, but also its military posts, mining districts, and most importantly the ongoing construction of the Union Pacific Railroad.  
     Given the importance that Dodge attached to the railroad, his priority was to address the threat to its construction posed by Native Americans who opposed US efforts to construct a transcontinental railroad and telegraph. The map refers to this area as the “Military Department of the Platte,” and it indicates the degree to which the federal government—through the army—was critical to the growth of the railroad into the West. Throughout 1866 the military established several forts to protect the Bozeman Trail from Sioux and Cheyenne Indians, including Forts Reno, Kearney, and Smith. But a particularly harsh winter in 1866-1867—coupled with the reduction in forces brought by the end of the Civil War—made it difficult for the military to communicate with and control these forts. Dodge sent soldiers to each of these forts, though attacks by the Sioux led the military to abandon all of them by 1868. Dodge also posted soldiers at depots along the South Platte in order to protect the trail and restore the telegraph line from Omaha to Denver. Here he met with greater success in stabilizing the region for railroad construction. The interest in movement is also apparent in the elaborate tables that list distance for several different overland routes.  
     In May 1866 Dodge resigned from the military and became the Chief Engineer of the Union Pacific Railroad. Full-scale construction of the road began early in 1866, and this map reveals his engagement with discussions regarding the possible route over the Rocky Mountains. The map was actually completed in January 1866, when Dodge was still in uniform, yet the intelligence was mostly gathered by railroad engineers. In fact, this blurring of the distinction between the aims of the military and those of the railroad is instructive, for the federal government was crucial to the completion of the railroad; whether supplying protection, labor, or the talents of its surveyors such as Dodge, the military and its soldiers were necessary for this massive project to be undertaken. The map was completed in 1866, but extensive annotations were made the following year, many taken from a map made by Christopher Colon Augur, commander of the region from 1867 to 1871.  
 

Selection Gallery
-----------------

[Map of the military district, Kansas and the territories](/web/20210120031804/https://mappingmovement.newberry.org/item/map-military-district-kansas-and-territories)

**Citation:**

Dodge, Grenville Mellen, "Map of the Military District, Kansas and the Territories" (manuscript, 1866). VAULT Case MS folio G4050 1866 .D6

[![](https://web.archive.org/web/20210120031804im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_16.jpg?itok=KM-h4NXa)](https://web.archive.org/web/20210120031804/https://mappingmovement.newberry.org/item/map-military-district-kansas-and-territories "Map of the military district, Kansas and the territories")

[View Full Metadata](/web/20210120031804/https://mappingmovement.newberry.org/item/map-military-district-kansas-and-territories)
