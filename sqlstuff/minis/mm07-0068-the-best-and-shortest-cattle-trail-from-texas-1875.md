mm07-0068-the-best-and-shortest-cattle-trail-from-texas-1875.md
﻿---
title: The Best and Shortest Cattle Trail from Texas, 1875
parent: Maps of Trails and Roads of the Great West
parentslug: maps-trails-and-roads-great-west
---

This map, prepared for and published by the Kansas and Pacific Railway, is both functional and promotional. It provides a summary of the great cattle trails stretching from Texas north to the Kansas railheads. Most of the trails are shown at least in part in a network but are individually unnamed. And what is prominently labeled as the “Ellsworth Cattle Trail” generally follows the more famous Chisholm Trail laid down by Texas rancher Jesse Chisholm. On the other hand, “J.S. Chisum’s Trail” shown in New Mexico is actually that pioneered by the Lincoln County rancher John Chisum. In this regard, the map is advertising Kansas Pacific Railway’s availability to the routes of the cattle trails. These are clearly marked routes to be followed by ranchers to market their beef east and west.  
     This map also can be seen as a booster map and a souvenir intended to promote the Kansas Pacific Railway in general. It was issued folded inside of the back cover of a guide to the Railway and trails leading to it. The delightful title cartouche of a Texas longhorn steer with the Railway’s name on a banner spanning its magnificent horns and the title of the map on a Texas lone star badge hanging from a ring in the steer’s nose clearly underscores the map’s publicity function.   

Selection Gallery
-----------------

[The Best and Shortest Cattle Trail from Texas, 1875](/web/20210303215449/http://mappingmovement.newberry.org/item/kansas-pacific-railway-best-and-shortest-cattle-trail-texas)

**Citation:**

Kansas Pacific Railway Company, "Kansas Pacific Railway : the Best and Shortest Cattle Trail from Texas" (Kansas City, MO : K.C. Lith. Co., 1875). VAULT Graff 2275

[![](https://web.archive.org/web/20210303215449im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_69.jpg?itok=f9EtWGEC)](https://web.archive.org/web/20210303215449/http://mappingmovement.newberry.org/item/kansas-pacific-railway-best-and-shortest-cattle-trail-texas "The Best and Shortest Cattle Trail from Texas, 1875")

[View Full Metadata](/web/20210303215449/http://mappingmovement.newberry.org/item/kansas-pacific-railway-best-and-shortest-cattle-trail-texas)
