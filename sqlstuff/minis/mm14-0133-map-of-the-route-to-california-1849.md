mm14-0133-map-of-the-route-to-california-1849.md
﻿---
title: Map of the Route to California, 1849
parent: Mapping Migration and Settlement
parentslug: mapping-migration-and-settlement
---

A third type of travel guide promoting migration provides directions for traveling along a single route. One such example is Joseph Ware's _The Emigrant Guide to California_, published in St. Louis in 1849. In outlining the primary route from Independence, Missouri, to the California gold fields, this guide documents one aspect of the westward migration within the United States, whether the emigrants intended to stay a short time for quick personal gain or to make a western location their permanent residence.  
     In a version edited by John Caughey and reprinted by Princeton University Press in 1932, the editor recognized that this work was the first travel guide to provide a complete description of the primary route to California for the group of prospective miners known as "forty-niners," and it continued to be the best source of travel information for several years. It appears that Ware did not travel this route himself before he composed this travel guide. Rather, he based his narrative and advice on information that he gathered from other sources, but primarily relied upon the published accounts of John C. Fremont's first two expeditions in 1842 and 1843-44. As a synthesis of information, it provided a good description of the route, but was not without errors.  
     In the first part of Ware's presentation, he discussed various options for travelling from the eastern United States to California, including by sea around the southern tip of South America, by sea and land across the Isthmus of Panama or Mexico, or overland, starting in Independence, Missouri. He compared modes of transportation, costs, and length of traveling time, concluding that the overland route was the best for residents of the Midwestern states. For those who chose the latter route, he suggested the best times for departure so that the trek could be concluded before winter snows. He gave other practical advice, such as the type and quantity of supplies that would be needed and how to set up a camp of wagons in order to provide protection from attack by Native Americans.  
     The remainder of the guide provided a running narrative describing the route, generally presenting information about distances, grades, water crossings, availability of potable water, and camp site locations. For example, in Kansas, he noted "Little Blue, twenty-eight miles from the Big Blue. It is fifty feet wide, timber plenty, grass and water good. You are now in Pawnee country. Watchfulness is required to prevent their stealing your stock. Your camp must be well guarded every night, and your stock caraled (sic), if necessary."  
     This little guide book is accompanied by a large fold-out map showing the full extent of the route, as well as a profile recording elevations along the route. For anyone familiar with the mapping of western exploration, it is readily apparent that this map is an almost exact copy of the map prepared by German immigrant Charles Preuss to accompany John C. Fremont’s report, published as a Congressional Serial document in 1845 (Grim 1994, 100-102). Ware made only two substantial changes to the Fremont/Preuss map. First, he added an inset diagram of the Midwestern states outlining the primary routes across those states to Independence. Secondly, he sketched the southerly route from Fort Hall (Idaho) across Nevada’s Great Basin to San Francisco, rather than the northerly route from Fort Hall to the Oregon Territory, as was depicted on the Fremont map. For “forty-niners,” Ware’s map was more useful than Fremont’s because it traced the complete trail to California.  
 

Selection Gallery
-----------------

[Map of the route to California : compiled from accurate observations ...](/web/20210303214422/http://mappingmovement.newberry.org/item/map-route-california-compiled-accurate-observations-and-surveys-government)

**Citation:**

Ware, Joseph E., "Map of the Route to California : Compiled from Accurate Observations and Surveys by Government", detached from _The Emigrants' Guide to California_ (St. Louis, Mo. : J. Halsall, \[1849\]). Graff 4538

[![](https://web.archive.org/web/20210303214422im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_77.jpg?itok=23SKwO-R)](https://web.archive.org/web/20210303214422/http://mappingmovement.newberry.org/item/map-route-california-compiled-accurate-observations-and-surveys-government "Map of the route to California : compiled from accurate observations ...")

[View Full Metadata](/web/20210303214422/http://mappingmovement.newberry.org/item/map-route-california-compiled-accurate-observations-and-surveys-government)
