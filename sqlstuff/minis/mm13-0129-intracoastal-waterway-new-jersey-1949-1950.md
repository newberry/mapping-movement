mm13-0129-intracoastal-waterway-new-jersey-1949-1950.md
﻿---
title: Intracoastal Waterway, New Jersey, 1949-1950
parent: Waterways Cartography, Part II: Landmarks and Exemplars in North America
parentslug: waterways-cartography-part-ii-landmarks-and-exemplars-north-america
---

The United States Coast and Geodetic Survey has a long history extending back to the nation’s founding. Its original function, to chart the nation’s Atlantic coast, addressed the needs of ocean-going ships, but the submerged coastal plain offered many opportunities for these ships to sail far inland, utilizing the bays, rivers, and sounds, all protected by barrier islands and peninsulas extending from the mainland. The original mapping of this broken coastline, going back to the efforts of John Smith and other early explorers, encouraged navigators to think about connecting these inland venues to create a protected waterway paralleling the coastline.  
     Some of the earliest proposals for canals in North America focused on these links. William Byrd II of Westover made one such recommendation in 1728. Thus, we find that American canal history, in its origins, may be divided into canals that reached into the interior of the continent and those that linked sheltered waters along the Atlantic and Gulf Coastal Plains. Another early focus for the improvement of interior coastal waters envisioned connecting Albemarle Sound with Chesapeake Bay. Later the Chesapeake and Delaware Canal linked these two bays along the Mid-Atlantic Coast, and the Delaware and Hudson Canal provided a final link that could be extended into the Midcontinent by way of the Erie Canal.  
     These coastal linkages found cartographic expression in a series of documents focusing on individual links in what eventually became a system called the Atlantic Intracoastal Waterway in the early twentieth century. The US Coastal and Geodetic Survey regularly published navigation charts and maps of these inland passages, and its book-length _Inside Route Pilot: Intracoastal Waterway, New York to Key West,_ provided commentary and directions for navigators, both professional and recreational. It appeared in many editions starting in 1912.  
     The three charts provided here represented both the second and fourth edition of the series covering New Jersey waters, first issued in 1940. A navigator in the summer of 1949 would use these connected sheets to ply the protected waters, starting from the Manasquan Inlet, the northern entrance to the waterway from the Atlantic Ocean. Sheet 825 (the fourth edition of the 1940 base map, corrected to July, 1946) uses two panels to show the route to Little Egg Harbor. The line of navigation, printed in red, utilizes navigational bearings, and a compass showing the magnetic variation in 1950 is printed on every sheet.  
     Chart 826 continues the navigation line to Longport, just south of Atlantic City. In this case the one map takes up the whole sheet. Again, it is the fourth edition of this sheet, but it carries a date of June 1949. Sheet 827, which completes the route to Cape May, was only in its second edition, updated to October 1943. A blue stamped date of June 12, 1950 at the bottom of the sheet assured navigators that the “lights, beacons, buoys, and dangers \[noted on the map were\] corrected for information received to the following date.” This sheet used three maps, two to bring the series to Cape Map and a third tracing an extension of the Great Egg River westward to May’s Landing.  
     The information provided on these navigation charts directly addressed the needs of navigators: water depths, in feet at mean low water, locations of navigation aids; heights in feet of bridge clearances; distances in nautical miles; dangers such as rocks, wrecks, shifting sand bars; and a grid of latitude and longitude at five-minute intervals. On land, the road pattern is clearly indicated with every street shown for urban areas and villages. Place names abound, and navigation landmarks like water tanks, towers, large buildings, docks, and harbors are always identified.  
     These coastal charts are of great interest to historians because they present a detailed portrayal of both the waters themselves and the development of the littoral. They were also revised, periodically updated, and appeared in many editions, thus documenting coastal changes for almost a century. Various issues of the _Inside Route Pilot_ also contain a valuable text with information on the use of the waterway, current problems in navigating the route, improvements in process, advice on finding supplies and provisions, and historical details with notes on ruins or relic features both in the water and along the shore.  
     For example, the 1920 (second) edition of the _Inside Route Pilot: Coast of New Jersey_ suggests that a “stranger” entering the Barnegat Inlet use a pilot, “either picking up a fisherman outside or setting signal and waiting for one from Barnegat City.” (12) Once inside the waterway, a boat seeking a place for a stop should know that the summer resort at the end of the railway at Barnegat City includes “a permanent settlement of oystermen and fishermen.” To reach this complex, however, a boater needed to use “a privately dredged channel \[which\] leads from the main channel southward to the landings on the inner branch just inside the point.” These directions were of obvious value to someone in need in 1920 but also open windows for the historian or story writer of a later date.  
     The information given in the larger book for the entire route from New York to Key West does not usually carry the same amount of detail but nevertheless can be used to put the information from other editions into various contexts. For example, if we check the comments on the Barnegat Inlet in the eighth edition (1936), the major issue was prevailing northeast winds when “the channel changes so rapidly that directions are impossible,” and the inlet, “unless unusually smooth, …is then impossible for any type of boat to enter” (15).  

Selection Gallery
-----------------

[Intracoastal Waterway, New Jersey, 1949-1950](/web/20210119192959/https://mappingmovement.newberry.org/item/new-jersey-intracoastal-waterway-little-egg-harbor-longport)

**Citation:**

U.S. Coast and Geodetic Survey, "New Jersey Intracoastal Waterway : Little Egg Harbor to Longport" (Washington, D.C. : U.S. Coast and Geodetic Survey, 1949). Map6F G3812.C6 1949 .U5

[![](https://web.archive.org/web/20210119192959im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_178.jpg?itok=PLbhoOUi)](https://web.archive.org/web/20210119192959/https://mappingmovement.newberry.org/item/new-jersey-intracoastal-waterway-little-egg-harbor-longport "Intracoastal Waterway, New Jersey, 1949-1950")

[View Full Metadata](/web/20210119192959/https://mappingmovement.newberry.org/item/new-jersey-intracoastal-waterway-little-egg-harbor-longport)

[Intracoastal Waterway, New Jersey, 1949-1950](/web/20210119192959/https://mappingmovement.newberry.org/item/new-jersey-intracoastal-waterway-manasquan-inlet-little-egg-harbor)

**Citation:**

U.S. Coast and Geodetic Survey, "New Jersey Intracoastal Waterway : Manasquan Inlet to Little Egg Harbor" (Washington, D.C. : U.S. Coast and Geodetic Survey, 1946 \[i.e. 1950?\]). Map6F G3812.C6 1950 .U5

[![](https://web.archive.org/web/20210119192959im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_172.jpg?itok=szsD0z9m)](https://web.archive.org/web/20210119192959/https://mappingmovement.newberry.org/item/new-jersey-intracoastal-waterway-manasquan-inlet-little-egg-harbor "Intracoastal Waterway, New Jersey, 1949-1950")

[View Full Metadata](/web/20210119192959/https://mappingmovement.newberry.org/item/new-jersey-intracoastal-waterway-manasquan-inlet-little-egg-harbor)

[Intracoastal Waterway, New Jersey, 1949-1950](/web/20210119192959/https://mappingmovement.newberry.org/item/new-jersey-intracoastal-waterway-longport-cape-may)

**Citation:**

U.S. Coast and Geodetic Survey, "New Jersey Intracoastal Waterway : Longport to Cape May" (Washington, D.C. : U.S. Coast and Geodetic Survey, 1943 \[i.e. 1949?\]). Map6F G3812.C6 1949 .U55

[![](https://web.archive.org/web/20210119192959im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_237.jpg?itok=o-VXI4Fv)](https://web.archive.org/web/20210119192959/https://mappingmovement.newberry.org/item/new-jersey-intracoastal-waterway-longport-cape-may "Intracoastal Waterway, New Jersey, 1949-1950")

[View Full Metadata](/web/20210119192959/https://mappingmovement.newberry.org/item/new-jersey-intracoastal-waterway-longport-cape-may)
