mm05-0044-fremont-s-exploration-of-the-oregon-trail-1846.md
﻿---
title: Fremont’s Exploration of the Oregon Trail, 1846
parent: State and Federal Mapping of Infrastructure and Movement
parentslug: state-and-federal-mapping-infrastructure-and-movement
---

In the late 1830s and early 1840s thousands of US settlers began to migrate overland to a region of the Pacific Northwest they called Oregon Country. The migrants brought their sense of national identity, and with it a sense that the region might not always be controlled by British interests or local First Nations. Part of this migration had been facilitated by John C. Fremont, whose expeditions greatly expanded knowledge of the Oregon Trail and the North American interior more generally. In 1846 Senator David Atchison, keen to develop US interests in the Pacific Northwest, asked Fremont to provide him with a map of the Oregon Trail. In response, Fremont’s skilled cartographer, German-born Charles Preuss, set to work and by December had produced a seven-sheet map that featured the topography of the trail as well as extensive information on game, water, rainfall, and the presence of Native peoples.  
     These seven sheets provided detailed information for actual and armchair migrants alike. Notice the care that Preuss took to include relevant information from Fremont’s Report, such as local climate. This information was still relatively scarce, as average temperature and rainfall maps of the American west would not be accessible until the mid-1850s. Here that information enables the migrant—or the potential migrant—to actually envision himself on the trail, and to understand firsthand the conditions of the overland journey.  
     Each of the seven sections details not just the topography, but the presence or absence of fuel, game, and water. Preuss also included the map with Fremont’s discussions of Native Americans, and the potential danger that they posed to migrants. With these lengthy asides the Preuss maps capture at least some of the drama of the West. On Section II Fremont describes the “strange emotion of grandeur” that came when the expedition spotted a large herd of bison: “There was not one among us who did not hear his heart beat quicker.”  
     Section III includes warnings about the harsh conditions of the area, where “no game is to be found” and “Sioux Indians are not to be trusted.” Little surprise then that Fremont advocated a stronger Federal presence and show of force in the Interior West in order to aid the nation’s westward expansion. By the time the maps were printed and distributed in late 1846, much of the area south of the 49th Parallel had come under federal jurisdiction as the nation’s newest territory and its foothold on the Pacific Coast, though further treaties would be negotiated with local First Nations in the decades to follow.  
 

Selection Gallery
-----------------

[Fremont’s Exploration of the Oregon Trail, 1846](/web/20210119184343/http://mappingmovement.newberry.org/item/topographical-map-road-missouri-oregon-commencing-mouth-kansas-missouri-river-and-ending-mouth)

**Citation:**

Preuss, Charles, "Topographical Map of the Road from Missouri to Oregon, Commencing at the Mouth of the Kansas in the Missouri River and Ending at the Mouth of the Wallah Wallah in the Columbia : in VII sections" (Baltimore: Lithogr. by E. Weber & Co., 1846), issued separately to accompany John Charles Frémont, _Report of the Exploring Expedition to the Rocky Mountains in the Year 1842, and to Oregon and North California in the Years 1843-’44_, Serial Set 461, U.S. 28th Congress, 2nd Session, Senate executive document 174 (Washington : Gales and Seaton, 1845). Graff 3360

[![](https://web.archive.org/web/20210119184343im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster.jpg?itok=GUJShSsa)](https://web.archive.org/web/20210119184343/http://mappingmovement.newberry.org/item/topographical-map-road-missouri-oregon-commencing-mouth-kansas-missouri-river-and-ending-mouth "Fremont’s Exploration of the Oregon Trail, 1846")

[View Full Metadata](/web/20210119184343/http://mappingmovement.newberry.org/item/topographical-map-road-missouri-oregon-commencing-mouth-kansas-missouri-river-and-ending-mouth)

[![](https://web.archive.org/web/20210119184343im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_0.jpg?itok=hsrQpN2Z)](https://web.archive.org/web/20210119184343/http://mappingmovement.newberry.org/item/topographical-map-road-missouri-oregon-commencing-mouth-kansas-missouri-river-and-ending-mouth "Fremont’s Exploration of the Oregon Trail, 1846")

[View Full Metadata](/web/20210119184343/http://mappingmovement.newberry.org/item/topographical-map-road-missouri-oregon-commencing-mouth-kansas-missouri-river-and-ending-mouth)

[![](https://web.archive.org/web/20210119184343im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_1.jpg?itok=6Q2yCgXC)](https://web.archive.org/web/20210119184343/http://mappingmovement.newberry.org/item/topographical-map-road-missouri-oregon-commencing-mouth-kansas-missouri-river-and-ending-mouth "Fremont’s Exploration of the Oregon Trail, 1846")

[View Full Metadata](/web/20210119184343/http://mappingmovement.newberry.org/item/topographical-map-road-missouri-oregon-commencing-mouth-kansas-missouri-river-and-ending-mouth)

[![](https://web.archive.org/web/20210119184343im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_2.jpg?itok=_z5In14V)](https://web.archive.org/web/20210119184343/http://mappingmovement.newberry.org/item/topographical-map-road-missouri-oregon-commencing-mouth-kansas-missouri-river-and-ending-mouth "Fremont’s Exploration of the Oregon Trail, 1846")

[View Full Metadata](/web/20210119184343/http://mappingmovement.newberry.org/item/topographical-map-road-missouri-oregon-commencing-mouth-kansas-missouri-river-and-ending-mouth)

[![](https://web.archive.org/web/20210119184343im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_3.jpg?itok=-H_cooy6)](https://web.archive.org/web/20210119184343/http://mappingmovement.newberry.org/item/topographical-map-road-missouri-oregon-commencing-mouth-kansas-missouri-river-and-ending-mouth "Fremont’s Exploration of the Oregon Trail, 1846")

[View Full Metadata](/web/20210119184343/http://mappingmovement.newberry.org/item/topographical-map-road-missouri-oregon-commencing-mouth-kansas-missouri-river-and-ending-mouth)

[![](https://web.archive.org/web/20210119184343im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_4.jpg?itok=OEh_y9ei)](https://web.archive.org/web/20210119184343/http://mappingmovement.newberry.org/item/topographical-map-road-missouri-oregon-commencing-mouth-kansas-missouri-river-and-ending-mouth "Fremont’s Exploration of the Oregon Trail, 1846")

[View Full Metadata](/web/20210119184343/http://mappingmovement.newberry.org/item/topographical-map-road-missouri-oregon-commencing-mouth-kansas-missouri-river-and-ending-mouth)

[![](https://web.archive.org/web/20210119184343im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_6.jpg?itok=v9iKu0V0)](https://web.archive.org/web/20210119184343/http://mappingmovement.newberry.org/item/topographical-map-road-missouri-oregon-commencing-mouth-kansas-missouri-river-and-ending-mouth "Fremont’s Exploration of the Oregon Trail, 1846")

[View Full Metadata](/web/20210119184343/http://mappingmovement.newberry.org/item/topographical-map-road-missouri-oregon-commencing-mouth-kansas-missouri-river-and-ending-mouth)

[![](https://web.archive.org/web/20210119184343im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_5.jpg?itok=_bhGTlYZ)](https://web.archive.org/web/20210119184343/http://mappingmovement.newberry.org/item/topographical-map-road-missouri-oregon-commencing-mouth-kansas-missouri-river-and-ending-mouth "Fremont’s Exploration of the Oregon Trail, 1846")

[View Full Metadata](/web/20210119184343/http://mappingmovement.newberry.org/item/topographical-map-road-missouri-oregon-commencing-mouth-kansas-missouri-river-and-ending-mouth)
