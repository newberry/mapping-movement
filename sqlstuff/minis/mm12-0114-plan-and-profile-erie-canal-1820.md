mm12-0114-plan-and-profile-erie-canal-1820.md
﻿---
title: Plan and Profile, Erie Canal, 1820
parent: Waterways Cartography, Part I: The Mapping of North America's Internal Navigation Systems
parentslug: waterways-cartography-part-i-mapping-north-americas-internal-navigation-systems
---

Joseph Louis Etienne Cordier, a young French engineer who did outstanding work on two of Napoleon’s transportation projects, one a highway and the other a canal, soon headed the northern department of the French corps of engineers. He spent his entire career pushing a variety of projects to completion, including over twenty canals, river improvements, and harbors. To stimulate public support for these projects, and to buttress a master plan for the development of canals and waterways in France, Cordier translated from English into French the 1803 edition of John Phillips’s classic _A_ _General History of Inland Navigation_. The volume appeared in Paris in 1819, packed with many diversions sounding Cordier’s favorite themes.  
      At the time of its publication, Albert Gallatin, the US Minister to France, informed the French engineer about his own interest in internal improvements, including his important “Report on Roads and Canals” presented to Congress in 1808 when he was Secretary of the Treasury. When FranVois Becquey, Cordier’s superior, issued his influential Report to the King in 1820, it bore some striking resemblances to the Gallatin Report made a dozen years earlier in America. By then Cordier had compiled a second volume of his translations of important canal documents from English into French, featuring Gallatin’s 1808 report and including documents related to the Grand Canal in New York. The sheer size of the Erie Canal, 350 miles in length, plus its enormous cost, staggered the Frenchman’s imagination. His book needed a map of that magnificent undertaking to underscore his major point: France needed a system of waterways and the Americans had shown the way.  
      It was reported that Cordier completed his second volume in 1819 but a printing issue delayed its appearance until the following year. Perhaps the maps caused the delay. The earliest printed maps of the Erie Canal appeared in the Western Canal Commission Report in 1817. Copies must have soon crossed the ocean, perhaps addressed to Albert Gallatin, and arrived in Paris in time for Cordier to have them adapted for a French audience and attached to his waiting second volume.  
      The Canal Commissioners would not issue “A New Map and Profile of the Proposed Canal from Lake Erie to the Hudson River” until 1821.  And, to my knowledge, a set of maps equivalent to the three published in Paris would not appear until 1825. These were featured in Cadwallader D. Colden’s memorial volume for the “Wedding of the Waters” ceremony in New York Harbor on October 26, 1825.  
      Cordier’s set of maps carries the title “Carte du Nord Des États Unis et ou Grand Canal de Communication….” It is made up of three separate parts. The major map shows the route of the canal between Lake Erie and the Hudson River, but its regional context extends from Montreal to Lake Michigan and south to Washington, DC and the mouth of the Ohio River. The second element in the set of maps is a profile or “Nivellement” of the Grand Canal. It is somewhat sketchy to be sure and does not indicate the locks that would connect one pool of water with the next.  Instead the chart shows the elevation for each pool of water.  This information is then repeated in outline form on the “Plan et Profil du Canal…”, printed on a separate sheet, on which the waterway route is directly related to a profile by a series of vertical lines. On this sheet the place names are not translated into French, but apparently follow information supplied by the Canal Commissioners.  
      Given the early date of the French map, much of the data is very tentative, and many aspects of the plan had to be changed, especially in the western reaches of the canal. But as it stands, Cordier’s set of maps, appearing at such an early date, in Paris no less, points to the international importance of the Erie Canal and its monumental role in the global story of public works.  

Selection Gallery
-----------------

[Plan and Profile, Erie Canal, 1820](/web/20201126090205/https://mappingmovement.newberry.org/item/carte-du-nord-des-e%CC%81tats-unis-et-du-grand-canal-de-communication-du-lac-erie%CC%81-a%CC%80-la-rivie%CC%80re-d)

**Citation:**

Cordier, Joseph, "Carte du Nord des États-Unis : et du Grand Canal de Communication du Lac Erié à la Rivière d’Hudsons in Histoire de la Navigation Intérieure, et Particuliérement de Celle des États-Unis d’Amérique" in _Histoire de la Navigation Intérieure_ (Paris : Firmin Didot, 1820), v. 2, map 1. H 69 .686 v. 2

[![](https://web.archive.org/web/20201126090205im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_32.jpg?itok=JQiKjxbe)](https://web.archive.org/web/20201126090205/https://mappingmovement.newberry.org/item/carte-du-nord-des-e%CC%81tats-unis-et-du-grand-canal-de-communication-du-lac-erie%CC%81-a%CC%80-la-rivie%CC%80re-d "Plan and Profile, Erie Canal, 1820")

[View Full Metadata](/web/20201126090205/https://mappingmovement.newberry.org/item/carte-du-nord-des-e%CC%81tats-unis-et-du-grand-canal-de-communication-du-lac-erie%CC%81-a%CC%80-la-rivie%CC%80re-d)

[Plan and Profile, Erie Canal, 1820](/web/20201126090205/https://mappingmovement.newberry.org/item/plan-et-profil-du-canal-projette%CC%81-du-lac-erie%CC%81-la-riviere-hudson-dans-l%E2%80%99etat-de-neuwyork)

**Citation:**

Cordier, Joseph. Plan et profil du canal projetté du lac Erié a la riviere Hudson dans l’etat de Neuwyork in his Histoire de la navigation intérieure, et particuliérement de celle des États-Unis d’Amérique (Paris : Firmin Didot, 1820).

[![](https://web.archive.org/web/20201126090205im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_33.jpg?itok=4r67OQUs)](https://web.archive.org/web/20201126090205/https://mappingmovement.newberry.org/item/plan-et-profil-du-canal-projette%CC%81-du-lac-erie%CC%81-la-riviere-hudson-dans-l%E2%80%99etat-de-neuwyork "Plan and Profile, Erie Canal, 1820")

[View Full Metadata](/web/20201126090205/https://mappingmovement.newberry.org/item/plan-et-profil-du-canal-projette%CC%81-du-lac-erie%CC%81-la-riviere-hudson-dans-l%E2%80%99etat-de-neuwyork)
