mm20-0196-georgia-central-railroad-and-its-connections-1869.md
﻿---
title: Georgia Central Railroad and its Connections, 1869
parent: Lines that Fracture and Fade: Two Centuries of Travel through Georgia, 1775-1976
parentslug: lines-fracture-and-fade-two-centuries-travel-through-georgia-1775-1976
---

D. Appleton and Company was a New York book publisher that began issuing travel guides in 1857. Like most early railroad guides, Appleton’s books were simply lists of steamboat, stagecoach, and railroad timetables with no cartographic information at all. It was not until 1868 that any of these guides began including railroad system maps in their publications. This 1869 map is therefore an early example of the genre, printed in an age before railroad companies began issuing their own system maps as a way of advertising their lines.  
     The map, detached from its original publication, highlights the railroad systems of Georgia, but does not include travel schedules or time tables, presumably as that information could be found elsewhere in the original book. A simple map, it nonetheless represents how developed Georgia’s railroad systems had become by 1869, only four years removed from the destruction and havoc of the Civil War.  
 

Selection Gallery
-----------------

[Georgia Central Railroad and its Connections, 1869](/web/20210127222651/http://mappingmovement.newberry.org/item/georgia-central-railroad-and-its-connections)

**Citation:**

D. Appleton and Company, "Georgia Central Railroad, and Its Connections", detached from F.D. Lee and J.L. Agnew, _Historical Record of the City of Savannah_ (Savannah, Ga. : J.H. Estill, 1869), between p. 140-141. Map1F G3866.P3 1869 .A6

[![](https://web.archive.org/web/20210127222651im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_134.jpg?itok=QyjY2O4K)](https://web.archive.org/web/20210127222651/http://mappingmovement.newberry.org/item/georgia-central-railroad-and-its-connections "Georgia Central Railroad and its Connections, 1869")

[View Full Metadata](/web/20210127222651/http://mappingmovement.newberry.org/item/georgia-central-railroad-and-its-connections)
