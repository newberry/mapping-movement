mm18-0171-coast-pilot-chart-of-new-england-1732.md
﻿---
title: Coast Pilot Chart of New England, 1732
parent: Navigating the Coasts and Seas
parentslug: navigating-coasts-and-seas
---

Halley’s chart of the coast of New England was published in the 1732 edition of _The English Pilot_, which was begun by John Seller, Sr. (1630-1697) in 1675. Seller, as a bookseller and printer, atlas compiler, and Hydrographer to the King, was a giant in the establishment of printed aids to nautical navigation, beginning in 1669 with his treatise _Practical navigation: or, An introduction to that whole art. Containing I. Several definitions in geometry, astronomy, geography, and navigation. 2. A new and exact kalender, etc._ _The English Pilot_ is some form has been continuously in print to the present day.  
     In Halley’s chart, the continental and island shore lines are shown in an equirectangular projection called the plate carré, which has significant distortions, but for relatively small areas, such as offshore from New England, the distortions are manageable. Some areas of the ocean display soundings, numbers on the water referring to the water depth at that spot, derived from sounding the bottom with lead weights on ropes marked by increments of length. There is a compass rose in the approximate center of the ocean expanse, a device tracing back centuries, to assist in plotting ships’ courses relative to the 32-point “rose” or divisions, of the 360-degree circle. Shallow shoal areas, such as the Georges Bank east of Cape Cod, are marked and sounded, as these were extremely important fishing grounds, essential to commerce on both sides of the Atlantic. Boston, the most important harbor in eastern North America at the time, is shown in great detail in an inset map in the upper left corner. Notice as well what is not presented: Any indication of Native American lands or waters, nor any French land or sea claims.  
 

Selection Gallery
-----------------

[Coast Pilot Chart of New England, 1732](/web/20210119184412/http://mappingmovement.newberry.org/item/correct-map-coast-new-england-1731)

**Citation:**

Southack, Cyprian, "A Correct Map of the Coast of New England, 1731", in _The English Pilot : the Fourth Book : Describing the West-India Navigation, from Hudson’s-Bay to the River Amazones ..._ (London : Printed for Thomas Page and William Mount, 1732), plate 14. Baskes oversize G1106.P5 E5 1732

[![](https://web.archive.org/web/20210119184412im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_214.jpg?itok=kbOl0laT)](https://web.archive.org/web/20210119184412/http://mappingmovement.newberry.org/item/correct-map-coast-new-england-1731 "Coast Pilot Chart of New England, 1732")

[View Full Metadata](/web/20210119184412/http://mappingmovement.newberry.org/item/correct-map-coast-new-england-1731)
