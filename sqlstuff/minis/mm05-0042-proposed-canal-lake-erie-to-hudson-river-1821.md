mm05-0042-proposed-canal-lake-erie-to-hudson-river-1821.md
﻿---
title: Proposed Canal, Lake Erie to Hudson River, 1821
parent: State and Federal Mapping of Infrastructure and Movement
parentslug: state-and-federal-mapping-infrastructure-and-movement
---

In 1816 a petition proposing a canal from the Hudson River to Lake Erie was submitted to the New York legislature, complete with over 100,000 signatures from individuals around the state. DeWitt Clinton, the chief advocate of the canal, was derided for his outsized ambition; Thomas Jefferson himself called the project “a little short of madness.” But the proposal to build a canal extending 350 miles passed the legislature, and it opened eight years later. Indeed, when completed in 1825 the 363 mile long canal was far longer than any in Europe, and the economic results were astounding: The canal cut the cost of moving freight by over ninety percent and helped to establish New York City as the port serving a vast hinterland of the Middle West.  
     The canal route had been mapped as early as 1811, and proposals for a canal reach back further than that. But these were delayed somewhat by the War of 1812, which ironically also demonstrated the utility of an inland waterway to the western frontier. This 1821 map of the proposed canal is an updated version of the original published in 1817, both showing the proposed route and the elevation involved. The mapped route was based on information from surveys and maps made by engineers who detailed particular areas of the route and the obstacles it faced. While the idea for the canal was not new—US Secretary of the Treasury Albert Gallatin had proposed a route from the Hudson River to Lake Ontario and further in his 1807 report on canals and roads—it was immensely ambitious, as the map illustrates. In part, the challenge was one of elevation, for the land rises over 600 feet from the Hudson River to Lake Erie, which required over fifty locks to traverse. The map identified towns to be served by the canal, as well as the connecting rivers and roads that could both aid the canal but also present competing transportation.  
     The 1820s saw a frenzy not just of canal building, but of canal maps: On most subsequent maps of New York the canal appeared as an inset, a source of tremendous pride and profit for the state. The historian Carter Goodrich called the Erie Canal “the most decisive single event in the history of American transportation,” one which stimulated seventeen other canal surveys in the nation.  
 

Selection Gallery
-----------------

[Proposed Canal, Lake Erie to Hudson River, 1821](/web/20210128001057/https://mappingmovement.newberry.org/item/new-map-and-profile-proposed-canal-lake-erie-hudson-river-state-new-york)

**Citation:**

New York (State). Canal Commissioners, "A New Map and Profile of the Proposed Canal from Lake Erie to Hudson River in the State of New York", detached from New York Corresponding Association for the Promotion of Internal Improvements, _Public Documents, Relating to the New-York Canals, Which Are to Connect the Western and Northern Lakes, with the Atlantic Ocean_ (New York: William A. Mercein, 1821), opposite title page. Map8F G3802.E44 1821 .N4

[![](https://web.archive.org/web/20210128001057im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/MM05-0042-01.jpg?itok=myy8Fnfj)](https://web.archive.org/web/20210128001057/https://mappingmovement.newberry.org/item/new-map-and-profile-proposed-canal-lake-erie-hudson-river-state-new-york "Proposed Canal, Lake Erie to Hudson River, 1821")

[View Full Metadata](/web/20210128001057/https://mappingmovement.newberry.org/item/new-map-and-profile-proposed-canal-lake-erie-hudson-river-state-new-york)
