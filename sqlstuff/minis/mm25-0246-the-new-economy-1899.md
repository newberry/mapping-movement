mm25-0246-the-new-economy-1899.md
﻿---
title: The New Economy, 1899
parent: Mapping Communication
parentslug: mapping-communication
---

These maps of Ohio and Pennsylvania appeared in the 1899 edition of _Cram’s Standard American Railway System Atlas of the World_. The George F. Cram Company was one of the few firms to rival Rand McNally in the mapmaking industry of late-nineteenth century America. Both companies embraced new methods of mass production that made maps cheaper to produce, while the explosive growth of the railroads drove demand for their atlases and gazetteers. George Cram’s uncle had worked for Sidney Edwards Morse, brother of Samuel Morse and the inventor of wax engraving, a printing method that was faster and cheaper than traditional copperplate engraving. The Cram Company’s mass-produced atlases were less expensive than older atlases, even less expensive than Rand McNally’s works, and sold nationwide on trains and in stations, by mail order and direct subscription.  
     Cram’s name was apt. Wax-engraved maps could contain more detail than copperplate maps, and Cram’s atlases were typically crammed with tiny place names and other text. This map of Pennsylvania contains little information on topography or any natural features, but it shows 135 different railroads and more than a thousand towns and villages. These were reference works, not works of art, designed to meet the needs of bankers, brokers, and other businessmen. What these maps depict is the new economy of the late nineteenth century: A dense web of railroad lines and telegraph offices connecting virtually every town and hamlet in the East. (West of the Mississippi, the grid was not nearly so dense, although by 1899 this was changing.) The atlas also contained a detailed index of towns and cities, listing populations, the names and locations of banks, railroads and shipping companies, and post offices from which money orders could be sent or received.  
     Atlases like this one were, in the words of geographer John Rennie Short, “a depiction of the national economic space, a space that has been collapsed by the railroad system, the telegraph and the ability to make financial transactions” (Short 2001, 227). On the eve of the twentieth century, that economic space was indeed national, but not yet international. Despite its title, Cram’s “world” atlas focused primarily on American states and cities. In the 1890 edition of _Cram’s Unrivaled Atlas of the World_, 136 of 181 pages were devoted to the United States. This tension between local focus and international ambition was even more evident in the titles of state and regional atlases like _Cram’s Superior Reference Atlas of Michigan and the World_ (Schulten 2001, 29).  
 

Selection Gallery
-----------------

[The New Economy, 1899](/web/20210127231610/http://mappingmovement.newberry.org/item/pennsylvania)

**Citation:**

Cram, George Franklin, "Pennsylvania", in _Cram's Standard American Railway System Atlas of the World_ (New York ; Chicago : Geo. F. Cram, 1899), p. 68-69. Baskes oversize G1046.P3 C7 1899

[![](https://web.archive.org/web/20210127231610im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_254.jpg?itok=iCgztq_C)](https://web.archive.org/web/20210127231610/http://mappingmovement.newberry.org/item/pennsylvania "The New Economy, 1899")

[View Full Metadata](/web/20210127231610/http://mappingmovement.newberry.org/item/pennsylvania)

[The New Economy, 1899](/web/20210127231610/http://mappingmovement.newberry.org/item/ohio)

**Citation:**

Cram, George Franklin, "Ohio", in _Cram's Standard American Railway System Atlas of the World_ (New York ; Chicago : Geo. F. Cram, 1899), p. 126-127. Baskes oversize G1046.P3 C7 1899

[![](https://web.archive.org/web/20210127231610im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_230.jpg?itok=01MrBWYq)](https://web.archive.org/web/20210127231610/http://mappingmovement.newberry.org/item/ohio "The New Economy, 1899")

[View Full Metadata](/web/20210127231610/http://mappingmovement.newberry.org/item/ohio)
