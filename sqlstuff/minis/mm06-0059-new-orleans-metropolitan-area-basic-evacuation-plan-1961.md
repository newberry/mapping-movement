mm06-0059-new-orleans-metropolitan-area-basic-evacuation-plan-1961.md
﻿---
title: New Orleans Metropolitan Area Basic Evacuation Plan, 1961
parent: Planning Transportation
parentslug: planning-transportation
---

This map illustrates the enormous planning that went into civilian evacuation of urban areas in the event of nuclear attack at the height of the Cold War. Issued by the New Orleans Office of Civil Defense, the text makes little attempt underplay the danger. Readers are urged to “Know what you should do, know why you should do it, know when you should do it.” An outline of a nuclear mushroom cloud on the reverse side of the map offers power visual imagery, reminding residents of the need to study the map in advance.  
     Arrows on the map point to the arterial roadways deemed best suited for fast evacuation. At first blush, these arrows resemble the movements of a Second World War tank division through a European city more than a route for civilians traveling by bicycle, car, or on foot. In some cases, residents were given considerable discretion, perhaps with the hope they wouldn’t all try to use the same roads. Most residents were directed east or west, in part due to the limited capacity of the only two Mississippi River bridges—the same two crossings that exist today. Residents near Lake Pontchartrain were directed to the lightly used terminal of New Orleans’ Lakefront Airport, which was being retrofitted as a concrete-walled bomb shelter at about the same period this map was produced (Most commercial air traffic had been moved to a newer airport closer to the edge of town, today’s Louis Armstrong International).  
     The reverse side of the map offers many references to the hoped-for speed with which the exodus would take place. Readers are advised that “Motor transportation will be your salvation,” and to “Keep no less than ½ tank of gas at any time. If a vehicle breaks down, push it off the road and abandon it.”  
     Nor are motorists led to believe that a nuclear blast will affect only a narrowly defined area. “It must be realized that it is not enough just for you to get outside the twenty-mile zone. You must keep going so that the people behind you can get out also,” the narrative warns. Indeed, the Soviet Union had upgraded from atomic to much more powerful hydrogen bombs six years earlier, and was in the midst of dramatic improvements to its intercontinental missiles at the time. Some residents no doubt cringed upon reading that that they should take heed of the “Take Cover Signal,” described as being “Warbling or intermittent 3 to 5 minute blasts on streets.”  
     New Civil Defense maps were issued for the city approximately once a year to reflect enhancements to evacuation plans. In 1963, about two years after this map was issued, security rose to maximal levels during the Cuban Missile Crisis. New Orleans was 669 miles from Havana, putting it out of range of short-range missiles, but it was an important strategic target nonetheless. By the mid-1970s, however, such maps were rarely issued to the general public.   
     Forty-four years after this map was issued, Hurricane Katrina brought notoriety to the enormous logistical difficulties of quickly moving large numbers of people out of New Orleans. Present-day plans for evacuation during hurricane season take on almost the same urgency as those during the Cold War. Such weather-induced plans, however, allow for the city to be emptied over a time period extending over days rather than mere hours.  
 

Selection Gallery
-----------------

[New Orleans Metropolitan Area Evacuation Plan 1961](/web/20200927150339/http://mappingmovement.newberry.org/item/new-orleans-metropolitan-area-basic-evacuation-plan)

**Citation:**

New Orleans (La.). Civil Defense, "New Orleans Metropolitan Area Basic Evacuation Plan" (Chicago ; San Jose \[Calif.\] : H.M. Goushá Co., c1961). Map4F G4014.N5R6 1961 .N4

[![](https://web.archive.org/web/20200927150339im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_57.jpg?itok=_cSdID42)](https://web.archive.org/web/20200927150339/http://mappingmovement.newberry.org/item/new-orleans-metropolitan-area-basic-evacuation-plan "New Orleans Metropolitan Area Evacuation Plan 1961")

[View Full Metadata](/web/20200927150339/http://mappingmovement.newberry.org/item/new-orleans-metropolitan-area-basic-evacuation-plan)

[![](https://web.archive.org/web/20200927150339im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_54.jpg?itok=TH-NENeL)](https://web.archive.org/web/20200927150339/http://mappingmovement.newberry.org/item/new-orleans-metropolitan-area-basic-evacuation-plan "New Orleans Metropolitan Area Evacuation Plan 1961")

[View Full Metadata](/web/20200927150339/http://mappingmovement.newberry.org/item/new-orleans-metropolitan-area-basic-evacuation-plan)
