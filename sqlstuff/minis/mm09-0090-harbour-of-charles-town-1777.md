mm09-0090-harbour-of-charles-town-1777.md
﻿---
title: Harbour of Charles Town, 1777
parent: Moving Pictures: Maps and Imagination in Eighteenth-Century Anglo-America
parentslug: moving-pictures-maps-and-imagination-eighteenth-century-anglo-america
---

Selection Gallery
-----------------

[Harbour of Charles Town, 1777](/web/20201125171009/https://mappingmovement.newberry.org/item/harbour-charles-town-south-carolina)

**Citation:**

Des Barres, Joseph F. W., "The Harbour of Charles Town in South-Carolina", detached from _Atlantic Neptune_ (\[London\] : Joseph Des Barres, \[ca. 1774-ca. 1784\]), plate \[no. 162\]. Map4F 3320 no. 162

[![](https://web.archive.org/web/20201125171009im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_273.jpg?itok=ufAG5WWq)](https://web.archive.org/web/20201125171009/https://mappingmovement.newberry.org/item/harbour-charles-town-south-carolina "Harbour of Charles Town, 1777")

[View Full Metadata](/web/20201125171009/https://mappingmovement.newberry.org/item/harbour-charles-town-south-carolina)
