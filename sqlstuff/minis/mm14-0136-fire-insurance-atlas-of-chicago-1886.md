mm14-0136-fire-insurance-atlas-of-chicago-1886.md
﻿---
title: Fire Insurance Atlas of Chicago, 1886
parent: Mapping Migration and Settlement
parentslug: mapping-migration-and-settlement
---

Throughout the last half of the nineteenth century, German speaking immigrants were the largest group coming into the United States. In addition to their widespread distribution in rural areas throughout the Mid-Atlantic, Midwestern, and Upper Great Plains states, there were also large concentrations, often known as “Little Germanies,” in most Midwestern and Great Lakes cities. Buffalo, Cincinnati, Cleveland, Louisville, St. Louis, Milwaukee, and Chicago all had sizeable German populations (at least twenty percent or greater by 1860).  
     One example of their urban experience is Chicago, where they constituted the largest immigrant group during the last half of the nineteenth century, comprising one-third of the city’s population. Chicago, founded in 1833, is located on the eastern shore of Lake Michigan, where the city provided a port-of-entry for German immigrants travelling by the Great Lakes into Illinois, Wisconsin, and the Upper Midwest. German settlement began in the 1840s and 1850s, and increased significantly during the last half of the nineteenth century (Harzig).  
     Large-scale urban atlases, such as fire insurance and real estate atlases, are a good source for identifying and locating key settlement features associated with individual immigrant groups (Karrow and Grim 1990; Grim 1994, 80-84). Prepared to assist insurance underwriters or real estate agents in assessing the risk involved in insuring individual properties or determining the ownership and relative value of individual properties, these maps displayed cartographically a block-by block inventory of buildings in most major cities and towns in the United States during the last third of the nineteenth century and the first half of the twentieth century. The large-scale maps in these atlases show the footprint of residential, commercial, industrial and public buildings throughout the built-up portions of individual cities. The buildings are color coded to indicate their construction material: Wood (yellow), brick (pink), or stone (brown or blue). Both fire insurance and real estate atlases identify major public buildings, such as churches and schools, as well as industrial buildings. In addition, most real estate atlases indicate the owners’ names for residential properties.  
     In order to illustrate German settlement in Chicago during the late nineteenth century, four sheets from a five-volume real estate atlas of Chicago published in 1886 by Elisha Robinson are displayed here. Sheets 23, 24, 25, and 26 in volume 3 depict a one-half square mile area, consisting of fifty-five blocks on Chicago’s North Side, an area recognized as one of first German concentrations in the city. The sample area is bounded by Halstead Street on the west, Fullerton Street on the north, Sedgwick Street on the east, and North Street on the south. The map sheets provide substantial evidence of the German presence in this area. Of particular note are three churches and associated schools and social services (St. Michael’s German Roman Catholic Church, Convent, and School; German Evangelical Church and Uhlich Evangelical Lutheran Orphanage; and Second German Baptist Church and School) and two German owned breweries (Bartholomae and Leich Brewing Company and K.C. Schmidt Brewing Company). It was religious, educational, social, industrial, and commercial activities such as these that helped provide cohesive lifestyles for the residents of the Little Germanies that developed in the Midwestern cities during the last half of the nineteenth century.  
     Unfortunately, the Robinson atlas did not include names of property owners for individual lots, as many real estate atlases do. Thus, other sources, such as manuscript census schedules or city directories, should be consulted to provide a better sense of the size and density of the German population in this neighborhood. A quick survey of _Lakeside Annual Directory of the City of Chicago_ (1881) confirms the large number of German residents in this part of the city. For example, the block of Hurlbut Street (now Cleveland Street) between North Street and Eugenie Street, where St. Michael’s German Roman Catholic Church is located, reveals such family surnames as Mueller, Schaefer, Scharf, Schlem, Schlacht, Schmidt, Schmitz, Wenzel, Wertel, and Wermerskerchen. The variety of occupations listed for these individuals (furniture maker, carpenter, watchmaker, cigar maker, tailor, upholsterer, engraver, printer, machinist, sign painter, dressmaker, trunk maker, teamster, and laborer) suggests a working-class neighborhood. The parallel streets of Mohawk Street and Hudson Street, between North Street and Eugenie Street, as well as many surrounding blocks, display similar patterns confirming the high density of German residents in this sample area.  
 

Selection Gallery
-----------------

[Fire Insurance Atlas of Chicago, 1886](/web/20210119183200/http://mappingmovement.newberry.org/item/e-12-sw-14-section-334014-canal-trustees-sub-part-sec-334014)

**Citation:**

Robinson, Elisha. E., "E. 1/2 of S.W. 1/4, Section 33.40.14 : Canal Trustees Sub. of Part of Sec. 33.40.14", in _Robinson’s Atlas of the City of Chicago, Illinois_ (New York : E. Robinson, 1886), v. 3, plate 23. Baskes oversize G4104.C6 1886 .R6

[![](https://web.archive.org/web/20210119183200im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_179.jpg?itok=eqPLGTf4)](https://web.archive.org/web/20210119183200/http://mappingmovement.newberry.org/item/e-12-sw-14-section-334014-canal-trustees-sub-part-sec-334014 "Fire Insurance Atlas of Chicago, 1886")

[View Full Metadata](/web/20210119183200/http://mappingmovement.newberry.org/item/e-12-sw-14-section-334014-canal-trustees-sub-part-sec-334014)

[Fire Insurance Atlas of Chicago, 1886](/web/20210119183200/http://mappingmovement.newberry.org/item/w-12-sw-14-section-334014-part-sheffields-add)

**Citation:**

Robinson, Elisha. E., "W. 1/2 of S.W. 1/4, Section 33.40.14 : Part of Sheffields Add.", in _Robinson’s Atlas of the City of Chicago, Illinois_ (New York : E. Robinson, 1886), v. 3, plate 24. Baskes oversize G4104.C6 1886 .R6

[![](https://web.archive.org/web/20210119183200im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_180.jpg?itok=iJHRp9eI)](https://web.archive.org/web/20210119183200/http://mappingmovement.newberry.org/item/w-12-sw-14-section-334014-part-sheffields-add "Fire Insurance Atlas of Chicago, 1886")

[View Full Metadata](/web/20210119183200/http://mappingmovement.newberry.org/item/w-12-sw-14-section-334014-part-sheffields-add)

[Fire Insurance Atlas of Chicago, 1886](/web/20210119183200/http://mappingmovement.newberry.org/item/w-12-nw-14-section-334014-canal-trustees-sub-part-sec-334014)

**Citation:**

Robinson, Elisha. E., "W. 1/2 of N.W. 1/4, Section 33.40.14 : Canal Trustees Sub. of Part of Sec. 33.40.14", in _Robinson’s Atlas of the City of Chicago, Illinois_ (New York : E. Robinson, 1886), v. 3, plate 25. Baskes oversize G4104.C6 1886 .R6

[![](https://web.archive.org/web/20210119183200im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_226.jpg?itok=GKRPcyA8)](https://web.archive.org/web/20210119183200/http://mappingmovement.newberry.org/item/w-12-nw-14-section-334014-canal-trustees-sub-part-sec-334014 "Fire Insurance Atlas of Chicago, 1886")

[View Full Metadata](/web/20210119183200/http://mappingmovement.newberry.org/item/w-12-nw-14-section-334014-canal-trustees-sub-part-sec-334014)

[Fire Insurance Atlas of Chicago, 1886](/web/20210119183200/http://mappingmovement.newberry.org/item/e-12-nw-14-section-334014-canal-trustees-sub-part-sec-334014)

**Citation:**

Robinson, Elisha. E., "E. 1/2 of N.W. 1/4, section 33.40.14 : Canal Trustees sub. of part of sec. 33.40.14", in _Robinson’s Atlas of the City of Chicago, Illinois_ (New York : E. Robinson, 1886), v. 3, plate 26. Baskes oversize G4104.C6 1886 .R6

[![](https://web.archive.org/web/20210119183200im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_239.jpg?itok=OwqpVNwD)](https://web.archive.org/web/20210119183200/http://mappingmovement.newberry.org/item/e-12-nw-14-section-334014-canal-trustees-sub-part-sec-334014 "Fire Insurance Atlas of Chicago, 1886")

[View Full Metadata](/web/20210119183200/http://mappingmovement.newberry.org/item/e-12-nw-14-section-334014-canal-trustees-sub-part-sec-334014)
