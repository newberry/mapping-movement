mm09-0088-south-carolina-and-part-of-georgia-1780.md
﻿---
title: South Carolina and Part of Georgia, 1780
parent: Moving Pictures: Maps and Imagination in Eighteenth-Century Anglo-America
parentslug: moving-pictures-maps-and-imagination-eighteenth-century-anglo-america
---

Even though De Brahm’s name remained on the map’s title, he likely had nothing to do with this updated version. De Brahm had spent the years 1765-1780 either in Florida conducting surveys of the peninsula’s eastern coast or in London answering charges of misconduct during his Florida tenure. The title names John Stuart, Superintendent for Indian Affairs, who was a likelier source for the updated information. As Superintendent, Stuart had a keen interest in surveys of the southern colonies, as he was charged with negotiating and surveying the 1763 Proclamation line between the numerous Indians of the Southeast and the southern colonies. Stuart thus worked closely with the surveyors of South Carolina and Georgia and likely used their records to update De Brahm’s earlier map.

Selection Gallery
-----------------

[South Carolina and Part of Georgia, 1780](/web/20210127202107/https://mappingmovement.newberry.org/item/map-south-carolina-and-part-georgia-1780)

**Citation:**

Faden, William, "A Map of South Carolina and a Part of Georgia" (London : William Faden, 1780). Oversize Ayer 133 .D28 1780

[![](https://web.archive.org/web/20210127202107im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_202.jpg?itok=k0Lr6_vs)](https://web.archive.org/web/20210127202107/https://mappingmovement.newberry.org/item/map-south-carolina-and-part-georgia-1780 "South Carolina and Part of Georgia, 1780")

[View Full Metadata](/web/20210127202107/https://mappingmovement.newberry.org/item/map-south-carolina-and-part-georgia-1780)
