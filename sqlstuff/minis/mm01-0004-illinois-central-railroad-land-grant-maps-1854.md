mm01-0004-illinois-central-railroad-land-grant-maps-1854.md
﻿---
title: Illinois Central Railroad, Land Grant Maps, 1854
parent: American Railroad Maps, 1828-1876
parentslug: american-railroad-maps-1828-1876
---

In 1850 the Illinois Central Railroad (formally chartered in 1851) became the first of many railroad companies to receive a grant of federal lands for the company to sell to finance its construction. Through these grants railroads engaged in land development, real estate sales, and the recruitment of settlers, both from the United States and abroad. From the beginning maps, such as these from a small atlas published by the Illinois Central in 1854, were part of this process.  
     The original Illinois Central grant constituted of alternate blocks (the even numbered “sections”) of land in a six-mile corridor, shown as a darker band of grey on the general state map on either side of the railroad’s right-of-way. The other lands in this zone remained in government hands in the hope that their value would at least double as the railroad was built, thus recovering the government’s loss of income from grant lands. Since some land, however, had already been sold before the enactment of the grant, the railroad was allowed to select “indemnity lands” from a band fifteen miles beyond the original grant zone, indicated by the lighter grey line on the general map.  
     The prominent grid on the index maps outlines the township and range system of land division, established in 1785 to facilitate settlement of the lands of the old Northwest Territory. The system called for the survey and division of most land west of the Appalachians into townships, usually consisting of thirty square-mile sections. The main north-south axis of the survey in Illinois, the Third Principal Meridian, roughly bisects the state parallel to the Illinois Central main line.  
     The grid allowed prospective buyers to locate parcels of interest on the atlas’s local maps, four of which are illustrated. Three of these maps show lands near the termini of the Y-shaped railroad. Galena, in the far northwestern corner of the state and the center of an important lead-mining region, and Cairo, over 300 miles to the south at the confluence of the Ohio and Mississippi rivers, were envisioned as the railroad’s termini from the start. Relatively little land remained available (the grey squares on each map) in these areas for the railroad to sell in 1854. No land at all remained for sale along the railroad’s approach to its third terminus, the budding metropolis of Chicago. In 1854 the city’s most important route was the Illinois and Michigan Canal, shown as a bold black line on the sectional map. This waterway, like the railroad, linked Lake Michigan to the Mississippi river system. Plate 29, south of Urbana, with its distinct checkerboard pattern, is more typical of the maps in the atlas. It pictures an area that was still sparsely settled. The brief text on the map observes that Urbana is a “flourishing town” and that the crossing Great Western Rail Road, Later the Wabash Railroad, will someday bring coal from Danville, Illinois, to the east, for the use of settlers and industries along the line of the Illinois Central, and, finally, that the whole of the surrounding country “is in the highest degree fertile and well wooded.”  

Selection Gallery
-----------------

[Railroad Land Grants, Illinois, 1854](/web/20210119190349/http://mappingmovement.newberry.org/item/outline-map-illinois)

**Citation:**

Illinois Central Railroad Company, "Outline Map of Illinois", in _Sectional Maps, Showing 2,500,000 Acres Farm and Wood Lands of the Illinois Central Rail Road Company in All Parts of the State of Illinois_ (Chicago : Illinois Central Rail Road Company, 1854?), map 1. Graff 2081

[![](https://web.archive.org/web/20210119190349im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/MM01-0004-01.jpg?itok=kOLcT-3Y)](https://web.archive.org/web/20210119190349/http://mappingmovement.newberry.org/item/outline-map-illinois "Railroad Land Grants, Illinois, 1854")

[View Full Metadata](/web/20210119190349/http://mappingmovement.newberry.org/item/outline-map-illinois)

[Land Parcels, Cairo Area, 1854](/web/20210119190349/http://mappingmovement.newberry.org/item/cairo-city)

**Citation:**

Illinois Central Railroad Company, "Cairo City", in _Sectional Maps, Showing 2,500,000 Acres Farm and Wood Lands of the Illinois Central Rail Road Company in All Parts of the State of Illinois_ (Chicago : Illinois Central Rail Road Company, 1854?), map 2. Graff 2081

[![](https://web.archive.org/web/20210119190349im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/MM01-0004-02.jpg?itok=eBh2Rn_-)](https://web.archive.org/web/20210119190349/http://mappingmovement.newberry.org/item/cairo-city "Land Parcels, Cairo Area, 1854 ")

[View Full Metadata](/web/20210119190349/http://mappingmovement.newberry.org/item/cairo-city)

[Land Parcels, Dunlieth Area, 1854](/web/20210119190349/http://mappingmovement.newberry.org/item/dunleith)

**Citation:**

Illinois Central Railroad Company, "Dunlieth", in _Sectional Maps, Showing 2,500,000 Acres Farm and Wood Lands of the Illinois Central Rail Road Company in All Parts of the State of Illinois_ (Chicago : Illinois Central Rail Road Company, 1854?), map 23. Graff 2081

[![](https://web.archive.org/web/20210119190349im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/MM01-0004-03.jpg?itok=kxuuuVmz)](https://web.archive.org/web/20210119190349/http://mappingmovement.newberry.org/item/dunleith "Land Parcels, Dunlieth Area, 1854")

[View Full Metadata](/web/20210119190349/http://mappingmovement.newberry.org/item/dunleith)

[Railroads and Townships, Chicago Area, 1854](/web/20210119190349/http://mappingmovement.newberry.org/item/railroad-and-township-map-chicago-region)

**Citation:**

Illinois Central Railroad Company, "\[Railroad and Township Map of the Chicago Region\]", in _Sectional Maps, Showing 2,500,000 Acres Farm and Wood Lands of the Illinois Central Rail Road Company in All Parts of the State of Illinois_ (Chicago : Illinois Central Rail Road Company, 1854?), map 24. Graff 2081

[![](https://web.archive.org/web/20210119190349im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/MM01-0004-04.jpg?itok=nNC0ESun)](https://web.archive.org/web/20210119190349/http://mappingmovement.newberry.org/item/railroad-and-township-map-chicago-region "Railroads and Townships, Chicago Area, 1854")

[View Full Metadata](/web/20210119190349/http://mappingmovement.newberry.org/item/railroad-and-township-map-chicago-region)

[Land Parcels, Urbana Area, 1854](/web/20210119190349/http://mappingmovement.newberry.org/item/plate-29-urbana)

**Citation:**

Illinois Central Railroad Company, "Urbana", in _Sectional Maps, Showing 2,500,000 Acres Farm and Wood Lands of the Illinois Central Rail Road Company in All Parts of the State of Illinois_ (Chicago : Illinois Central Rail Road Company, 1854?), map 31. Graff 2081

[![](https://web.archive.org/web/20210119190349im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/MM01-0004-05.jpg?itok=ZQQ0tlpu)](https://web.archive.org/web/20210119190349/http://mappingmovement.newberry.org/item/plate-29-urbana "Land Parcels, Urbana Area, 1854")

[View Full Metadata](/web/20210119190349/http://mappingmovement.newberry.org/item/plate-29-urbana)
