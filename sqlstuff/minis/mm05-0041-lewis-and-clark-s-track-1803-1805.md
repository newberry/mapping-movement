mm05-0041-lewis-and-clark-s-track-1803-1805.md
﻿---
title: Lewis and Clark’s Track, 1803-1805
parent: State and Federal Mapping of Infrastructure and Movement
parentslug: state-and-federal-mapping-infrastructure-and-movement
---

When the United States acquired the Louisiana Territory in 1803, knowledge about the Far West remained limited. Americans knew little—if anything—about the land they had just purchased from France. Amidst such geographical ignorance, wishful thinking thrived, such as Thomas Jefferson’s hope for river portage that would link the Pacific with the drainage of the Mississippi River. As John Allen has written, it was commonly assumed at the time that rivers remained navigable all the way to their sources, which raised the possibility that the headwaters of the Missouri might offer a passage to the Columbia, and hence the Pacific Ocean.  
     The expedition of Meriwether Lewis and William Clark ended such dreams and replaced them with a spectacularly complex picture of the American Northwest. The expedition yielded tremendous detail of the course and geography of the Missouri River drainage basin, linking it with the Mississippi River. Equally valuable was the voluminous information brought back about the tribes, flora, and fauna of the Northwest.  
     William Clark’s map exemplifies this new, more detailed picture of the west that emerged after the expedition, though its provenance is unclear. It was copied from the larger hand-drawn map of William Clark, completed in 1810, and offered the most comprehensive picture of this territory at the time. Clark’s map revealed more geographical distance and topographic complexity in the Rocky Mountains, showing a detailed system of rivers that included the Missouri, the Clearwater, the Columbia, and the Snake. The entire Rocky Mountain system in the north is no less detailed, showing the intermountain region as well as hydrographic drainage.  
     Yet for all the advances in geographical knowledge yielded by the expedition, some important myths persisted about the possibilities for movement within the West. One of these was that the headwaters of the great western rivers were close to one another in the central Rocky Mountains, a myth that would not be definitively disproven until the 1840s.  
 

Selection Gallery
-----------------

[Lewis and Clark's Track, 1803-1805](/web/20210119192747/http://mappingmovement.newberry.org/item/map-lewis-and-clark%E2%80%99s-track-across-western-portion-north-america-misissippi-sic-pacific-ocean)

**Citation:**

Clark, William, "A Map of Lewis and Clark’s Track Across the Western Portion of North America from the Misissippi \[sic\] to the Pacific Ocean" (manuscript, circa 1811). VAULT map9C G4050 1811 .C5

[![](https://web.archive.org/web/20210119192747im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_232.jpg?itok=S6npBNbD)](https://web.archive.org/web/20210119192747/http://mappingmovement.newberry.org/item/map-lewis-and-clark%E2%80%99s-track-across-western-portion-north-america-misissippi-sic-pacific-ocean "Lewis and Clark's Track, 1803-1805")

[View Full Metadata](/web/20210119192747/http://mappingmovement.newberry.org/item/map-lewis-and-clark%E2%80%99s-track-across-western-portion-north-america-misissippi-sic-pacific-ocean)

[![](https://web.archive.org/web/20210119192747im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_65.jpg?itok=t2beELRF)](https://web.archive.org/web/20210119192747/http://mappingmovement.newberry.org/item/map-lewis-and-clark%E2%80%99s-track-across-western-portion-north-america-misissippi-sic-pacific-ocean "Lewis and Clark's Track, 1803-1805")

[View Full Metadata](/web/20210119192747/http://mappingmovement.newberry.org/item/map-lewis-and-clark%E2%80%99s-track-across-western-portion-north-america-misissippi-sic-pacific-ocean)
