mm15-0149-the-hollow-earth-1906.md
﻿---
title: The Hollow Earth, 1906
parent: Maps, Movement, and American Literature
parentslug: maps-movement-and-american-literature
---

Maps navigating fictional worlds, while rare compared to maps appending travel literature, have been a staple of the American reading experience since the eighteenth century. Since the arrival of copies containing Daniel Defoe’s _Robinson Crusoe_ (1719-20), American readers encountered fold-out and insert maps illustrating imaginary journeys and cartographic views of fictional islands and continents, counties and even whole worlds. Early fictional maps were available for classics such as John Bunyan’s _Pilgrim’s Progress_ (c. 1678; 1778) and Jonathan Swift’s _Gulliver’s Travels_ (1726). More familiar today are the maps illustrating Frank Baum’s _The Wizard of Oz_ (1900), William Faulkner’s _Absalom, Absalom!_ (1936), J. R. R. Tolkien’s _The Hobbit_ (1937) and _The_ _Lord of the Rings_ trilogy (1954-55), not to mention book-related maps such as the those prefacing C. S. Lewis’s “Narnia” series and J. K. Rowling’s “Marauder’s Map” made famous in _Harry Potter and the Prisoner of Azkaban_ (1999).  
    William Reed’s volume, _The Phantom of the Poles_ (1906), a cross between pseudoscience and science fiction, seeks to explain geological and atmospheric phenomena observed by polar explorers by arguing that the Earth is hollow and that its insides are accessible through gigantic holes at either pole. The much-publicized expeditions by Admiral Robert Peary (1909) and Roald Amundson (1911) soon debunked the myth of the holes, and along with it the hollow-earth theory. However, Reed’s imaginary map of the hollow earth continues to inform modern science fiction in which book illustrators, through the use of a few cartographic symbols, easily invoke both the authority of the map as a tool of scientific representation and subsequently the authenticity of an imaginary underground geography, here consisting of oceans, continents, and even the opportunity of travel (see the sailing vessel at the map’s upper center).

Selection Gallery
-----------------

[The Hollow Earth, 1906](/web/20210303215148/http://mappingmovement.newberry.org/item/globe-showing-section-earth%E2%80%99s-interior)

**Citation:**

Reed, William, "Globe Showing Section of the Earth’s Interior", in _Phantom of the Poles_ (New York : Walter S. Rockey Co., 1906), page \[27\]. Fitzgerald QE509 .R44 1906

[![](https://web.archive.org/web/20210303215148im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_46.jpg?itok=a2ZBHVGD)](https://web.archive.org/web/20210303215148/http://mappingmovement.newberry.org/item/globe-showing-section-earth%E2%80%99s-interior "The Hollow Earth, 1906")

[View Full Metadata](/web/20210303215148/http://mappingmovement.newberry.org/item/globe-showing-section-earth%E2%80%99s-interior)
