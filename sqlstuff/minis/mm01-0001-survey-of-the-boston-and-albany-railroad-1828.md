mm01-0001-survey-of-the-boston-and-albany-railroad-1828.md
﻿---
title: Survey of the Boston and Albany Railroad, 1828
parent: American Railroad Maps, 1828-1876
parentslug: american-railroad-maps-1828-1876
---

“The most important question,” the railroad engineer William H. Searles concluded in 1885, “is whether or not to build the line at all.” Normally this began with an investigation of the economic, political, and geographical conditions that might support construction of a railroad. If a railroad promoter got beyond this stage, he would usually assemble a group of like-minded investors, seek a state charter of incorporation, and then start to raise funds for an engineering survey. This fieldwork would then be summarized in a map and a profile (a cross-section showing the changing elevation along the route), like these showing the results of the 1827-28 survey of the future Boston and Albany, one of the earliest American railroads. The Massachusetts Board of Internal Improvements, which sponsored the survey, was anxious to keep pace with New York’s Erie Canal, constructed 1817-25. Surveyor James Baldwin’s map was used in a variety of ways to drum up public support as well as financing, the first of many railroad maps rooted in an engineering tradition pressed into service as a promotional tool.  
Baldwin’s survey required two maps, one from Boston to Springfield on the Connecticut River, and one from the Springfield to Albany, totaling sixty-three inches in length. Alternative routes were explored, but Baldwin did not complete his survey of the northern route beyond the Connecticut River because the route’s summit west of the river would be about 500 feet higher than the southern route through Springfield. The report included detailed descriptions of the routes on the map as well as summary considerations of other options investigated. At this date, the Board of Internal Improvements still planned to use horses to pull cars on the rails projected on this map. Indeed, one reason for constructing a profile was to determine whether one or two horses would be needed to pull a car on a particular segment. Furthermore, since the early steam ocomotives lacked the hosepower to ascent steep grades, incline planes might be prescribed, such as those used in the Allegheny mountains of Pennsylvania.  
The completed line departed in some details from the original surveyed course, and was not finished until 1843. A bridge comleting the route across the Hudson River to Albany was not completed until 1866. By then railroads had transformed the New England landscape. Water-powered factories such as the Boston Manufacturing Company textile mill at Waltham were able to expand operations using steam engines powered by coal supplied by the railroad. The daily supply of milk brought in by the railroads encouraged Boston to enact the nation’s first fresh milk ordinance in 1856. Dairy farmers made daily trips into towns along the railroad where the milk trains shuttled their cans back and forth to the city, on roads improved to facilitate the trip. The line envisioned in 1828 was nevertheless a technology in transition from older ones. In many instances early railroads were conceived as links between communities and navigable rivers or canals, rather to supplant them. This very route, for example, was shown on William Norris’s _Maps of the Railroads and Canals_ (1834) as an extension of the Erie Canal.   

Selection Gallery
-----------------

[Plan of a survey for a rail road](/web/20210119185311/http://mappingmovement.newberry.org/item/plan-survey-rail-road-made-under-direction-board-directors-internal-improvements)

**Citation:**

Baldwin, James Fowle, "Plan of a Survey for a Rail Road : Made Under the Direction of the Board of Directors of Internal Improvements", in Massachusetts. Board of Internal Improvements, _Report of the Board of Directors of Internal Improvements of the State of Massachusetts : on the Practicability and Expediency of a Rail-Road from Boston to the Hudson River, and from Boston to Providence_ (Boston : Press of the Boston Daily Advertiser, 1829), maps 1-2. Case H 66844 .396

[![](https://web.archive.org/web/20210119185311im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/MM01-0001-01.jpg?itok=0nlMTAmI)](https://web.archive.org/web/20210119185311/http://mappingmovement.newberry.org/item/plan-survey-rail-road-made-under-direction-board-directors-internal-improvements "Plan of a survey for a rail road")

[View Full Metadata](/web/20210119185311/http://mappingmovement.newberry.org/item/plan-survey-rail-road-made-under-direction-board-directors-internal-improvements)

[![](https://web.archive.org/web/20210119185311im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/MM01-0001-02.jpg?itok=OyZq0rSK)](https://web.archive.org/web/20210119185311/http://mappingmovement.newberry.org/item/plan-survey-rail-road-made-under-direction-board-directors-internal-improvements "Plan of a survey for a rail road")

[View Full Metadata](/web/20210119185311/http://mappingmovement.newberry.org/item/plan-survey-rail-road-made-under-direction-board-directors-internal-improvements)

[Boston and Albany Railroad profiles from Boston to New York State line](/web/20210119185311/http://mappingmovement.newberry.org/item/boston-and-albany-railroad-survey-profiles-boston-new-york-state-line-canaan)

**Citation:**

Baldwin, James Fowle, "\[Boston and Albany Railroad Survey Profiles from Boston to New York State Line at Canaan\]", in Massachusetts. Board of Internal Improvements, _Report of the Board of Directors of Internal Improvements of the State of Massachusetts : on the Practicability and Expediency of a Rail-Road from Boston to the Hudson River, and from Boston to Providence_ (Boston : Press of the Boston Daily Advertiser, 1829), maps 3-5. Case H 66844 .396

[![](https://web.archive.org/web/20210119185311im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/MM01-0001-03.jpg?itok=1SWsu32Z)](https://web.archive.org/web/20210119185311/http://mappingmovement.newberry.org/item/boston-and-albany-railroad-survey-profiles-boston-new-york-state-line-canaan "Boston and Albany Railroad profiles from Boston to New York State line")

[View Full Metadata](/web/20210119185311/http://mappingmovement.newberry.org/item/boston-and-albany-railroad-survey-profiles-boston-new-york-state-line-canaan)

[![](https://web.archive.org/web/20210119185311im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/MM01-0001-04.jpg?itok=PJUbbQUc)](https://web.archive.org/web/20210119185311/http://mappingmovement.newberry.org/item/boston-and-albany-railroad-survey-profiles-boston-new-york-state-line-canaan "Boston and Albany Railroad profiles from Boston to New York State line")

[View Full Metadata](/web/20210119185311/http://mappingmovement.newberry.org/item/boston-and-albany-railroad-survey-profiles-boston-new-york-state-line-canaan)

[![](https://web.archive.org/web/20210119185311im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/MM01-0001-05.jpg?itok=Jh7DgnAM)](https://web.archive.org/web/20210119185311/http://mappingmovement.newberry.org/item/boston-and-albany-railroad-survey-profiles-boston-new-york-state-line-canaan "Boston and Albany Railroad profiles from Boston to New York State line")

[View Full Metadata](/web/20210119185311/http://mappingmovement.newberry.org/item/boston-and-albany-railroad-survey-profiles-boston-new-york-state-line-canaan)
