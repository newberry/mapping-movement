mm09-0087-south-carolina-and-part-of-georgia-1757.md
﻿---
title: South Carolina and Part of Georgia, 1757
parent: Moving Pictures: Maps and Imagination in Eighteenth-Century Anglo-America
parentslug: moving-pictures-maps-and-imagination-eighteenth-century-anglo-america
---

De Brahm had spent his early career as a military engineer in the service of Holy Roman Emperor Charles VII. A conversion to Protestantism forced him out of that role and onto a boat bound for Georgia. The governors of Georgia and South Carolina quickly realized the advantage of having a professionally trained engineer and surveyor, each appointing De Brahm as Surveyor General for their respective colonies. In this position, De Brahm had access to each colony’s survey records and used them, along with his own surveys and observations, to compile this map, published in London in 1757. It remained the standard coastal chart of South Carolina and Georgia for decades.

Selection Gallery
-----------------

[South Carolina and Part of Georgia, 1757](/web/20210127202507/https://mappingmovement.newberry.org/item/map-south-carolina-and-part-georgia-1757)

**Citation:**

De Brahm, John Gerar William, "A Map of South Carolina and a Part of Georgia" (London : Thomas Jefferys, 1757). Oversize Ayer 133 .D28 1757

[![](https://web.archive.org/web/20210127202507im_/https://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_148.jpg?itok=IZGEnwma)](https://web.archive.org/web/20210127202507/https://mappingmovement.newberry.org/item/map-south-carolina-and-part-georgia-1757 "South Carolina and Part of Georgia, 1757")

[View Full Metadata](/web/20210127202507/https://mappingmovement.newberry.org/item/map-south-carolina-and-part-georgia-1757)
