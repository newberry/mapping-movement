mm13-0126-buffalo-and-the-great-lakes-1931.md
﻿---
title: Buffalo and the Great Lakes, 1931
parent: Waterways Cartography, Part II: Landmarks and Exemplars in North America
parentslug: waterways-cartography-part-ii-landmarks-and-exemplars-north-america
---

The American experience in the First World War indicated that modern port facilities were essential to sending an American Expeditionary Force overseas and keeping it and its allies well supplied. The bottlenecks that hampered logistics during the Great War led Congress to include in the Merchant Marine Act of 1920 a provision for a series of studies reporting on the current situation of US ports and advising on ways to improve their operations. Two agencies, the Board of Engineers for Rivers and Harbors in the War Department and the United States Shipping Board, cooperated in producing, by 1930, a series of twenty-two volumes covering seventy ocean ports. The work soon extended to inland transportation on the Great Lakes (1926) and in the Mississippi Valley (1929).  
     The first volume of the Lake Series, _The Port of Buffalo, New York,_ published in 1931, put the issue at hand into perspective: “The Great Lakes and their connecting channels form a natural transportation highway, which in point of volume and importance of traffic has no equal as an inland route for waterborne commerce.” Moreover, as the western terminus of the New York State Barge Canal, the city of Buffalo was “the principal transshipment port in connection with rail and lake traffic to and from the West.” (151) Most of this traffic was in bulk commodities, the port receiving grain, iron ore, and limestone and sending out coal in return. In 1930 Buffalo reigned as “the greatest grain distributing port as well as the largest milling city of the Western Hemisphere.” (152)  
     Our focus map illuminates the reach and extent of Buffalo’s role in shipping on the Great Lakes in 1929. The map is also a cartogram because the width of the lines indicated the relative volume, in short tons, of the freight received at the port. Each harbor sending freight to Buffalo sent out a line to join the aggregated total which was drawn proportional to the tonnage involved. The actual tonnage for each port is also given by a number next to its name. Note that small quantities of goods also arrived from Montreal on the St. Lawrence River and the New York State Barge Canal.  
     The general impression created by the map is that shipments of iron ore from Duluth-Superior at the western tip of Lake Superior, joined by 2.7 million tons of wheat from Port Arthur and Fort William in Canada, formed the major spine of this lake traffic. About two dozen other ports added lesser volumes to the flowage, but these shipments often carried much higher values because they included manufactured goods as well as bulk shipments of commodities.  
     The striking flow chart, folded out opposite page 150 of the report of the United States Shipping Board on _The Port of Buffalo_, should be read with its twin, which immediately follows, showing the “Destination of Lake Shipments from Buffalo” during the same year. (Focus Map 6-2) In this case the traffic sent on the New York State Barge Canal and the Hudson River, as well as that to Canadian ports, was much more significant. The bulk of this outbound tonnage was probably coal from the Pennsylvania Coal Fields, especially the broad line returning shipments to Duluth-Superior. Because this pair of maps used the same base map, the inflow and outflow of freight to Buffalo is easily compared. Note the contrast in traffic volumes on the two maps utilizing the Welland Canal between Lakes Erie and Ontario and the St. Mary’s Ship Canal at Sault Ste. Marie. A large-scale map of the “Port and Facilities at Buffalo, N.Y.” follows page 155 in the report, emphasizing the railroad connections available.  
     An “Airplane view of the Port of Buffalo, N.Y.” serves as the frontispiece for the document, folding out to provide a companion piece for the harbor map. (Focus Map 6-6) The report ends on a favorable note: “Considering the large number of railroads entering the port, the numerous waterways dividing it, and the widely separated terminals, the port is well coordinated and equipped to handle…a much greater tonnage without congestion or delay.” (153) However, it would be advisable, even necessary, to institute a port commission with the authority to oversee these further developments.  

Selection Gallery
-----------------

[Buffalo and the Great Lakes, 1931](/web/20210119192807/http://mappingmovement.newberry.org/item/origin-lake-receipts-buffalo-ny-during-1929)

**Citation:**

United States. Board of Engineers for Rivers and Harbors, "Origin of Lake Receipts at Buffalo, N.Y., During 1929", in _Port of Buffalo, N.Y._ (Washington : Government Printing Office, 1931), following p. 150. Map4C G3804.B9P55 1931 .U5

[![](https://web.archive.org/web/20210119192807im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_168.jpg?itok=3Wia8s-4)](https://web.archive.org/web/20210119192807/http://mappingmovement.newberry.org/item/origin-lake-receipts-buffalo-ny-during-1929 "Buffalo and the Great Lakes, 1931")

[View Full Metadata](/web/20210119192807/http://mappingmovement.newberry.org/item/origin-lake-receipts-buffalo-ny-during-1929)

[Buffalo and the Great Lakes, 1931](/web/20210119192807/http://mappingmovement.newberry.org/item/destination-lake-shipments-buffalo-ny-during-1929)

**Citation:**

United States. Board of Engineers for Rivers and Harbors, "Destination of Lake Shipments from Buffalo, N.Y., During 1929", in _Port of Buffalo, N.Y._ (Washington : Government Printing Office, 1931), opposite p. 151. Map4C G3804.B9P55 1931 .U5

[![](https://web.archive.org/web/20210119192807im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_169.jpg?itok=xtF4rE30)](https://web.archive.org/web/20210119192807/http://mappingmovement.newberry.org/item/destination-lake-shipments-buffalo-ny-during-1929 "Buffalo and the Great Lakes, 1931")

[View Full Metadata](/web/20210119192807/http://mappingmovement.newberry.org/item/destination-lake-shipments-buffalo-ny-during-1929)

[Buffalo and the Great Lakes, 1931](/web/20210119192807/http://mappingmovement.newberry.org/item/airplane-view-port-buffalo-ny)

**Citation:**

United States. Board of Engineers for Rivers and Harbors, "Airplane View of the Port of Buffalo, N.Y.", in _Port of Buffalo, N.Y._ (Washington : Government Printing Office, 1931), frontispiece. Map4C G3804.B9P55 1931 .U5

[![](https://web.archive.org/web/20210119192807im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_176.jpg?itok=EVmU-JAa)](https://web.archive.org/web/20210119192807/http://mappingmovement.newberry.org/item/airplane-view-port-buffalo-ny "Buffalo and the Great Lakes, 1931")

[View Full Metadata](/web/20210119192807/http://mappingmovement.newberry.org/item/airplane-view-port-buffalo-ny)

[Buffalo and the Great Lakes, 1931](/web/20210119192807/http://mappingmovement.newberry.org/item/port-facilities-buffalo-ny)

**Citation:**

United States. Board of Engineers for Rivers and Harbors, "Port Facilities at Buffalo, N.Y.", in _Port of Buffalo, N.Y._ (Washington : Government Printing Office, 1931), opposite p. 155. Map4C G3804.B9P55 1931 .U5

[![](https://web.archive.org/web/20210119192807im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_170.jpg?itok=xmOCaXMS)](https://web.archive.org/web/20210119192807/http://mappingmovement.newberry.org/item/port-facilities-buffalo-ny "Buffalo and the Great Lakes, 1931")

[View Full Metadata](/web/20210119192807/http://mappingmovement.newberry.org/item/port-facilities-buffalo-ny)
