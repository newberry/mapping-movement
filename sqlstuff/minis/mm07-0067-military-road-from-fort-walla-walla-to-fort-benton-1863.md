mm07-0067-military-road-from-fort-walla-walla-to-fort-benton-1863.md
﻿---
title: Military Road from Fort Walla Walla to Fort Benton, 1863
parent: Maps of Trails and Roads of the Great West
parentslug: maps-trails-and-roads-great-west
---

This map was part of a report by John Mullan on the construction of the road portrayed published by the government also in 1863. He was an accomplished military engineer who along with Edward Freyhold also produced a major map of the Pacific Northwest in 1865. Freyhold was a prominent topographer and draughtsman who worked on numerous maps of the West for the government and the railroads in addition to those he did with Mullan. This road map also drew information from the notes, maps, and reports of others, including Theodore Kolecki, P.M. Engel, Gustavus Sohon, Captain Leo White, C.R. Howard, and Walter De Lacy.  
     The publisher of this map, Julius Bien & Co. of New York, was known for its fine lithography. Its founder Julius Bien (1826-1909) was born in Germany and trained there as an engraver and lithographer and came to the United States in 1849. He worked extensively for the government and the railroads, and his company eventually employed over two hundred people. Much of the Company’s extensive quality output consisted of maps and atlases of the West.  

Selection Gallery
-----------------

[Military Road from Fort Walla Walla to Fort Benton, 1863](/web/20210127233239/http://mappingmovement.newberry.org/item/map-military-road-fort-walla-walla-columbia-fort-benton-missouri)

**Citation:**

Mullan, John, "Map of Military Road from Fort Walla Walla on the Columbia to Fort Benton on the Missouri ... 1858-1863", detached from United States. Army. Corps of Engineers, _Report on the Construction of a Military Road from Fort Walla-Walla to Fort Benton_, Serial Set 1149, 37th Congress, 3rd session, Senate executive document 43 (Washington: Government Printing Office, 1863), at end. Map8F Graff 2932 map no. 1

[![](https://web.archive.org/web/20210127233239im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_166.jpg?itok=mfI2MnnJ)](https://web.archive.org/web/20210127233239/http://mappingmovement.newberry.org/item/map-military-road-fort-walla-walla-columbia-fort-benton-missouri "Military Road from Fort Walla Walla to Fort Benton, 1863")

[View Full Metadata](/web/20210127233239/http://mappingmovement.newberry.org/item/map-military-road-fort-walla-walla-columbia-fort-benton-missouri)
