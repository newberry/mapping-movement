mm06-0053-road-map-of-the-country-around-new-york-1895.md
﻿---
title: Road map of the country around New York, 1895
parent: Planning Transportation
parentslug: planning-transportation
---

The topography, streets, railroad lines and terminals of the New York City region are shown in magnificent detail in this large-format 1895 map. The prominence of steam railroads is evident in the bold lines denoting their routes, which grab the reader’s attention more than the thinly drawn rapid-transit lines, which were still in various phases of development. The uneven quality of roads is emphasized by Rand McNally’s note that this is a map “With the good roads specially marked.”  
     Grand Central Depot, predecessor of today’s famous Grand Central Terminal, was the endpoint of the only direct conventional rail line into Midtown Manhattan. The existence of this line bestowed enormous market power on the New York Central Railroad, but competition was fierce among elevated rapid-transit (“El”) companies on many routes within the city. The El crossed the Brooklyn Bridge and fanned out through Brooklyn. Trains on these lines, while catalysts for development, primarily used steam propulsion, generating a great deal of smoke and soot.  
     This is the only map featured in this essay that was created primarily to assist in the navigation of travelers on all modes of travel over a large region. Even so, such maps were heavily used by government officials exploring options to improve transportation. This map illustrates the proliferation of rail terminals along the New Jersey shoreline of the Hudson River, with particularly large facilities (from south to north) at Jersey City, Hoboken, and Weehawken.  The crisscrossing configuration of the routes and density of train operations led to significant chaos and commotion along the waterfront. On the opposite side of Manhattan Island, the tracks of the Long Island Rail Road (LIRR), a Pennsylvania Railroad (PRR) subsidiary, can be seen terminating along the East River at Hunter’s Point, thereby requiring passengers to make burdensome ferry transfers to reach Manhattan.  
     In no other major city in America (except perhaps San Francisco) did waterways present such vexing geographical challenges to the railroads. With tunnels and bridges to central Manhattan still years away, ferry service was of paramount importance for both passengers and freight. Another fifteen years passed after this map’s publication before PRR finished tunneling under the Hudson and East rivers and opened the new Pennsylvania Station in 1910, providing its passengers and those of the affiliated LIRR with a magnificent new gateway into Midtown. To this day, however, the tunneling process is arguably incomplete: Swarms of commuters bound for Manhattan from New Jersey still ride trains to the Hudson shoreline and transfer to buses, ferries, or connecting trains.  
     The limited extent of the street grid in outlying regions vividly illustrates just how much more growth would take place. Flushing, NY, (upper right) was only lightly developed at the time, but would be absorbed into the city in 1898. Portions of Brooklyn remained pastoral, despite their proximity to El lines, which were mostly newly built at the time. Huge swaths of land immediately southwest of Newark and on Long Island remained untouched by development. A housing boom in Queens would occur after the opening of the Queensboro Bridge (originally Blackwell's Island Bridge) in 1909. This massive cantilever span handled both vehicles and transit lines.   
     The red line denotes the boundaries of what was considered Greater New York at the time. This area seems miniscule in comparison to what is now considered metropolitan New York. The region had only about five million people at the time, less than a third of today’s population.  
 

Selection Gallery
-----------------

[Road map of the country around New York, 1895](/web/20201024183753/http://mappingmovement.newberry.org/item/rand-mcnally-co%E2%80%99s-road-map-country-around-new-york-including-parts-northern-and-central-new)

**Citation:**

Rand McNally and Company, "Rand McNally & Co.’s Road Map of the Country Around New York" (New York ; Chicago : Rand, McNally & Co., \[1895\], c1894). Map6F G3804.N4A1 1895 .R3

[![](https://web.archive.org/web/20201024183753im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/poster_56.jpg?itok=ON5gxPFc)](https://web.archive.org/web/20201024183753/http://mappingmovement.newberry.org/item/rand-mcnally-co%E2%80%99s-road-map-country-around-new-york-including-parts-northern-and-central-new "Road map of the country around New York, 1895")

[View Full Metadata](/web/20201024183753/http://mappingmovement.newberry.org/item/rand-mcnally-co%E2%80%99s-road-map-country-around-new-york-including-parts-northern-and-central-new)

[![](https://web.archive.org/web/20201024183753im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/MM06-0053-02_01.jpg?itok=CkBX-Qyx)](https://web.archive.org/web/20201024183753/http://mappingmovement.newberry.org/item/rand-mcnally-co%E2%80%99s-road-map-country-around-new-york-including-parts-northern-and-central-new "Road map of the country around New York, 1895")

[View Full Metadata](/web/20201024183753/http://mappingmovement.newberry.org/item/rand-mcnally-co%E2%80%99s-road-map-country-around-new-york-including-parts-northern-and-central-new)

[![](https://web.archive.org/web/20201024183753im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/MM06-0053-02_02.jpg?itok=qMnztpsr)](https://web.archive.org/web/20201024183753/http://mappingmovement.newberry.org/item/rand-mcnally-co%E2%80%99s-road-map-country-around-new-york-including-parts-northern-and-central-new "Road map of the country around New York, 1895")

[View Full Metadata](/web/20201024183753/http://mappingmovement.newberry.org/item/rand-mcnally-co%E2%80%99s-road-map-country-around-new-york-including-parts-northern-and-central-new)
