mm01-0003-guidebook-to-the-boston-and-worcester-and-western-railroads-1847.md
﻿---
title: Guidebook to the Boston and Worcester and Western Railroads, 1847
parent: American Railroad Maps, 1828-1876
parentslug: american-railroad-maps-1828-1876
---

In 1675, John Ogilby published _Britannia_, the first modern collection of strip maps showing the course of specific routes—in his case, the roads of Great Britain—in detail. This type of publication, imitated in the United States by Christopher Colles in 1789 and Matthew Carey in 1802 was applied to railroad routes by the London publisher James Wyld in 1837, when he began a series of guides for passengers on the various railways radiating out of London. William Guild took up the idea in America a decade later, publishing a more elaborate handbook with strip maps for passengers travelling between Boston and Albany on the Boston and Worcester and Western Railroads. The guide covers the entire 200-mile route in exquisite detail from Boston to Greenbush, New York, where passengers had to take a ferry across the Hudson to proceed to Albany. The author decried the idea of tourists reading newspapers while en route when, encouraging them instead to look out of the windows at the passing scene. The miles from Boston printed in bold type (Miles 94-100 for this segment), helped passengers gauge their progress.  
     Though the guide took note of the scenery and its natural history, and other objects passed by this line, “bridges, viaducts, tunnels, cuttings, embankments, \[and\] gradients” receive as much, if not more attention. See, for example, grade level road crossings shown (mile 94), bridges over the tracks (mile 96 at Chickopee Village, where the railroad right-of-way is cut through sand), or where the railroad itself is carried on a viaduct over the road (between miles 99 and 100). In this respect, Guild’s guidebook shares with James Baldwin’s survey of the same route from two decades earlier (Map 1) a keen interest in rail infrastructure and engineering, at a time when rail bridges, viaducts, and even embankments were relative novelties. The Connecticut River wooden truss bridge on granite piers (at mile 99), 1264 feet in length, was a major engineering feature of the route, and Guild accordingly featured it in a separate illustration. Though the map shows two tracks crossing, there was in fact only one, which created a major bottleneck that was not redressed until 1874 when the Connecticut River was crossed by an iron bridge supporting two tracks.  
     Guild went on to publish similar guides to routes between Boston and New York, along the Hudson River, and from New York City to the White Mountains. A shorter passenger guide to the Boston and Albany Railroad presenting “historical associations and other matters of interest of every town” without maps also appeared in 1847, perhaps with railroad sponsorship. In the 1870s, the railroad sponsored similar publications called “Route Books” and “Panoramic Guides.” These, like similar guidebooks published for other rail routes well into the twentieth century, devoted most of their attention to the history and geography of the passing landscape. But in 1847, the railroad itself was the great marvel and the focal point.  

Selection Gallery
-----------------

[Miles 94-100 west of Boston](/web/20210127225625/http://mappingmovement.newberry.org/item/map-detailing-miles-94-100-west-boston-boston-and-worcester-railroad-and-western-rail-road)

**Citation:**

Guild, William, "\[Map Detailing Miles 94-100 West of Boston on the Boston and Worcester Railroad and the Western Rail-Road\]", in _A Chart and Description of the Boston and Worcester and Western Railroads_ (Boston: Bradbury & Guild, 1847), page 44. Map3C G3761.P3 1847 .G8

[![](https://web.archive.org/web/20210127225625im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/MM01-0003-01.jpg?itok=0CH0X-Yc)](https://web.archive.org/web/20210127225625/http://mappingmovement.newberry.org/item/map-detailing-miles-94-100-west-boston-boston-and-worcester-railroad-and-western-rail-road "Miles 94-100 west of Boston")

[View Full Metadata](/web/20210127225625/http://mappingmovement.newberry.org/item/map-detailing-miles-94-100-west-boston-boston-and-worcester-railroad-and-western-rail-road)

[Chickopee River, Mt. Tom, Mt. Holyoke](/web/20210127225625/http://mappingmovement.newberry.org/item/chickopee-river-88th-mile-mt-tom-and-mt-holyoke)

**Citation:**

Guild, William, "Chickopee River, at 88th Mile ; Mt. Tom \[and\] Mt. Holyoke\]", in _A Chart and Description of the Boston and Worcester and Western Railroads_ (Boston: Bradbury & Guild, 1847), page 45. Map3C G3761.P3 1847 .G8

[![](https://web.archive.org/web/20210127225625im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/MM01-0003-02.jpg?itok=UnetO-t_)](https://web.archive.org/web/20210127225625/http://mappingmovement.newberry.org/item/chickopee-river-88th-mile-mt-tom-and-mt-holyoke "Chickopee River, Mt. Tom, Mt. Holyoke")

[View Full Metadata](/web/20210127225625/http://mappingmovement.newberry.org/item/chickopee-river-88th-mile-mt-tom-and-mt-holyoke)

[Connecticut River Bridge](/web/20210127225625/http://mappingmovement.newberry.org/item/connecticut-river-bridge-springfield-massachusetts)

**Citation:**

Lossing, Benson John, "Connecticut River Bridge \[at Springfield, Massachusetts\]", in William Guild, _A Chart and Description of the Boston and Worcester and Western Railroads_ (Boston: Bradbury & Guild, 1847), page 49. Map3C G3761.P3 1847 .G8

[![](https://web.archive.org/web/20210127225625im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/MM01-0003-04.jpg?itok=ti-y_CsT)](https://web.archive.org/web/20210127225625/http://mappingmovement.newberry.org/item/connecticut-river-bridge-springfield-massachusetts "Connecticut River Bridge")

[View Full Metadata](/web/20210127225625/http://mappingmovement.newberry.org/item/connecticut-river-bridge-springfield-massachusetts)

[Interior of engine house, at Springfield](/web/20210127225625/http://mappingmovement.newberry.org/item/interior-engine-house-springfield-massachusetts)

**Citation:**

Lossing, Benson John, "Interior of engine house, at Springfield \[Massachusetts\]", in William Guild, _A Chart and Description of the Boston and Worcester and Western Railroads_ (Boston: Bradbury & Guild, 1847), page 47. Map3C G3761.P3 1847 .G8

[![](https://web.archive.org/web/20210127225625im_/http://mappingmovement.newberry.org/sites/newberry/files/styles/300px-wide-four-columns/adaptive-image/public/MM01-0003-03.jpg?itok=95XNOcMQ)](https://web.archive.org/web/20210127225625/http://mappingmovement.newberry.org/item/interior-engine-house-springfield-massachusetts "Interior of engine house, at Springfield")

[View Full Metadata](/web/20210127225625/http://mappingmovement.newberry.org/item/interior-engine-house-springfield-massachusetts)
