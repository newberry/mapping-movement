Territorial Claims, North America, 1720
Referenced by Essay: 
Moving Pictures: Maps and Imagination in Eighteenth-Century Anglo-America
Moll’s map was a direct response to Guillame De Lisle’s Carte Nouvelle de la Louisiana, published in Paris in 1718. Moll, a German mapmaker working in England, took exception on behalf of his adopted country to De Lisle’s assertions of French dominion over all of North American west of the Appalachians and north of Spanish Florida. Moll reproduced much, but not all, of the information from De Lisle’s map in order to stir Britain towards a more vigorous defense against France’s ambitious claims.

Selection Gallery
Territorial Claims, North America, 1720
Citation:
Moll, Herman, "A New Map of the North Parts of America Claimed by France Under [the] Names of Louisiana, Mississipi, Canada and New France, With [the] Adjoyning Territories of England and Spain" ([London] : H. Moll; Thos. Bowles; John King; John Bowles, 1720). VAULT drawer Graff 2855

 