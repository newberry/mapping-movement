import adapter from '@sveltejs/adapter-static';
// import preprocess from 'svelte-preprocess';
import path from 'path';
import { vitePreprocess } from '@sveltejs/kit/vite';

const dev = process.argv.includes('dev');

/** @type {import('@sveltejs/kit').Config} */
const config = {
	preprocess: [vitePreprocess()],
	kit: {
		adapter: adapter({
			precompress: true
		}),
		paths: {
			base: dev ? '' : '/mapping-movement'
		},
		prerender: { entries: ['/'] },
		alias: {
			$data: path.resolve('./src/data'),
			$lib: path.resolve('./src/lib'),
			$utils: path.resolve('/src/lib/utils'),
			$src: path.resolve('./src')
		}
	}
};

export default config;

