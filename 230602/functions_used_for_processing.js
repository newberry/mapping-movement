let masterMonster = allData.map((ed) => {
    let md = mapData.filter((m) => m.parent === ed.title);
    ed.maps = md;
    let perif = periph.filter((p) => p.title === ed.title).pop();
    console.log(perif);
    ed.bib = perif.bibliography;
    ed.fr = perif.fr;
    return ed;
});


let cortexApiUrl = [
    'https://collections.newberry.org/API/search/v3.0/search?query=Text:',
    '&fields=CoreField.Identifier,Document.CortexParentLink,Document.CortexShareLink,Document.ParentIdentifier,Title&format=json'
];

let apiUrls = imageMapping.map((essay) => {
    let urls = essay.maps
        .filter((m) => m.image_filename.length > 0)
        .map((m, idx) => {
            let filename =
                m.text_filename.length > 0
                    ? m.text_filename.replace('.txt', `_${idx}.txt`)
                    : essay.essay_text_filename.replace('.txt', `_${idx}.txt`);
            return 'wget -O ' + filename + ' ' + masterMonster[0] + m.image_filename + masterMonster[1];
        });
    return urls;
});


import apiData from '$data/_allApiData.json';
let foundWithIiif = apiData.filter((apid) => apid.items.some((adi) => adi['Document.IIIFV3ID']));
let notFound = apiData.filter((apid) => apid.items.every((adi) => !adi['CoreField.Identifier']));
let foundButNoIiif = apiData.filter((apid) => {
    let noIiif = apid.items.every((adi) => adi['Document.IIIFV3ID'] === '');
    console.log(noIiif);
    return apid.items.length > 0 && noIiif;
});
let yesAndNo = apiData.filter((apid) => {
    let noIiif =
        apid.items.some((adi) => adi['Document.IIIFV3ID'] === '') &&
        apid.items.some((adi) => adi['Document.IIIFV3ID'] !== '');
    console.log(noIiif);
    return apid.items.length > 0 && noIiif;
});
let moreThanOneResult = apiData
    .filter((apid) => apid.items.length > 1)
    .map((apid) => {
        let itemcount = apid.items.length;
        apid.itemcount = itemcount;
        apid.items = 'f this for now';
        return apid;
    });