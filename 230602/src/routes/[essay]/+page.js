
import essayData from '$data/allData.json';

export function load({ params }) {
  // DO NOT DELETE.  Acts as a "a = a" in order to refresh page data on url change
  params.essay
  // console.log("params.essay",params.essay)
    return essayData.filter((ed) => ed.path[1].indexOf(params.essay) > -1).pop()
  }

// I have a **[essay]/+page.js** which takes essay text from a local json file, and filters it based on path, eg:  
//      data = essays.filter((ed) => ed.path[1].indexOf(params.essay) > -1).pop()
// the **[essay]/+page.svelte** takes the $page.data and formats it per usual
//      let content = $page.data
// my sidebar has links to all the essays 

// the links change the url, and the initial essay loads just fine; but going from one essay to another doesn't refresh the data; the original essay remains.
// From googling, I think I understand that the issue is that the change will only occur if 
//    1. the variable whose value needs to change exists on the left side of an equasion where a changing dependency is on the right
//    2. the variable is bound to the changing paramater, eg 
//      $: content = $page.data