import { writable } from "svelte/store";
export const miradactive = writable(false)
export const top = writable(false)
export const pageTitle = writable(['Mapping Movement'])