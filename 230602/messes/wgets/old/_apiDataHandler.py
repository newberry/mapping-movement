# _apiDataHandler.py
import os, json

outputArray = []

for root, dirs, files in os.walk('./bikes'):
    for filename in files:
        if filename.endswith('.json'):
            f = open(os.path.join(root, filename))
            data = json.load(f)
            outputObj = {}
            outputObj["filename"] = filename
            outputObj["items"] = data["APIResponse"]["Items"]
            outputArray.append(outputObj)
            f.close()

         
with open("_bikes_allApiData.json", "w") as outfile:
    json.dump(outputArray, outfile)