The Most Inhabited Part of Virginia, 1755
Referenced by Essay: 
Moving Pictures: Maps and Imagination in Eighteenth-Century Anglo-America
Fry and Jefferson’s (father of Thomas) map owed its origins to the contest over the Ohio lands. The two Virginia planters had been appointed as commissioners to help extend the Virginia-North Carolina boundary in 1749 and were again called upon by the governor of Virginia to create a map in 1750. Much of the information on the Ohio Valley likely came from Fry as his colonel’s commission in the Virginia militia took him to the western part of the colony in the early 1750s. While stationed at Winchester, Fry had access to the information of military men, land speculators’ surveys, and the occasional Indian scout/diplomat. On Fry’s death in 1755, his quartermaster John Dalrymple took Fry’s updated version of the map to London for publication.

Selection Gallery
The Most Inhabited Part of Virginia, 1755
Citation:
Fry, Joshua, "A Map of the Most Inhabited Part of Virginia Containing the Whole Province of Maryland : with Part of Pensilvania, New Jersey and North Carolina" (London : Thomas Jefferys, [1755]). VAULT map7C G3880 1751 .F7 1755

 