Colonial Post Roads, 1773
Referenced by Essay: 
Mapping Communication
In 1773, the British postmaster general sent surveyor Hugh Finlay on a ten-month journey through Britain’s American colonies to map and assess the colonial mails. Finlay’s maps and journals offer a detailed portrait of both communications and political unrest on the eve of the American Revolution.
     Finlay set out from Quebec City in September 1773, traveling by canoe with a scout, two military officers, and several Native guides. This map, copied from Finlay’s own hand-drawn map when his journals were published in 1867, shows the first leg of his travels, with a proposed route for a new postal road from Quebec to “the Massachusetts Province,” or present-day Maine. Early in his journal, Finlay criticized the map-making abilities of his Abenaki guides, claiming, “It is impossible to guess distances from an Indian draft; that people have no idea of proportion.” But he also admitted his dependence on them. Every night, Finlay noted, their guide Mentowermet sketched out the next day’s route on birch bark, marking ponds, lakes, marshes, ascents and descents. “He was right in everything but distances,” Finlay wrote (5-6).
     After reaching Fort Halifax (at the map’s bottom-left; it is now Winslow, Maine), Finlay traveled south by postal roads through New England to Philadelphia. Then he sailed to Charleston, South Carolina, where he began a journey northward to Virginia. Along the way, he interviewed postmasters and postal carriers, keeping careful record of routes and delivery times. Finlay discovered many Americans sending messages outside the postal system to avoid British postage or surveillance. Even official postal carriers were illegally taking on letters outside the mails for extra pay. Britain’s deep unpopularity in the colonies made it difficult to stop such practices. “Were any Deputy Post master to do his duty and make a stir in such matter,” Finlay wrote, “he would draw on himself the odium of his neighbors and be mark’d as the friend of Slavery and oppression and a declared enemy to America” (32).
     Staunchly loyal to the British Crown, Finlay was rarely welcomed on his travels, and by June 1774 he feared enough for his safety that he cut his expedition short. After British authorities fired Benjamin Franklin for leaking documents to aid the colonial cause, Finlay was named to replace him as deputy postmaster for British North America. Within a few months, the American colonies were in open revolt. Finlay fled to Quebec, where he would serve as postmaster for the remaining British colonies until 1799 (Steele 1983).
 

Selection Gallery
Colonial Post Roads, 1773
Citation:
Finlay, Hugh, "Hugh Finlay’s Tract from Canada to the Massachusetts Province", in Journal Kept by Hugh Finlay, Surveyor of the Post Roads on the Continent of North America, During His Survey of the Post Offices Between Falmouth and Casco Bay, in the Province of Massachusetts, and Savannah, in Georgia (Brooklyn : Frank H. Norton, 1867), following p. xxv. Case G 831 .298


View Full Metadata
Colonial Post Roads, 1773
Citation:
Finlay, Hugh, "Route from New London to Saybrook", in Journal Kept by Hugh Finlay, Surveyor of the Post Roads on the Continent of North America, During His Survey of the Post Offices Between Falmouth and Casco Bay, in the Province of Massachusetts, and Savannah, in Georgia (Brooklyn : Frank H. Norton, 1867), following p. 38. Case G 831 .298