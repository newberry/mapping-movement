"title": "Christopher Columbus's Letter, 1494",
"parent": "Maps, Movement, and American Literature",
"text": [
	"Two woodcuts illustrating the pamphlet De Insulis nuper inuentis (1494) were among the first “maps” offering visual evidence of the existence of a new-found world. They were published in 1493 inside a pamphlet containing Latin translations of three letters sent by Christopher Columbus (1451-1506) to the Spanish Court in which he announced the discovery of islands in what he wrongly believed to be the Sea of India and thus the gateway to Cathay (China).",
	"Drawn in the tradition of late medieval landscape art—and thus devoid of modern notions of proportion, scale, and the rules of perspective—the two illustrations offer complementary views of Columbus approaching America as a geographical place, travel destination, and human habitat. The illustration on the right shows Columbus’s ship, the Santa Maria, as a multi-oared Mediterranean galley at anchor in the foreground; on the illustration’s center right, the artist shows Columbus, standing up in a row boat, approaching the island of Haiti (“Insula Hyspana”) and on the way of making contact with a group of disproportionately drawn figures representing Native Americans. The second illustration on the left offers a cartographic overview of the islands discovered and named by Columbus. They include Fernada, Hyspana, Ysabella, Saluatorie, and Conceptionis Marie."
	"The eight-page pamphlet was an immediate bestseller. Between Spanish and Latin editions, and Tuscan and German translations, it is estimated that some ten thousand copies of the illustrations were available to a literate minority within only five years of his voyage. Reflecting the artistry of untravelled artists who created map-like representations by following medieval conventions for depicting natural and human spaces, the illustrations proved to be highly influential, celebrating Columbus’s claim to have found a passage to China while providing first visual clues to a mostly illiterate European audience about previously unknown American lands and peoples."


Selection Gallery
Insula Hyspana
Citation:
Columbus, Christopher, \"Insula Hyspana\", in  \"De Insulis In Mari Indico Repertis\", issued in Carlo Verardi's Historia Baetica. In Laudem Serenissimi Ferdinandi Hispania[rum] Regis Bethicae & Regni Granatae Obsidio, Victoria, & Triu[m]phus (Basel : Johann Bergmann, 1494), folio 29b. VAULT Ayer 107.56 1493 1494

View Full Metadata
Ferna[n]da, Hyspana, Ysabella, Saluatorie, Conceptons Marie
Citation:
Columbus, Christopher. \"Ferna[n]da, Hyspana, Ysabella, Saluatorie, Conceptons Marie\" in his De insulis in Mari Indico repertis, issued in Carlo Verardi's Historia Baetica. In laudem Serenissimi Ferdinandi Hispania[rum] regis Bethicae & regni Granatae obsidio, victoria, & triu[m]phus (Basel : Johann Bergmann, 1494), folio 31b.