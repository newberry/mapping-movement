{
	"title": "Ancient Courses, Mississippi River Meander Belt",
	"parent": "State and Federal Mapping of Infrastructure and Movement",
	"text": [
		"The Mississippi River Commission was created in 1879 in order to stabilize the river for flood control and navigation. But the 1927 flood demonstrated that existing practices were inadequate, and heightened the urgency of preventing future floods, managing the flow of the river, and stabilizing its banks. The ensuing economic depression of the 1930s generated support for large-scale public works that would advance this end. One of the most notable scientists and engineers in this endeavor was Harold Norman Fisk, a geologist who began to study the Lower Mississippi Valley in his work for the Louisiana Geological Survey. Fisk was fascinated by the prehistoric and modern courses of the river, and experimented with representing this dynamic through maps. In 1941 he convinced the President of the River Commission to support a comprehensive study of the geomorphology of the lower Mississippi River.",
		"Fisk’s colleagues remember him as a demanding leader—even rude and confrontational—but also brilliant. His rigorous two-year study paid off when he produced a massive report with over 100 maps and graphs documenting the complex geological history of the river. He used aerial photographs to search for abandoned channels and patterns of soils, vegetation, and drainage. He also drew on historic maps to chart the course of the river at more recent moments: 1765, 1820, 1880, and 1940. This evidence was combined with extensive fieldwork and prior research in order to reconstruct the evolution of the alluvial plain.",
		"The centerpiece of the report was a set of fifteen maps that each covered a section of the Mississippi River Valley, from Cape Girardeau to Donaldsonville (above New Orleans). At a scale of one inch per mile, the intensely colored maps graphically identify the ever-changing course of the river and the process of sedimentation, both before and since the modern meander belt of the river was formed about 6000 years ago. Each of the shading techniques marks a particular stage in the evolution of the river, including its many cutoffs. This layering effect reconstructed the sequence of events in the long evolution of the river and its valley. The maps are admittedly complex, but they allowed geologists to answer specific questions about the behavior of the river over geologic time as well as recent history. Such information—elegantly and intuitively presented—must have been most welcome in the aftermath of such a devastating flood.",
		"The maps were printed by the Army Map Service during the Second World War, and in fact the War made the river into an important source of transportation, which heightened the attention paid to this research. The report was published in 1944, and it brought Fisk immediate attention and wider latitude to extend his research into related areas. Soon petroleum geologists recognized the implications of his work for oil exploration, particularly the possibility of searching for stratigraphic traps that might contain reserves of oil and gas. In the 1950s he put his skills to work in Houston—with substantial success—yet he continued his geological research throughout that time until he died of leukemia in 1964."
	],
	
	"maps": [
		{
			"maptitle": "The Changing Path of the Mississippi",
			"mapsource": "Fisk, Harold Norman, \"Ancient Courses, Mississippi River Meander Belt, Cape Girardeau, Mo.-Donaldsonville, La.\", in Geological Investigation of the Alluvial Valley of the Lower Mississippi River (Vicksburg, Miss. : Mississippi River Commission, 1945), Atlas Plate 22, Sheets 2, 3, 7, and 9. Not held by Newberry Library (November 2014)",
		},
	],
	"textfilename:": "1945_ancient_courses.txt",
	"subpath":"1945-ancient-courses",
},
	
	