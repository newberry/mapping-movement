<script>
	import Essay from '$lib/essay.svelte';
	import Placeholder from '$lib/placeholder.svelte';
</script>

<Essay>
	<div slot="title">
		<h1>Around, Over, and Through the Americas</h1>
	</div>
	<div slot="author">
		<p class="author">
			<em> by: </em>
			<span> JORDANA DYM </span>
		</p>
	</div>
	<div slot="body">
		<article>
			<section>
				<p class="body-text">
					In 1922, Berkeley student Frances Toor dedicated her MA thesis on the Moquí people of
					Spanish New Mexico to Professor Herbert Eugene Bolton for “introducing me to the
					interesting study of Spanish documents, [which] opened for me new horizons in the study of
					American History.” Toor, whose interests in folklore soon took her to Mexico and a career
					guiding American tourists, shared Bolton’s vision of the interconnected history of the
					Americas. After integrating the Spanish borderlands of North America into US history
					Bolton called for studies of hemispheric parallels and connections. His vision may have
					been radical in early twentieth-century North American academic circles, but not to
					readers of José Enríquez Rodó (Ariel, 1900) and other Latin American scholars whose
					americanismo was wary of an increasingly influential and internationalist United States
					but did not rule out a hemispheric consciousness, nor the more contemporary hemispheric
					histories of Historians like Felipe Fernández Armesto (2005). In fact, this Americanist
					perspective would have been very familiar to those who, since Martin Waldseemüller dubbed
					the lands of the New World “America” in his 1507 world map, represented the hemisphere as
					a continent or fourth part of a world formerly thought to have three: Africa, Europe and
					Asia (Lois 2012).
				</p>
				<p class="body-text">
					North Americans and Europeans have traveled around, over, and through Latin America since
					Columbus set foot in the Caribbean, creating or using maps to show their paces, paddles,
					and pit stops. Since achieving independence in the nineteenth century, Latin Americans
					have also “mapped back,” offering their own visions of linkages between the Americas and
					beyond. This essay offers a selection from the vast and diverse cartographies of movement
					by land, sea, and air, mapping the hemisphere’s enduring geographical and human
					connections.
				</p>
				<p class="body-text">
					What connects the maps introduced below—all part of the Newberry Library’s impressive
					holdings—is their circulation among a general public between the era of the 1823 US Monroe
					Doctrine and Simón Bolívar’s 1826 Congress of Panama to the Good Neighbor policy and
					foundation of the Organization of American States in 1948. Whether produced by
					governments, corporations, artists, scholars, or journalists for use in atlases,
					advertising or en route, these popular maps demonstrate how Americans of all kinds
					traveled specifically for business and pleasure, war and peace, and, always, to discover,
					explore, and know the Americas just a little better.
				</p>
				<p class="body-text">
					It is impossible in ten maps to offer a comprehensive collection of cartography of
					conquest, colonization, commodities, commerce, or communication between the Americas.
					Aiming smaller, this essay and its maps focus on three big ideas—exploration, extraction,
					and experience—which many maps from these places and the long nineteenth century presented
					to the men, women, and children who moved in the hemisphere during Latin America’s first
					independent century. Road maps, atlases, newspaper and magazine articles, advertisements,
					corporate brochures, travel accounts, geography books, and government reports will reward
					those inclined to investigate further with many more possibilities of finding routes to,
					through, and around the hemisphere.
				</p>
			</section>
			<Placeholder />
			<section>
				<h2 class="section-header">Sea: Around the Coasts</h2>
				<p class="body-text">
					The sea brought Europeans and their descendants to the Americas in the sixteenth century,
					where they struggled to find easy overland routes and instead sought out river travel from
					the Mississippi to the Amazon to ease tough inland journeys. Sea charts since this period
					have offered a particular “coastal vision” of the lands reached, and for
					eighteenth-century Latin America emphasized both getting to the shores and getting around
					them, whether to move French military and trading vessels around and between French
					overseas territories as the manuscript Cartes Marines (1726) (See Map 9) reveal, or to
					track the routes of whaling vessels such as that of James Colnett of Britain’s Royal Navy
					(1798). Small-scale charts tended to follow portolan tradition and concentrated geographic
					information at the shore—naming river mouths, sholas, and harbors that pilots might look
					for—while larger scale maps would fill in fortifications, information of potential
					opposition (reported “anthropophages,” or cannibals, in the case of a Cartes Marine chart
					of the Louisiana coast).
				</p>
				<p class="body-text">
					Sea charts, however, don’t tell the full story of the importance of water as a means of
					moving people, products, and projects in the Americas. Land maps of the nineteenth and
					early twentieth centuries both inscribed the seabornee history of exploration and conquest
					and showed successive coastal and river routes, sometimes in the same map. In the region,
					Agustín Codazzi’s 1840 Atlas de Venezuela (See Map 1), a pioneering Latin American
					national atlas, included Columbus, Cortes’s and Pizarro’s routes of exploration and
					conquest for sixteenth-century Spain on a geographic map ostensibly devoted to the
					hemisphere’s contemporary political and demographic makeup. He also brought readers up to
					date on contemporary sea exploration. On the same map, Codazzi plotted two contemporary
					journeys in North America. The United States’ Lewis and Clark Expedition of 1806, which
					followed the Missouri and Columbia Rivers to reach the Pacific Ocean in North America.
					Commercially-inspired scientific exploration by British sailors John Franklin, John Ross
					and Alexander Mackenzie sought in the 1810s and 1820s to find the elusive northwest
					passage and magnetic north. Codazzi, himself a former Napoleonic officer, seemed to be
					comparing sixteenth and nineteenth century waterborne imperialisms from the safety of
					Venezuela, his adopted home. Was he making an argument finding equivalence between those
					coming to the region to conquer and those seeking ways through it in search of a
					transcontinental route and Northwest Passage?
				</p>
				<p class="body-text">
					Turn of the twentieth-century maps with sea routes produced for Latin American publics
					tended to highlight neither exploration nor conquest but two established and modern
					moneymaking ventures for the region: Commerce and tourism. The Brazilian Ministry of
					Industry’s 1908 commercial map (See Map 5) eschewed showing waterborne exploration in
					favor of a naked message about the sea’s importance to commercialism. The map commemorated
					a century of open ports, pointedly naming international shipping partners and marking
					their routes to Europe, Asia, and Africa, as well as North America, and identifying
					national river, coastal and international steamship companies. Such nationally-sponsored
					maps touted Latin America’s accessibility by ocean highways when printed at home, like
					Brazil’s map, or abroad. Richard Mayer of New York’s 1920 “Commercial Map” of Colombia
					(Mayer 1920) pointedly showed an active Caribbean coast steamer network, British, French,
					Dutch and other companies transiting the newly-opened Panama Canal, and also directed
					readers inland to the country’s navigable rivers and extended railway network, along with
					a few ropeways.
				</p>
				<p class="body-text">
					Nineteenth- and twentieth-century North American mapmakers, too, devoted energy to mapping
					routes and transport possibility for Latin America’s waters. In the 1850s, the US Navy
					explored and hoped to exploit the Amazon and River Plate regions by turning rivers into
					international waterways; nationalism and environmental and technological challenges
					quickly ended that initiative. By 1905, the United States was creating its own waterway,
					the Panama Canal, to increase economic, political, and strategic influence. Businesses
					investing in and exporting coffee, bananas, nitrates, and other raw materials founded
					steamship companies that earned extra money by contracting to carry the US mail and
					passengers. Their brochures and glossy magazine spreads often sported maps alongside
					illustrations inviting visitors to experience a relief from winter, or a romantic break.
					Montevideo-born California-based artist Jo Mora’s 1933 vignettes of conquistadors and
					annotations next to galleons drawn on the seas in a seriously entertaining poster “carte”
					or map for the cruise company Grace Lines, is just one example (See Map 3). Mora’s
					illustrations offer a counterpoint to Codazzi’s route-focused depiction of Spanish
					exploration. In addition, Moras’s inset map of the Panama Canal, a highlight of the
					steamer journey, touts the miles saved when this engineering marvel and new water route
					opened in 1914. Notably absent are the routes plied by Grace’s North American and European
					competitors seen in the Brazilian map, an important silence to consider when thinking
					about the contributions of privately-sponsored maps to our understanding of American
					mobilities (See Map 8).
				</p>
			</section>
			<Placeholder />
			<section>
				<h2 class="section-header">Land and Air: Across Desert and Jungle, Over the Mountains</h2>
				<p class="body-text">
					If sea charts and cruise lines focused attention on getting to and around the Americas by
					water, land maps drew in those needing to enter and cross sometimes-challenging terrain.
					Cartographic emphasis and content differed substantially depending on which means of
					transportation a map described: Walking or riding a mule or horse leads to a different
					cartographic perspective and relationship with the land than riding in machine-age
					transportation. Although railroad and highway routes might be similar to footpaths and
					carriageways, trains and automobiles generally transformed land from a barrier into a
					comfortable, efficient means of communication and commerce (Salvatore 2006).
				</p>
				<p class="body-text">
					The difference in the two perspectives comes through clearly when comparing North American
					maps reporting on a military expedition to still-Spanish Santa Fe and Los Angeles in 1846
					(See Map 10), a Boston writer’s railway journey only a decade later (See Map 7), and an
					English-language motorists’ guide book, produced by former student and expatriate guide
					Frances Toor in Mexico almost eighty years later (See Map 4). The journals and maps of the
					topographic engineers in the US Army of the West, tasked with mapping the Santa Fé trail
					as well as their battles with the Mexican Army in 1846-1847, address explorer’s hardships
					and uncertainties (water, shelter, vulnerability to attack) that not only Lewis and Clark
					but Alvar Nuñez Cabeza de Vaca, Alexander MacKenzie, or any other explorer of the day
					would have found wearily familiar. Information about where to pasture supply animals or
					take on water was perhaps as valuable as taking accurate measurements to place settlements
					on the map. In contrast, Robert Tomes in 1855 on the Panama Railroad and Frances Toor in
					1933 presenting Mexico’s new highways, could draw on existing data or maps to demonstrate
					how rail and later motor travel would enhance a passenger’s experience. The maps (and
					commentary) by Tomes, written when North Americans were still migrating West, and by Toor,
					when they were heading south on vacation, were geared at those who might travel for
					business or pleasure.
				</p>
				<p class="body-text">
					In addition to maps of land travel that sought to open new paths or make known paths
					easier to follow, others retraced steps and even presented solutions to academic riddles.
					In the eighteenth century, scientists Charles Marie de la Condamine, Antonio Ulloa, and
					Jorge Juan helped the French and Spanish crowns determine the shape of the earth, and
					Alexander von Humboldt drew on his travels in the Americas to provide a new theory of
					nature. By the mid- nineteenth century archaeologists from Frenchman Philippe François de
					la Renaudière in Mexico, American John Lloyd Stephens in the Yucatán and Honduras, and
					Briton Clements Markham in Perú uncovered Mayan and Inca ruins, medicinal plants, and
					novel flora and fauna. They plotted and published maps and plans of ruins and their routes
					of travel in popular and scholarly journals, as well as in bestselling travel accounts.
					Such explorations (and the maps they produced) continued into the twentieth century, when
					the hardships of walking were a welcome challenge and opportunity to revisit history,
					rather than a daunting impediment. Thus, Hiram Bingham, more famous for a later expedition
					that recovered Machu Picchu, opted to retrace a prior expedition’s route in 1906 to decide
					whether Simón Bolívar really had led exploits as impressive as those of Hannibal and
					Napoléon crossing the Alps when he led campaigns over the cordilleras of the Andes. The
					map of his mountainous route (See Map 2) might thus have more in common with that of
					military mapping than a tourist route, even though the privations he underwent were of his
					own devising (as he avoided the regular well-supplied routes that Bolívar, too, had to
					skip in order to surprise the enemy).
				</p>
				<p class="body-text">
					Mountains, in fact, were the biggest obstacle to transiting the Americas by land,
					complicating everything from military maneuvers to freight transport to railroad
					construction. So it was the third dimension that provided a solution: Mapping by and from
					the air commenced in the 1920s, providing the fastest and (eventually) easiest route from
					urban North America to urban South America, stopping at military and rural landing strips
					on the way. Maps of these air routes—to celebrate them in newspaper or glossy magazine
					stories and to entice travelers with them in airline company brochures—borrowed from
					conventions developed for rail and auto travel, generally giving attention to the
					supposedly straight lines linking stops on the route and emptying the map space of all
					geographic referents except points of departure and arrival, with the occasional
					punctuation of a particularly dramatic mountain whose impressive height the airplane could
					scale (See Map 6).
				</p>
			</section>
			<Placeholder />
			<section>
				<h2 class="section-header">Mobilizing Movement</h2>
				<p class="body-text">
					The maps that comprise this section were produced in a time of technological, political
					and economic developments that transformed the hemisphere. A little bit of background
					should help put the maps not only in conversation with each other, but with their time
					period.
				</p>
				<p class="body-text">
					In the early nineteenth century, all of mainland America faced important transitions:
					Political independence, renegotiated political systems, alliances, and territorial extent.
					By the end of the century, stability had been achieved, but the first half of the century
					was rocky; the United States experienced the War of 1812 and Civil War, while Latin
					American countries experienced numerous internal and international conflicts and
					dissolutions of federations into smaller countries. Initially, despite the Monroe
					Doctrine’s “keep out” message, European powers exercised extensive soft power and were
					Latin America’s principal sources of investment, exchange and technology. When the United
					States took over building the Panama Canal from the French, however, a baton was passed;
					engineering not just a pathway between the oceans but also the independence of Panama
					confirmed the arrival of North America as a Hunter (Teddy Roosevelt), presciently
					recognized by Nicaraguan nationalist Ruben Darío in “Ode to Roosevelt” (1904) as joining
					the cults of Mammon (wealth) and Hercules (power). This Hunter would not hesitate to
					behave as the region’s “policeman” putting “boots on the ground” in the circum-Caribbean
					repeatedly until the early 1930s. José Enríquez Rodo’s 1900 essay Ariel put the divide in
					terms of “spirit” against “matter,” and was as a basis of a Latin American solidarity set
					in opposition (at times) to the American vision.
				</p>
				<p class="body-text">
					US versions of Pan-Americanism, and three “Pan-American transportation utopias” described
					below, undergirded this expansionism. The US had been skeptical about Bolívar’s Spanish
					American-driven Pan-Americanism in 1826, when it was just one small English-speaking
					country in a Spanish- or Portuguese-speaking community of nations. But its policy changed
					upon growing into a continental power. From the 1880s, the US promoted and participated in
					the meetings of the Pan-American Conference and promoted subsequent regional institutions,
					including the Pan American Geographic and History Institute (1928). Its Latin American
					neighbors interpreted this interest as both contribution and threat. Fostering hemispheric
					projects that advanced its own strategic, economic and political interests, the US
					unsuccessfully advocated for a Pan-American Railway system in the 1880s but got traction
					for a Pan-American Highway (largely built from 1925 to 1945) and Pan-American air routes
					(accomplished by 1929) (Salvatore 2006, 666-679). Each project led to surveys and
					mapmaking, national and transnational, official and popular. The US government also
					enlisted mapmakers to help sell its policies. If Latin American companies or individuals
					based in Latin America, like Frances Toor in Mexico were selling the American public
					guidebooks with road maps of Latin America by the 1920s, the Postwar hemispheric “Good
					Neighbor” initiative brought the major US road map producers into the circuit.
				</p>
				<p class="body-text">
					Economically, this period was a boom time in the Americas. Between 1850 and 1913, Latin
					American exports grew 1000%, making up about 5-8% of global trade and about a third of all
					tropical exports, for only about 3-4% of the overall population. However, the impact of
					economic growth and mechanization was uneven. In 1890, Brazil surpassed Japan in per
					capita wealth and was about equal to Russia, while by 1914, Argentina was one of the
					world’s five richest countries. However, in 1913, the wealth of thirteen of Latin
					America’s twenty countries derived from just a single product, and five
					countries—Argentina, Brazil, Chile, Cuba and Mexico—earned over 80% of it. Foreign
					investment—largely British, French and German in the early nineteenth century,
					transitioning to North American by the close of World War I—brought capital and new
					infrastructure, but tended to benefit home offices and enclaves and not society as a
					whole, and was also subject to international market fluctuations. Additionally, imported
					engineers, locomotives, railway stock and other finished products did not contribute
					appreciably to industrialization (Topik and Allen 1998, 8-11; Halsey 1914, Appendices 1
					and 2).
				</p>
				<p class="body-text">
					As Latin America became increasingly integrated into world commerce, transportation’s
					mechanization meant foreigners and products on the move into and out of Latin America
					spent decreasing amounts of time on a journey. In the first half of the nineteenth
					century, most North Americans were more interested in traveling across or around Latin
					America than to it; faster transportation methods meant that they spent less and less time
					in the region they transited. Tens of thousands of US citizens just wanted to pass
					through, choosing to cross Panama or Nicaragua rather than the US overland route during
					the gold rush. Traveling by wagon from Independence, Missouri to Sacramento, California in
					1850 could take up to four months, a journey that could only be undertaken in summer. The
					Panama Railroad reduced interoceanic transit from about four days to five hours in 1855
					and three hours by 1860, for up to 1500 persons and the freight of three steamships;
					steamship travel on either end completed the whole trip in around three weeks (Otis 1867,
					56, 161), about half the time needed to sail around Cape Horn. Europeans and North
					Americans who did come to explore, to seek ancient civilizations, invest or participate in
					one of the many internal and international conflicts, or to map, like Codazzi in Venezuela
					and Colombia, Richard Schomburgk in Guayana, and Maximiliano von Sonnenstern in Central
					America, could be counted by the dozens or hundreds, not the thousands.
				</p>
				<p class="body-text">
					Helped by the faster transportation, a sea (and air) change was also taking place. As
					boats got sturdier, safer, and swifter, the tropics began their transition in outsiders’
					eyes from insalubrious traps to healthful and relaxing resorts. The Caribbean, Mexico, and
					Central America were both faster and easier to reach than South America, and more popular
					destinations for North American travelers.
				</p>
				<p class="body-text">
					For those headed to the Caribbean, the increase of investors and visitors was perhaps most
					noticeable in Cuba, where the number of US-flagged vessels visiting annually more than
					tripled from 1800 to 1852, from 606 to 1886, as US businessmen invested in sugar
					plantations and copper mines, as merchants opened restaurants, hotels, and retail stores,
					as engineers and machinists worked on railroads and sugar processing. Tourists soon
					followed. Richard Henry Dana and Julia Ward Howe respectively published their impressions
					of a “vacation voyage” and a “trip” to Cuba in 1859 and 1860, in steamships that reached
					the Caribbean in less than a week. Helped by improvements in sanitation and services,
					North Americans stopped fearing the Caribbean tropics as unhealthy and began to plot
					winter trips for their therapeutic benefits, although it would be a few generations before
					the sun and beaches, rather than the mountain air and springs, became the principal
					attraction. Lou Perez Jr. puts the annual mid-nineteenth century influx of United States
					visitors at 5000 per year. Nor was Cuba alone; Jamaica went from “record” arrivals of 500
					foreigners in 1873 to having 20,000 in 1895 brought by a single operator (Taylor 1993, 4).
					By 1949, Bermuda expected 60,000 tourists, and with air travel, tourism became the
					“largest single factor in the economic development of the Caribbean.” By 1960 a million
					tourists brought in $140m to the Caribbean, more than the “total US foreign aid to all
					Latin America.”
				</p>
				<p class="body-text">
					Mexico and Central America also grew as destinations for international tourism. By 1937,
					the New York Times reported reaching Guatemala City from New York City in twenty-four
					hours. Americans, beginning with Prohibition, increasingly found Mexico by bus and car,
					and kept coming; by 1950, Mexico received over $135m in tourist business. Still, those
					numbers are relative; in 1930, only 4% of “Uncle Sam’s Tourists” opted for Latin America
					over Western Europe, where they spent the other 90% of the billion dollars expended on
					foreign travel. In 1954, Latin America received only about 3% of the $1 billion that
					Americans spent on travel a year, and even this went primarily to areas “north of the
					canal.”
				</p>
				<p class="body-text">
					South America, even in the early years of air travel, took days and weeks to reach from
					the North, and few tourists made the effort. In 1921, Grace Log of Grace Lines reported
					that American-flag carriers’ passengers from New York reached the Panama Canal six days
					later, Lima in another week, and Valparaiso, Chile after twenty days; those headed to
					Buenos Aires faced another three days of railroad travel over the Andes. Plane service
					shortened the trip to less than a week by the early 1930s, yet did not expand as quickly
					or rapidly as expected. In South America, however, fewer tourist attractions—ruins, spas,
					or great cultural centers—or resort destinations beckoned. In 1922, a US businessman’s
					guide estimated the number of North Americans living in South America at 3000 for the
					continent, and as late as 1949, Mexico was raking in $90 million a year when Brazil only
					received $6 million from tourists to carnival. In 1959, Pan Am still flew only twice a
					week to Buenos Aires, taking over fourteen hours, although it had daily service to
					Venezuela where travelers could take connecting flights. Not until 1960 did the flight
					from New York to Rio drop from a full day and night to nine hours.
				</p>
				<p class="body-text">
					If few North Americans vacationed in the southern cone, however, it is important to
					remember that South America, principally Brazil and Argentina, attracted millions of
					immigrants from Europe, primarily from Spain, Portugal, and Italy. These aspirants filled
					berths in European steam lines’ western crossings, much as Gold Rush migrants had filled
					North American boats. And millions traveled within the Americas, although more often for
					work than for pleasure. The Panama Canal project and many Central American banana farms
					relied on West Indian laborers, who were more mobile than one might expect. Alejandro
					Miranda, a working-class Nicaraguan, traveled from the late 1870s to early 1900s by land
					working odd jobs throughout Central America as a young man, was a railroad engineer in
					Mexico and Cuba, and returned to Nicaragua to operate a telegraph office, worked as a
					journalist throughout Central America, and was later a railroad engineer in Cuba. Like
					fellow telegrapher and Colombian novelist Gabriel García Márquez’s father, he was just one
					of the many Latin Americans whose mobility expanded with the railroad and communication.
					Some, like José Martí, even worked in the US for a while, sometimes (like Martí) driven
					into exile by political conditions. Even though freight, not passengers, inspired the
					construction of most Latin American railways, they still carried substantial and growing
					numbers between city and countryside. Argentina transported 750,000 people and 71,500
					metric tons of freight over 154 miles of railroad in 1865; by 1912, with over 20,000 miles
					of rail (the world’s ninth longest system), 68 million passengers and almost 34 million
					metric tons of freight, it was a powerhouse; by that time, Colombia’s lines carried more
					than a million passengers a year, and Henry Meigg’s railway in Peru over 2.4m passengers
					in 1912/13. (Halsey 1914)
				</p>
				<p class="body-text">
					With so much personal, commercial, and official interaction between the Americas by the
					1930s, when railroad, steamship, automobile, and airline linked capitals and hinterlands
					and moved people in and out, as well as around, the hemisphere, is it any wonder that
					North Americans began to think hemispherically? As early as 1932, Herbert E. Bolton, in
					his presidential address to the American Historical Association, argued that the
					“increasing importance of inter-American relations makes imperative a better understanding
					by each of the history and culture of all.” Bolton posited the common chronology in the
					Western Hemisphere of European discovery, exploration and colonization, succeeded by
					nations “striving on the one hand for national solidarity, political stability, and
					economic well-being, and on the other hand for a satisfactory adjustment of relations with
					each other and with the rest of the world” (Bolton, 1932, 448-9). While it took until 2005
					for Felipe Fernández Armesto to publish The Americas: A Hemispheric History, by 1952 at
					least one individual was hitting the road to test that theory—even if he was doing so
					unaware. Ernesto “Che” Guevara and Alberto Granados on “La Poderosa” were Argentine
					students motorcycling and then hitchhiking from Argentina to Venezuela, Miami, and back.
					It would be interesting to know if any of the maps that envisioned a Pan-American
					geography inspired the journey or were used in planning the trip. Certainly, the
					middle-class Guevara household would have had access to such materials.
				</p>
			</section>
		</article>
	</div>
</Essay>
