import json, os, re

outputArray = []

# txtfile = 'amercan_railroad_maps_1828_1876/1828_survey_boston.txt'
rootdir = './'
galleryStart = 0

for subdir, dirs, files in os.walk(rootdir):
    for txtfile in files:
        map = {
            "title": '',
            "parent": '',
            "path": '',
            "miniessay": [],
            "citations": []
        }
        # print(os.path.join(subdir, file))
        # print(txtfile)
        with open(os.path.join(subdir, txtfile)) as file:
            if "_MAIN_" in txtfile or '.txt' not in txtfile:
                break
            map["path"] = os.path.join(subdir, txtfile).replace("_","-").replace(".txt", "")
            for idx, line in enumerate(file):
                line = line.strip()
                if line == "Selection Gallery":
                    galleryStart = idx
                    # break
                if idx == 0: 
                    map["title"] = line
            # else if "Referenced by Essay" in line:
                elif idx == 2 :
                    map["parent"] = line
                elif galleryStart > 0 and idx > galleryStart:
                    if line.startswith("tittle="):
                        splitter = re.search(r'^tittle= (.+);;citte= (.+)$', line)
                        # print(splitter)
                        citation = {
                            "title": splitter.group(1),
                            "source": splitter.group(2)
                        }
                        map["citations"].append(citation)
                        
                elif len(line) > 0 and "Referenced by Essay" not in line and "Selection Gallery" not in line:
                    map["miniessay"].append([idx, line])
        outputArray.append(map)

with open('data_map.json', 'w') as f:
    json.dump(outputArray, f, indent=4)