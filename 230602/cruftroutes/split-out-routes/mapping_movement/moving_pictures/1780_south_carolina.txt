South Carolina and Part of Georgia, 1780
Referenced by Essay: 
Moving Pictures: Maps and Imagination in Eighteenth-Century Anglo-America
Even though De Brahm’s name remained on the map’s title, he likely had nothing to do with this updated version. De Brahm had spent the years 1765-1780 either in Florida conducting surveys of the peninsula’s eastern coast or in London answering charges of misconduct during his Florida tenure. The title names John Stuart, Superintendent for Indian Affairs, who was a likelier source for the updated information. As Superintendent, Stuart had a keen interest in surveys of the southern colonies, as he was charged with negotiating and surveying the 1763 Proclamation line between the numerous Indians of the Southeast and the southern colonies. Stuart thus worked closely with the surveyors of South Carolina and Georgia and likely used their records to update De Brahm’s earlier map.

Selection Gallery
South Carolina and Part of Georgia, 1780
Citation:
Faden, William, "A Map of South Carolina and a Part of Georgia" (London : William Faden, 1780). Oversize Ayer 133 .D28 1780