<script>
	import Essay from '$lib/essay.svelte';
	import Placeholder from '$lib/placeholder.svelte';
</script>

<Essay>
	<div slot="title">
		<h1>Mapping Migration and Settlement</h1>
	</div>
	<div slot="author">
		<p class="author">
			<em> by: </em>
			<span> RONALD GRIM </span>
		</p>
	</div>
	<div slot="body">
		<article>
			<section>
				<p class="body-text">
					Migration, which is the process of people moving from one place to another in order to
					establish a new residence, has been a reoccurring theme in US history. The nature of this
					migration experience has a variety of temporal and geographical components which can be
					studied with maps. Some of the more important questions to address geographically include
					identifying homelands migrants have left, locating the places where they settled, and
					determining where and how they moved after they arrived in the North America. The reasons
					for people relocating into and within the United States have been numerous, but in most
					cases cannot be easily determined from the cartographic evidence.
				</p>
				<p class="body-text">
					This essay and the accompanying captioned maps identify and illustrate a variety of maps
					that can be used effectively to document the spatial components of the US migration story.
					Very few maps show actual migration patterns; however, maps often show roads, railroads,
					or other transportation routes that migrants could have used. Maps also capture a moment
					in time, providing a cross section of a geographical space that depicts the migrants’
					homeland or place of origin, as well as their new home or place of eventual settlement.
					Some migrations are not well documented on contemporary maps, such as the African slave
					trade and the Underground Railroad; the Native Americans’ “trail of tears;” or the
					journeys of young women who moved to New England industrial towns from the nearby rural
					countryside. The selection of maps for this essay is not intended to be comprehensive;
					each immigrant group’s story is different. Rather the selection is based on my research
					which has focused on German-American and African-American experiences. For a general
					description of the geographical components of a wider variety of immigrant groups, please
					refer to the thematic maps and accompanying discussions found in We the People (Allen and
					Turner 1988).
				</p>
			</section>
			<Placeholder />
			<section>
				<h2 class="section-header">Migration Processes and Trends</h2>
				<p class="body-text">
					The United States is known as a nation of immigrants. One of the distinctive
					characteristics of its culture is the variety and vitality of its immigrant and ethnic
					composition. The nation was founded and grew as a result of migration, primarily from
					Europe, Africa, and East Asia during the eighteenth and nineteenth centuries, but also
					from other parts of the world in the twentieth century. Almost all Americans can claim to
					have some foreign ancestry, whether they are a first- or second-generation immigrant, or
					they can trace their ancestry to an earlier generation of immigrants.
				</p>
				<p class="body-text">
					Many immigrants came voluntarily or freely, making a conscious decision to migrate based
					on a variety of economic, political, or religious push-pull factors. The great majority of
					these sought better jobs and higher standards of living; others fled political oppression
					or situations of ethnic cleansing; while some desired to practice their religious beliefs
					more freely. The need to escape physical disasters, such as droughts or famines, motivated
					others to leave their homelands.
				</p>
				<p class="body-text">
					During the seventeenth and eighteenth centuries, most of the newly arriving colonists came
					from England, settling along the Atlantic seaboard and in the major port cities of Boston,
					New York, Philadelphia, and Charleston. While the British dominated the political and
					economic structure of the thirteen colonies, large numbers of Germans and Scotch Irish
					settled in southeastern Pennsylvania and the backcountry from Maryland south to the
					Carolinas (Lemon 1990).
				</p>
				<p class="body-text">
					However, the nineteenth and early twentieth centuries, particularly from about 1840 to
					1920, was the period of mass migration from Europe to the United States. During this time
					period, the nation’s population grew from 17 to 105 million, a sixfold increase. Although
					there was a high rate of natural increase with birth rates exceeding death rates, 33
					million immigrants came into the country during this time span. After 1924, when entry
					restrictions were instituted with the enactment of the National Origins Act, the number of
					new immigrants declined noticeably (Ward 1990).
				</p>
				<p class="body-text">
					From 1840 to 1880, the largest number of immigrants came from Northwest Europe,
					particularly Germany and Ireland, with smaller numbers from England, France, and
					Scandinavia. The Germans settled in both rural and urban settings primarily in the
					Mid-Atlantic and Midwestern states, while the Irish settled in urban and industrial
					communities primarily in the Northeastern states. Both avoided the South primarily because
					of their antipathy toward slavery and the lack of industrial jobs in the southern economy.
				</p>
				<p class="body-text">
					After 1880, the source regions changed drastically. Although the number of German, Irish,
					English and Scandinavian immigrants remained high, their numbers were surpassed by
					immigrants from Italy, Greece, and lands ruled by the Austrian-Hungarian and Russian
					empires. These immigrants tended to settle in the larger cities in the Northeastern,
					Mid-Atlantic, and Midwestern states.
				</p>
				<p class="body-text">
					Throughout the last two thirds of the nineteenth century, there was also a sizeable flow
					of immigrants from Asia, particularly from China, to the West Coast of the United States.
					Numbering about one million, the Asian immigrants worked primarily on railroad
					construction and in the mining communities of the West.
				</p>
				<p class="body-text">
					For others, however, their migration was involuntary or forced. The two primary examples
					of forced migration within the American experience are the Native and African Americans.
					As Europeans colonized eastern North America and their settlement frontier expanded across
					the Appalachians, Native Americans were pushed further west and confined to reservations,
					first in the Midwest, then the Great Plains and the West, through a succession of negative
					pressures including disease, battles, treaty negotiations, and land cessions. African
					Americans, on the other hand, were the victims of the seventeenth and eighteenth century
					slave trade. Many of the Africans who were sold into slavery came from the West Coast of
					Africa, where they were imported to West Indian sugar plantations, before re-export to the
					southeastern United States. Within the United States, the Africans’ movements were tied to
					the migrations of their owners until their eventual emancipation following the Civil War.
				</p>
				<p class="body-text">
					In addition to the migrations into the United States from Europe, Africa, and Asia, there
					was considerable migration within the United States. One major trend was the general
					movement of the settlement frontier westward across the United States. With the signing of
					the Paris Peace treaty in 1783, the new nation gained control of the lands east of the
					Mississippi River. Settlement moved rapidly from the eastern states across the Appalachian
					Mountains into the Old Northwest and the Southeast, as these public domain lands were
					systematically surveyed and subsequently sold as part of the General Land Office’s iconic
					township and range system (Grim 1990). This east to west migration tended to occur in
					broad latitudinal bands, with migrants from New England and the Mid-Atlantic states moving
					into the Upper Midwest, those from the Chesapeake region moving into the Lower Midwest and
					the Upper South, and those from the Carolinas and Georgia moving into the Lower Southeast.
				</p>
				<p class="body-text">
					With the purchase of the Louisiana Territory in 1803, the spirit of Manifest Destiny
					spurred the further acquisition and settlement of the lands west of the Mississippi. By
					adding Texas, the Mexican Southwest and the Pacific Northwest, the national territory
					extended as far as the Pacific Ocean by the middle of the nineteenth century. The
					discovery of gold in California and the construction of transcontinental railroads
					hastened migration to the West Coast, whether it was long-time residents of the older
					eastern states or newly arrived immigrants from Europe, China, or Mexico.
				</p>
				<p class="body-text">
					Industrialization in the early nineteenth century encouraged a rural to urban migration
					trend within the United States. With employment opportunities developing in the new
					industrial towns of New England, young women from rural communities along with French
					Canadians moved into such towns as Lowell and Lawrence. As industrialization spread
					through the Mid-Atlantic and Midwestern cities, rural to urban migration increased
					rapidly. Following the Civil War, many emancipated slaves remained in the South, but
					others started to move to northward to urban and industrial centers in the Northeast and
					Midwest, a trend that greatly accelerated during the first half of the twentieth century.
					From 1840 to 1920 the urban population of the United States increased from eleven to
					fifty-one percent of the total US population. This dramatic increase can be attributed to
					both foreign immigration and rural to urban migration.
				</p>
			</section>
			<Placeholder />
			<section>
				<h2 class="section-header">Personal Accounts of Migration</h2>
				<p class="body-text">
					Unfortunately, there are very few maps that were compiled or published during the
					eighteenth, nineteenth, and early twentieth centuries that document individual migration
					stories or the general geographical patterns of migration into and within the United
					States. What we know about the geographical components of this migration process, we have
					learned from millions of personal stories, which can be plotted on and documented with a
					wide variety of maps published during this period. Diaries, letters, and family
					genealogies provide the basis for reconstructing numerous personal migration routes, from
					which historians have been able to determine general patterns.
				</p>
				<p class="body-text">
					One example of a personal narrative that is particularly useful for identifying geographic
					locations and travel routes is an account prepared by John Parkinson, a young man from the
					lead mining region of southwestern Wisconsin. In this biographical account, which was
					published in a 1921 issue of the Wisconsin Magazine of History (Parkinson 1921), he
					focused on his relocation from Wisconsin to the California Gold Fields during the 1850s,
					but also described the geographic origins of his family before they settled in Wisconsin.
				</p>
				<p class="body-text">
					This young twenty-year-old did not prepare any maps of his journeys, but he did make
					specific mention in his account regarding the modes of transportation and general routes
					that he followed between destinations representing various legs of his three-year journey.
					He and three other men were outfitted with two wagons and eight oxen by Parkinson's
					father. This team left southwestern Wisconsin, May 3, 1852. They crossed the Mississippi
					River at Dubuque, and traversed Iowa via Cedar Rapids and Des Moines. After crossing the
					Missouri River, they travelled through Nebraska paralleling the north side of the Platte
					River to South Pass, where they selected the emigrant road to California. On this leg of
					the trek, they crossed the Green and Bear Rivers, passed north of Great Salt Lake,
					traversed the Great Basin and Sierra Nevada Range, and arrived at the Sacramento River,
					October 11, five months after they left home.
				</p>
				<p class="body-text">
					After three years of prospecting for gold at various mining camps in Northern California,
					Parkinson decided to return to Wisconsin to continue his college education. Rather than
					retrace the arduous overland journey, he decided to return by sea, crossing the continent
					at the Isthmus of Nicaragua. This return journey employed a number of different modes of
					transportation. It consisted of traveling by wagon to Shasta City, stagecoach and boat
					down the Sacramento River to San Francisco, steamer south along the Pacific Coast to
					Nicaragua, wagon and steam boat across the Isthmus and Lake Nicaragua, steamer from
					Greytown to New York City, train from New York to Chicago and then Freeport, and finally
					by stagecoach to home. Parkinson noted that the return trip took three weeks, but he also
					remarked that when he took a similar trip to California with his family fifty years later
					by train, it took only four days.
				</p>
				<p class="body-text">
					We also learn from Parkinson's account the basic components of his family's geographic
					origins and movements. His father's family was English, settling first in Virginia and
					then moving to Tennessee and eventually Illinois. His mother's family was Scotch-Irish
					Presbyterian, coming from Northern Ireland and settling initially in North Carolina. They
					subsequently moved to southern Illinois, where his parents met in 1817. His parents were
					encouraged by a relative to move to southwestern Wisconsin to take advantage of the mining
					opportunities and wheat farming. This short synopsis reinforces general migration trends
					that historians have noted during the eighteenth century when the primary immigrants were
					English, Scotch Irish, and German, and during the late eighteenth and early nineteenth
					century, when migration from the original thirteen colonies trended westward in broad
					latitudinal bands.
				</p>
				<p class="body-text">
					Using some of the maps described in this essay, as well as those described in other
					sections of the Mapping Movement portal, it would be possible to plot a fairly accurate
					geographical outline of Parkinson's three-year adventure in the Gold Fields, and to trace
					the movements related to his family’s genealogy. Some of these maps would include those
					showing the initial wagon roads from the Midwest to California (Caption 4); maps of the
					early road and railroad networks in the eastern part of the United States (Caption 3);
					statistical maps and atlases that depict general population and immigration trends
					(Caption 6); and landownership maps and atlases that record mid-nineteenth century
					settlement patterns in the Midwest (Caption 9).
				</p>
				<p class="body-text">
					While personal narratives have rarely been mapped collectively, one exception is the
					initial attempt during the 1890s, as well as subsequent twentieth century revisions, to
					reconstruct maps of the various routes associated with the African American slaves’ escape
					to freedom on the Underground Railroad. These maps were based on the accounts of
					abolitionists and escaping slaves (Caption 1).
				</p>
			</section>
			<Placeholder />
			<section>
				<h2 class="section-header">Maps of Migration</h2>
				<p class="body-text">
					If personal narratives of individual family migration histories, such as Parkinson's
					memoir, are not accompanied by illustrative maps, how can we map the migration process?
					Fortunately, the three fundamental geographical components of migration into and within
					the United States can be studied cartographically. These cartographic sources include maps
					of the migrant's homeland or place of origin, the general paths or specific routes that
					the migrants travelled, and maps of the places the migrants settled, whether these are
					thematic maps or large-scale maps of rural or urban settings showing the name of
					individual landowners. While these maps are not dynamic in allowing us to observe the
					entire process, they provide static snapshots of specific stages or steps within the
					process.
				</p>

				<h2 class="section-header">Homeland Maps</h2>
				<p class="body-text">
					For many Americans with European ancestry, locating the specific town or village from
					which their ancestors migrated is an important part of their genealogical research. Small-
					and moderate-scale maps showing major cities and towns, as well as the primary
					transportation networks, were published for most European countries during the eighteenth
					century. Maps of this nature are readily available in numerous American map libraries that
					have good historic collections of single sheets maps and world atlases. During the
					nineteenth century, larger scale topographic map series were published for many European
					countries, first for France, England, the Low Countries, and later for Scotland, Ireland,
					Switzerland, Germany, the Austrian-Hungarian Empire, Poland, and Russia (Larsgaard 1993).
					With the aid of contemporary gazetteers, geographic names databases (see Further Reading)
					and more specialized gazetteers, such as Where Once We Walked (Mokotoff and Sack 2002),
					which provides a listing of Jewish Shtetls in eastern and central Europe, researchers can
					locate most cities, towns, and small villages throughout Europe (USGS 1991).
				</p>
			</section>
			<Placeholder />
			<section>
				<h2 class="section-header">Transportation and Promotional Maps</h2>
				<p class="body-text">
					In determining how immigrants arrived at their new locations, researchers would like to
					know about their trans-Atlantic voyages, the port cities where they entered the country,
					and the routes they travelled throughout the country to reach their final destination.
					While only scattered maps showing specific voyages or the general course of trans-Atlantic
					routes are available for the late eighteenth and nineteenth centuries, there are numerous
					maps of the nation or individual states that show ports of entry and assist in tracing
					routes described in personal narratives. National or state maps published during the last
					half of the eighteenth century generally show only the major roads, but similar maps
					published after the introduction of internal improvements during the first half of the
					nineteenth century distinguish between turnpikes, post roads, canals and railroads. And in
					many cases there are also maps of individual canal or rail routes as they were planned,
					constructed, and promoted, as discussed in other sections of this portal (Grim 2000).
				</p>
				<p class="body-text">
					One specific type of map, particularly those published to accompany or illustrate
					promotional travel literature, selected or reinterpreted more detailed reference and
					transportation maps to address specific immigrant populations. For example, the guidebooks
					intended to promote German immigration were written in German, and the maps were
					translated into German. In some cases, the selection of towns and communities displayed on
					the map reflect a German bias. The geographical coverage of these guidebooks and maps
					varied. Some, such as Traugott Bromme's advice book for German emigrants, provided a
					general discussion of emigration from Germany to various locations around the world,
					although there is a decided emphasis on various regions within North America (Caption 2).
					Besides including small general maps pairing most of the states, he included maps of the
					major port cities on the northeast coast, showing immigrants where they would most likely
					disembark. Francis Grund's handbook (Caption 3) is more focused geographically, promoting
					migration to the Midwestern states, while Joseph Ware's emigrant guide to California
					(Caption 4), highlights and maps a single route of migration, most likely the one followed
					by John Parkinson, whose personal memoir is illustrated above.
				</p>
			</section>
			<Placeholder />
			<section>
				<h2 class="section-header">Thematic and Statistical Maps</h2>
				<p class="body-text">
					Thematic maps, based on census data and immigration statistics, are the best graphics for
					visualizing where immigrants were born and where they settled. Unfortunately, such maps
					began to be published in the United States only during the middle of the nineteenth
					century, following the US censuses of 1860 and 1870. Although federal population censuses
					were taken every ten years beginning in 1790, place of birth was not recorded until 1850.
					Demographic data, such as this, was not mapped until 1860, when the slave population in
					the southern states was the first demographic variable to be mapped (Schulten 2011;
					Caption 5). Starting with the 1870, 1880, and 1890 censuses, the US Census Bureau, along
					with a commercial publisher, issued three statistical atlases that employed a number of
					creative graphic devices to display information about the nation’s growing immigrant
					population (Dahmann; Caption 6).
				</p>
				<p class="body-text">
					The Census Bureau continued to publish statistical atlases for the 1900, 1910, and 1920
					censuses. However, the maps were less attractive and less creative, and the Bureau failed
					to produce similar publications for succeeding censuses. However, this void in providing
					cartographic and visual interpretation of census data was taken up by academic and
					professional cartographers, particularly as they prepared national and state atlases
					during the late twentieth century. For example, two geography professors at California
					State University, Northridge, published We the People: An Atlas of Ethnic Diversity (Allen
					and Turner 1988) which was based on data collected during the 1980 census from a
					controlled sample asking respondents what they considered their primary national ancestry.
					The atlas contains numerous thematic maps, showing ancestry for sixty-seven national or
					ethnic groups. Another example is the Historical Atlas of Massachusetts (Halpern and
					Wilkie 1991). The chapter on immigration includes innovative cartograms showing the size
					of ethnic population in counties and towns throughout Massachusetts.
				</p>
				<p class="body-text">
					While most cartographic representations of immigration statistics have been prepared as
					national or state maps, there are a few examples of studies that focused on individual
					cities, or portions of cities. One example is a survey of one of the poorest section of
					Chicago, based on a study conducted by residents of Jane Addams Hull House in the early
					1890s (Caption 7). These colorful maps depict the percentage of each immigrant group
					living in individual structures within the study area.
				</p>
			</section>
			<Placeholder />
			<section>
				<h2 class="section-header">Large-Scale Settlement Maps</h2>
				<p class="body-text">
					Thematic maps are very useful for identifying the heaviest densities or largest
					concentrations of individual immigrant groups within the nation or in an individual state.
					However, there are other moderate- to large-scale maps that help enhance our understanding
					of general migration trends or immigrant settlement patterns.
				</p>
				<p class="body-text">
					For example, state sectional maps, such as the one used to illustrate migration into
					Kansas during the 1850s (Caption 8), illustrate several trends. State sectional maps,
					which were prepared for most Midwestern and Great Plains states, show the progress of
					General Land Office surveys for the year in which they were published. Thus, by viewing a
					chronological succession of these maps for an individual state such as Kansas, it is
					possible to demonstrate how rapidly the settlement frontier was moving through the state,
					generally in a westerly direction. In addition the sample map of Kansas also identifies
					the emigrant roads to California and Oregon providing further evidence of this westward
					migration trend. On the other hand, the distinct delineation on this map of the lands
					reserved for Native Americans illustrates their removal from eastern tribal lands to the
					Great Plains and eventually to the Indian Territory. More subtle clues, such as marginal
					illustrations and the location of specific events, provide evidence of the conflicts
					between the pro- and antislavery groups settling in Kansas after the Kansas-Nebraska Act
					of 1850.
				</p>
				<p class="body-text">
					Large-scale landownership maps are useful for examining immigrant settlement patterns in
					rural areas. Such maps, showing the names of individual landowners, were first published
					during the 1790s for a number of New England towns. Similar maps were published at the
					county level during the first decades of the nineteenth century, and became very prevalent
					for most counties in the New England and Mid-Atlantic states by the 1850s. Published as
					large wall maps, they displayed the names of individual property owners, and located
					churches, schools, public buildings, and factories. They were often illustrated with
					vignettes of the homes or businesses of the map's patrons (Stephenson 1967; Conzen 1990).
					Following the Civil War, land ownership maps were published in an atlas format for many
					counties in the Midwestern and Great Plain states (LeGear 1950). These atlas maps also
					showed the names of individual landowners, and located churches, schools, and factories.
					The atlases often included a picture section and business directory, helping to identify
					the wealthier and more prominent citizens of the county. By locating the churches, which
					were often named according to their immigrant affiliation and by comparing family surnames
					with manuscript census schedules, it is possible to identify clusters of immigrant
					settlers, as demonstrated for the German settlement in and near Guttenberg, Iowa (Caption
					9).
				</p>
				<p class="body-text">
					The visual identification of such population clusters on cartographic sources can be
					confirmed by an analysis of the associated manuscript census schedules from the United
					States decennial population censuses. Beginning in 1850, these schedules listed the place
					of birth for each individual and for both parents beginning in 1880. The manuscript census
					schedules are maintained by the US National Archives and Records Administration as part of
					the records of the Bureau of the Census (Record Group 29:
					http://www.archives.gov/research/guide-fed-records/groups/029.html). Microfilm copies of
					these census schedules are available at NARA regional facilities and at many libraries
					throughout the country. These schedules can also be accessed online through subscription
					data bases such as Heritage Quest Online
					(http://www.heritagequestonline.com/hqoweb/library/do/login) or free data bases such as
					Family Search (https://familysearch.org/). For example, the manuscript census schedules
					for Guttenberg, Iowa, are reproduced in NARA microfilm publication T9, roll 0333 (1880,
					Iowa, Clayton County, Guttenberg, ED 137, sheets 407-418). For this research, the
					Guttenberg schedules were accessed through Family Search.
				</p>
				<p class="body-text">
					Fire insurance and real estate atlases provide a similar resource for identifying
					immigrant neighborhoods in urban settings. First published for major cities, such as New
					York City, Chicago, and Boston in the 1860s, these urban atlases became standard
					publications for most major cities and towns during the second half of the nineteenth
					century, and continued to be updated and revised until the middle of the twentieth
					century. Fire insurance atlases, which were prepared to assist insurance underwriters in
					determining risk assessment, showed the footprint of individual buildings which were color
					coded to reflect their construction material (yellow for wood or frame structures, pink
					for brick, and blue for stone). Churches, schools, major public buildings, and factories
					were labeled (LOC 1981; Karrow and Grim 1990). Real estate atlases served a similar
					function for real estate agents and included similar information, although most included
					the names of residential property owners. Again, by locating churches and other social
					organizations that are associated with specific immigrant groups and by comparing the
					family surnames on the map sheets with those listed in contemporary city directories or
					manuscript census schedules, it is possible to identify immigrant neighborhoods, as
					demonstrated for a major German neighborhood in Chicago using an 1886 real estate atlas
					(Caption 10). It is also interesting to note that maps from a fire insurance atlas were
					used as base maps to plot the data collected for the Hull House maps (Caption 7).
				</p>
			</section>
		</article>
	</div>
</Essay>
