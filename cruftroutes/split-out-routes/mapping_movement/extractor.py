import json, os

outputArray = []

# txtfile = 'amercan_railroad_maps_1828_1876/1828_survey_boston.txt'
rootdir = './'

for subdir, dirs, files in os.walk(rootdir):
    for txtfile in files:
        map = {
            "title": '',
            "parent": '',
            "path": '',
            "miniessay": []
        }
        # print(os.path.join(subdir, file))
        print(txtfile)
        with open(os.path.join(subdir, txtfile)) as file:
            if "_MAIN_" in txtfile or 'xlsx' in txtfile:
                break
            map["path"]  = os.path.join(subdir, txtfile).replace(".txt", "").replace("_","-")[1:]
            for idx, line in enumerate(file):
                line = line.strip()
                if line == "Selection Gallery":
                    break
                if idx == 0: 
                    map["title"] = line
            # else if "Referenced by Essay" in line:
                elif idx == 2 :
                    map["parent"] = line
                elif len(line) > 0 and "Referenced by Essay" not in line:
                    map["miniessay"].append(line)
        outputArray.append(map)

with open('test2.json', 'w') as f:
    json.dump(outputArray, f, indent=4)