mkdir src/routes/mapping-migration
mkdir src/routes/state-and-federal
mkdir src/routes/waterways-cartography-part-2
mkdir src/routes/european-maps-for-exploration
mkdir src/routes/american-railroad-maps-1828-1876
mkdir src/routes/mapping-communication
mkdir src/routes/around-over-through
mkdir src/routes/navigating-the-coasts-and-seas
mkdir src/routes/waterways-cartography-part-1
mkdir src/routes/maps-movement-lit
mkdir src/routes/maps-of-trails-and-roads
mkdir src/routes/american-railroad-maps-1873-2012
mkdir src/routes/moving-pictures
mkdir src/routes/lines-that-fracture
mkdir src/routes/planning-transportation
mkdir src/routes/planning-transportation/1898-property-chicago
mkdir src/routes/planning-transportation/1895-road-map-ny
mkdir src/routes/planning-transportation/1911-electric-la
mkdir src/routes/planning-transportation/1969-plan-nyc
mkdir src/routes/planning-transportation/1937-new-england-regional
mkdir src/routes/planning-transportation/1876-railway-philadelphia
mkdir src/routes/planning-transportation/1951-valparaiso
mkdir src/routes/planning-transportation/1836-north-river
mkdir src/routes/planning-transportation/1961-new-orleans
mkdir src/routes/planning-transportation/1946-preliminary-chicago
mkdir src/routes/moving-pictures/1755-british-colonies
mkdir src/routes/moving-pictures/1757-south-carolina
mkdir src/routes/moving-pictures/1730-charecke-nation
mkdir src/routes/moving-pictures/1672-virginia
mkdir src/routes/moving-pictures/1720-territorial-claims
mkdir src/routes/moving-pictures/1755-virginia
mkdir src/routes/moving-pictures/1780-south-carolina
mkdir src/routes/moving-pictures/1777-charles-town
mkdir src/routes/moving-pictures/1775-carolinas
mkdir src/routes/moving-pictures/1775-war-new-england
mkdir src/routes/maps-of-trails-and-roads-of-the-great-west/1857-overland-mail-route-to-california
mkdir src/routes/maps-of-trails-and-roads-of-the-great-west/1859-route-from-kansas-city
mkdir src/routes/maps-of-trails-and-roads-of-the-great-west/1875-best-and-shortest-cattle
mkdir src/routes/maps-of-trails-and-roads-of-the-great-west/1870-territory-of-montana
mkdir src/routes/maps-of-trails-and-roads-of-the-great-west/1846-new-map-of-texas
mkdir src/routes/maps-of-trails-and-roads-of-the-great-west/1898-gold-and-coal-fields
mkdir src/routes/maps-of-trails-and-roads-of-the-great-west/1811-road-from-capital-of-new-spain
mkdir src/routes/maps-of-trails-and-roads-of-the-great-west/1779-map-of-the-province-of-new-mexico
mkdir src/routes/maps-of-trails-and-roads-of-the-great-west/1863-military-road-from-fort-walla
mkdir src/routes/maps-of-trails-and-roads-of-the-great-west/1830-map-of-texas-with-parts
mkdir src/routes/mapping-communication/1773-colonial-post-roads
mkdir src/routes/mapping-communication/1899-new-economy
mkdir src/routes/mapping-communication/1889-standard-time
mkdir src/routes/mapping-communication/1858-wire-across-ocean
mkdir src/routes/mapping-communication/1932-rivers-roads-rates
mkdir src/routes/mapping-communication/1866-wire-across-continent
mkdir src/routes/mapping-communication/1768-chart-gulf-stream
mkdir src/routes/mapping-communication/1849-routes-to-california
mkdir src/routes/mapping-communication/1905-independent-telephone
mkdir src/routes/mapping-communication/1926-radio-stations
mkdir src/routes/around-over-through/1909-route-bogota
mkdir src/routes/around-over-through/1726-louisiana-mexico
mkdir src/routes/around-over-through/1933-ye-olde-spanish
mkdir src/routes/around-over-through/1848-military-leavenworth
mkdir src/routes/around-over-through/1908-united-brazil
mkdir src/routes/around-over-through/1855-panama-rr
mkdir src/routes/around-over-through/1929-airmail-argentina
mkdir src/routes/around-over-through/1840-atlas-venezuela
mkdir src/routes/around-over-through/1938-motorists-mexico
mkdir src/routes/around-over-through/1950-panorama-american
mkdir src/routes/lines-that-fracture/1930-motor-routes-to-augusta
mkdir src/routes/lines-that-fracture/1863-usarmy-map-of-northwest-georgia
mkdir src/routes/lines-that-fracture/1822-state-of-georgia
mkdir src/routes/lines-that-fracture/1835-north-carolina-south
mkdir src/routes/lines-that-fracture/1919-national-highways-proposed-in-georgia
mkdir src/routes/lines-that-fracture/1869-georgia-central-railroad
mkdir src/routes/lines-that-fracture/1968-principal-us-electric
mkdir src/routes/lines-that-fracture/1976-mapping-the-travels-of-john
mkdir src/routes/lines-that-fracture/1775-southern-indian-district-of-north-america
mkdir src/routes/lines-that-fracture/1818-map-of-the-state-of-georgia
mkdir src/routes/american-railroad-maps-1873-2012/1886-canadian-pacific
mkdir src/routes/american-railroad-maps-1873-2012/1916-georgia-railroad
mkdir src/routes/american-railroad-maps-1873-2012/1890-lake-superior
mkdir src/routes/american-railroad-maps-1873-2012/1919-railroad-valuation-map-itasca
mkdir src/routes/american-railroad-maps-1873-2012/1909-rand-mcnally-business-atlas-florida
mkdir src/routes/american-railroad-maps-1873-2012/1971-amtrak-passenger
mkdir src/routes/american-railroad-maps-1873-2012/1870-white-mountains
mkdir src/routes/american-railroad-maps-1873-2012/1945-railroad-operations-map-industrial
mkdir src/routes/american-railroad-maps-1873-2012/1925-atlas-of-traffic-maps
mkdir src/routes/american-railroad-maps-1873-2012/1920-milwaukee-road
mkdir src/routes/american-railroad-maps-1873-2012/1916-georgia-railroad.
mkdir src/routes/amercan-railroad-maps-1828-1876/1836-vision-us-rr
mkdir src/routes/amercan-railroad-maps-1828-1876/1854-rr-us
mkdir src/routes/amercan-railroad-maps-1828-1876/1828-survey-boston
mkdir src/routes/amercan-railroad-maps-1828-1876/1847-boston-worcester
mkdir src/routes/amercan-railroad-maps-1828-1876/1854-northern-rr
mkdir src/routes/amercan-railroad-maps-1828-1876/1872-track-book
mkdir src/routes/amercan-railroad-maps-1828-1876/1873-atsf-kansas
mkdir src/routes/amercan-railroad-maps-1828-1876/1854-illinois-land-grant
mkdir src/routes/amercan-railroad-maps-1828-1876/1867-penn-rr
mkdir src/routes/amercan-railroad-maps-1828-1876/1853-pacific-rr
mkdir src/routes/mapping-migration/1843-northwestern-states
mkdir src/routes/mapping-migration/1898-underground-railroad-routes
mkdir src/routes/mapping-migration/1863-slave-populations
mkdir src/routes/mapping-migration/1856-eastern-kansas
mkdir src/routes/mapping-migration/1846-german-immigration
mkdir src/routes/mapping-migration/1886-plat-book-clayton
mkdir src/routes/mapping-migration/1849-route-california
mkdir src/routes/mapping-migration/1886-fire-insurance-chicago
mkdir src/routes/mapping-migration/1895-nationalities-chicago
mkdir src/routes/mapping-migration/1898-statistical-atlas-us
mkdir src/routes/maps-movement-lit/1755-british-french
mkdir src/routes/maps-movement-lit/1836-michigan-ouisconsin
mkdir src/routes/maps-movement-lit/1862-mississippi
mkdir src/routes/maps-movement-lit/1784-usa
mkdir src/routes/maps-movement-lit/1884-zigzag
mkdir src/routes/maps-movement-lit/1657-island-barbados
mkdir src/routes/maps-movement-lit/1616-new-england
mkdir src/routes/maps-movement-lit/1955-road-map-southwestern
mkdir src/routes/maps-movement-lit/1494-christopher-columbus
mkdir src/routes/maps-movement-lit/1906-hollow-earth
mkdir src/routes/state-and-federal/1905-panama-canal
mkdir src/routes/state-and-federal/1846-fremont-oregon
mkdir src/routes/state-and-federal/1821-canal-lake-erie
mkdir src/routes/state-and-federal/1808-rapids-ohio
mkdir src/routes/state-and-federal/1945-ancient-courses
mkdir src/routes/state-and-federal/1866-union-pacific-rr
mkdir src/routes/state-and-federal/1858-chicago-harbor
mkdir src/routes/state-and-federal/1841-us-expeditionary-oregon
mkdir src/routes/state-and-federal/1803-lewis-clark
mkdir src/routes/state-and-federal/1944-silk-chart
mkdir src/routes/waterways-cartography-part-1/1829-canals-and-railroads
mkdir src/routes/waterways-cartography-part-1/1852-proposed-dams-and-jetties
mkdir src/routes/waterways-cartography-part-1/1869-proposed-canal-inland
mkdir src/routes/waterways-cartography-part-1/1820-plan-and-profile-erie-canal
mkdir src/routes/waterways-cartography-part-1/1862-canada-lakes-and-canals
mkdir src/routes/waterways-cartography-part-1/1817-navigation-guide-for-inland-rivers
mkdir src/routes/waterways-cartography-part-1/1878-longitudinal-sections
mkdir src/routes/waterways-cartography-part-1/1848-travel-guide-hudson
mkdir src/routes/waterways-cartography-part-1/1808-proposed-canal-new-york
mkdir src/routes/waterways-cartography-part-1/1862-canal-profiles-new-york
mkdir src/routes/navigating-the-coasts/1859-frontiers-of-ocean
mkdir src/routes/navigating-the-coasts/1891-algae-concentrations
mkdir src/routes/navigating-the-coasts/1833-stage-and-steamboat-routes-ohio
mkdir src/routes/navigating-the-coasts/1732-variations-of-the-compass
mkdir src/routes/navigating-the-coasts/1732-coast-pilot-chart
mkdir src/routes/navigating-the-coasts/1838-us-coast-survey-chart-new-haven
mkdir src/routes/navigating-the-coasts/1934-us-coast-survey-chart-hawaii
mkdir src/routes/navigating-the-coasts/1820-ships-and-navigation
mkdir src/routes/navigating-the-coasts/1807-trade-winds
mkdir src/routes/navigating-the-coasts/1877-austro-hungarian
mkdir src/routes/european-maps-for-exploration/1612-virginia
mkdir src/routes/european-maps-for-exploration/1541-nautical-chart-of-the-pacific-coast-of-mexico
mkdir src/routes/european-maps-for-exploration/1703-new-voyages-in-northern-america
mkdir src/routes/european-maps-for-exploration/1801-mackenzies-northern-voyages
mkdir src/routes/european-maps-for-exploration/1688-globe-gore-of-eastern-north-america
mkdir src/routes/european-maps-for-exploration/1598-florida-and-the-apalachee-lands
mkdir src/routes/european-maps-for-exploration/1672-jesuit-map-of-lake-superior
mkdir src/routes/european-maps-for-exploration/1761-russian-voyages-in-the-pacific-northwest
mkdir src/routes/european-maps-for-exploration/1612-new-france
mkdir src/routes/european-maps-for-exploration/1784-map-of-kentucky
mkdir src/routes/waterways-cartography-part-2/1964-commodities
mkdir src/routes/waterways-cartography-part-2/1894-water-transportation-routes-us
mkdir src/routes/waterways-cartography-part-2/1908-freight-rates-inland
mkdir src/routes/waterways-cartography-part-2/1931-buffalo-great-lakes
mkdir src/routes/waterways-cartography-part-2/1949-intracoastal-waterway-nj
mkdir src/routes/waterways-cartography-part-2/1970-nautical-charts-illinois
mkdir src/routes/waterways-cartography-part-2/1904-canals-ohio
mkdir src/routes/waterways-cartography-part-2/1936-us-lake-survey
mkdir src/routes/waterways-cartography-part-2/1882-inland-canals
mkdir src/routes/waterways-cartography-part-2/1898-water-routes-yukon
cp -vi time-to-make-a-mess/mains/_MAIN_mapping_migration.txt src/routes/mapping-migration/+page.svelte
cp -vi time-to-make-a-mess/mains/_MAIN_state_and_federal.txt src/routes/state-and-federal/+page.svelte
cp -vi time-to-make-a-mess/mains/_MAIN_waterways_cartography_part_2.txt src/routes/waterways-cartography-part-2/+page.svelte
cp -vi time-to-make-a-mess/mains/_MAIN_european_maps_for_exploration.txt src/routes/european-maps-for-exploration/+page.svelte
cp -vi time-to-make-a-mess/mains/_MAIN_american_railroad_maps_1828_1876.txt src/routes/american-railroad-maps-1828-1876/+page.svelte
cp -vi time-to-make-a-mess/mains/_MAIN_mapping_communication.txt src/routes/mapping-communication/+page.svelte
cp -vi time-to-make-a-mess/mains/_MAIN_around_over_through.txt src/routes/around-over-through/+page.svelte
cp -vi time-to-make-a-mess/mains/_MAIN_navigating_the_coasts_and_seas.txt src/routes/navigating-the-coasts-and-seas/+page.svelte
cp -vi time-to-make-a-mess/mains/_MAIN_waterways_cartography_part_1.txt src/routes/waterways-cartography-part-1/+page.svelte
cp -vi time-to-make-a-mess/mains/_MAIN_maps_movement_lit.txt src/routes/maps-movement-lit/+page.svelte
cp -vi time-to-make-a-mess/mains/_MAIN_maps_of_trails_and_roads.txt src/routes/maps-of-trails-and-roads/+page.svelte
cp -vi time-to-make-a-mess/mains/_MAIN_american_railroad_maps_1873_2012.txt src/routes/american-railroad-maps-1873-2012/+page.svelte
cp -vi time-to-make-a-mess/mains/_MAIN_moving_pictures.txt src/routes/moving-pictures/+page.svelte
cp -vi time-to-make-a-mess/mains/_MAIN_lines_that_fracture.txt src/routes/lines-that-fracture/+page.svelte
cp -vi time-to-make-a-mess/mains/_MAIN_planning_transportation.txt src/routes/planning-transportation/+page.svelte
cp -vi time-to-make-a-mess/content/planning_transportation/1898_property_chicago.txt src/routes/planning-transportation/1898-property-chicago/+page.svelte
cp -vi time-to-make-a-mess/content/planning_transportation/1895_road_map_ny.txt src/routes/planning-transportation/1895-road-map-ny/+page.svelte
cp -vi time-to-make-a-mess/content/planning_transportation/1911_electric_la.txt src/routes/planning-transportation/1911-electric-la/+page.svelte
cp -vi time-to-make-a-mess/content/planning_transportation/1969_plan_nyc.txt src/routes/planning-transportation/1969-plan-nyc/+page.svelte
cp -vi time-to-make-a-mess/content/planning_transportation/1937_new_england_regional.txt src/routes/planning-transportation/1937-new-england-regional/+page.svelte
cp -vi time-to-make-a-mess/content/planning_transportation/1876_railway_philadelphia.txt src/routes/planning-transportation/1876-railway-philadelphia/+page.svelte
cp -vi time-to-make-a-mess/content/planning_transportation/1951_valparaiso.txt src/routes/planning-transportation/1951-valparaiso/+page.svelte
cp -vi time-to-make-a-mess/content/planning_transportation/1836_north_river.txt src/routes/planning-transportation/1836-north-river/+page.svelte
cp -vi time-to-make-a-mess/content/planning_transportation/1961_new_orleans.txt src/routes/planning-transportation/1961-new-orleans/+page.svelte
cp -vi time-to-make-a-mess/content/planning_transportation/1946_preliminary_chicago.txt src/routes/planning-transportation/1946-preliminary-chicago/+page.svelte
cp -vi time-to-make-a-mess/content/moving_pictures/1755_british_colonies.txt src/routes/moving-pictures/1755-british-colonies/+page.svelte
cp -vi time-to-make-a-mess/content/moving_pictures/1757_south_carolina.txt src/routes/moving-pictures/1757-south-carolina/+page.svelte
cp -vi time-to-make-a-mess/content/moving_pictures/1730_charecke_nation.txt src/routes/moving-pictures/1730-charecke-nation/+page.svelte
cp -vi time-to-make-a-mess/content/moving_pictures/1672_virginia.txt src/routes/moving-pictures/1672-virginia/+page.svelte
cp -vi time-to-make-a-mess/content/moving_pictures/1720_territorial_claims.txt src/routes/moving-pictures/1720-territorial-claims/+page.svelte
cp -vi time-to-make-a-mess/content/moving_pictures/1755_virginia.txt src/routes/moving-pictures/1755-virginia/+page.svelte
cp -vi time-to-make-a-mess/content/moving_pictures/1780_south_carolina.txt src/routes/moving-pictures/1780-south-carolina/+page.svelte
cp -vi time-to-make-a-mess/content/moving_pictures/1777_charles_town.txt src/routes/moving-pictures/1777-charles-town/+page.svelte
cp -vi time-to-make-a-mess/content/moving_pictures/1775_carolinas.txt src/routes/moving-pictures/1775-carolinas/+page.svelte
cp -vi time-to-make-a-mess/content/moving_pictures/1775_war_new_england.txt src/routes/moving-pictures/1775-war-new-england/+page.svelte
cp -vi time-to-make-a-mess/content/maps_of_trails_and_roads_of_the_great_west/1857_overland_mail_route_to_california.txt src/routes/maps-of-trails-and-roads-of-the-great-west/1857-overland-mail-route-to-california/+page.svelte
cp -vi time-to-make-a-mess/content/maps_of_trails_and_roads_of_the_great_west/1859_route_from_kansas_city.txt src/routes/maps-of-trails-and-roads-of-the-great-west/1859-route-from-kansas-city/+page.svelte
cp -vi time-to-make-a-mess/content/maps_of_trails_and_roads_of_the_great_west/1875_best_and_shortest_cattle.txt src/routes/maps-of-trails-and-roads-of-the-great-west/1875-best-and-shortest-cattle/+page.svelte
cp -vi time-to-make-a-mess/content/maps_of_trails_and_roads_of_the_great_west/1870_territory_of_montana.txt src/routes/maps-of-trails-and-roads-of-the-great-west/1870-territory-of-montana/+page.svelte
cp -vi time-to-make-a-mess/content/maps_of_trails_and_roads_of_the_great_west/1846_new_map_of_texas.txt src/routes/maps-of-trails-and-roads-of-the-great-west/1846-new-map-of-texas/+page.svelte
cp -vi time-to-make-a-mess/content/maps_of_trails_and_roads_of_the_great_west/1898_gold_and_coal_fields.txt src/routes/maps-of-trails-and-roads-of-the-great-west/1898-gold-and-coal-fields/+page.svelte
cp -vi time-to-make-a-mess/content/maps_of_trails_and_roads_of_the_great_west/1811_road_from_capital_of_new_spain.txt src/routes/maps-of-trails-and-roads-of-the-great-west/1811-road-from-capital-of-new-spain/+page.svelte
cp -vi time-to-make-a-mess/content/maps_of_trails_and_roads_of_the_great_west/1779_map_of_the_province_of_new_mexico.txt src/routes/maps-of-trails-and-roads-of-the-great-west/1779-map-of-the-province-of-new-mexico/+page.svelte
cp -vi time-to-make-a-mess/content/maps_of_trails_and_roads_of_the_great_west/1863_military_road_from_fort_walla.txt src/routes/maps-of-trails-and-roads-of-the-great-west/1863-military-road-from-fort-walla/+page.svelte
cp -vi time-to-make-a-mess/content/maps_of_trails_and_roads_of_the_great_west/1830_map_of_texas_with_parts.txt src/routes/maps-of-trails-and-roads-of-the-great-west/1830-map-of-texas-with-parts/+page.svelte
cp -vi time-to-make-a-mess/content/mapping_communication/1773_colonial_post_roads.txt src/routes/mapping-communication/1773-colonial-post-roads/+page.svelte
cp -vi time-to-make-a-mess/content/mapping_communication/1899_new_economy.txt src/routes/mapping-communication/1899-new-economy/+page.svelte
cp -vi time-to-make-a-mess/content/mapping_communication/1889_standard_time.txt src/routes/mapping-communication/1889-standard-time/+page.svelte
cp -vi time-to-make-a-mess/content/mapping_communication/1858_wire_across_ocean.txt src/routes/mapping-communication/1858-wire-across-ocean/+page.svelte
cp -vi time-to-make-a-mess/content/mapping_communication/1932_rivers_roads_rates.txt src/routes/mapping-communication/1932-rivers-roads-rates/+page.svelte
cp -vi time-to-make-a-mess/content/mapping_communication/1866_wire_across_continent.txt src/routes/mapping-communication/1866-wire-across-continent/+page.svelte
cp -vi time-to-make-a-mess/content/mapping_communication/1768_chart_gulf_stream.txt src/routes/mapping-communication/1768-chart-gulf-stream/+page.svelte
cp -vi time-to-make-a-mess/content/mapping_communication/1849_routes_to_california.txt src/routes/mapping-communication/1849-routes-to-california/+page.svelte
cp -vi time-to-make-a-mess/content/mapping_communication/1905_independent_telephone.txt src/routes/mapping-communication/1905-independent-telephone/+page.svelte
cp -vi time-to-make-a-mess/content/mapping_communication/1926_radio_stations.txt src/routes/mapping-communication/1926-radio-stations/+page.svelte
cp -vi time-to-make-a-mess/content/around_over_through/1909_route_bogota.txt src/routes/around-over-through/1909-route-bogota/+page.svelte
cp -vi time-to-make-a-mess/content/around_over_through/1726_louisiana_mexico.txt src/routes/around-over-through/1726-louisiana-mexico/+page.svelte
cp -vi time-to-make-a-mess/content/around_over_through/1933_ye_olde_spanish.txt src/routes/around-over-through/1933-ye-olde-spanish/+page.svelte
cp -vi time-to-make-a-mess/content/around_over_through/1848_military_leavenworth.txt src/routes/around-over-through/1848-military-leavenworth/+page.svelte
cp -vi time-to-make-a-mess/content/around_over_through/1908_united_brazil.txt src/routes/around-over-through/1908-united-brazil/+page.svelte
cp -vi time-to-make-a-mess/content/around_over_through/1855_panama_rr.txt src/routes/around-over-through/1855-panama-rr/+page.svelte
cp -vi time-to-make-a-mess/content/around_over_through/1929_airmail_argentina.txt src/routes/around-over-through/1929-airmail-argentina/+page.svelte
cp -vi time-to-make-a-mess/content/around_over_through/1840_atlas_venezuela.txt src/routes/around-over-through/1840-atlas-venezuela/+page.svelte
cp -vi time-to-make-a-mess/content/around_over_through/1938_motorists_mexico.txt src/routes/around-over-through/1938-motorists-mexico/+page.svelte
cp -vi time-to-make-a-mess/content/around_over_through/1950_panorama_american.txt src/routes/around-over-through/1950-panorama-american/+page.svelte
cp -vi time-to-make-a-mess/content/lines_that_fracture/1930_motor_routes_to_augusta.txt src/routes/lines-that-fracture/1930-motor-routes-to-augusta/+page.svelte
cp -vi time-to-make-a-mess/content/lines_that_fracture/1863_usarmy_map_of_northwest_georgia.txt src/routes/lines-that-fracture/1863-usarmy-map-of-northwest-georgia/+page.svelte
cp -vi time-to-make-a-mess/content/lines_that_fracture/1822_state_of_georgia.txt src/routes/lines-that-fracture/1822-state-of-georgia/+page.svelte
cp -vi time-to-make-a-mess/content/lines_that_fracture/1835_north_carolina_south.txt src/routes/lines-that-fracture/1835-north-carolina-south/+page.svelte
cp -vi time-to-make-a-mess/content/lines_that_fracture/1919_national_highways_proposed_in_georgia.txt src/routes/lines-that-fracture/1919-national-highways-proposed-in-georgia/+page.svelte
cp -vi time-to-make-a-mess/content/lines_that_fracture/1869_georgia_central_railroad.txt src/routes/lines-that-fracture/1869-georgia-central-railroad/+page.svelte
cp -vi time-to-make-a-mess/content/lines_that_fracture/1968_principal_us_electric.txt src/routes/lines-that-fracture/1968-principal-us-electric/+page.svelte
cp -vi time-to-make-a-mess/content/lines_that_fracture/1976_mapping_the_travels_of_john.txt src/routes/lines-that-fracture/1976-mapping-the-travels-of-john/+page.svelte
cp -vi time-to-make-a-mess/content/lines_that_fracture/1775_southern_indian_district_of_north_america.txt src/routes/lines-that-fracture/1775-southern-indian-district-of-north-america/+page.svelte
cp -vi time-to-make-a-mess/content/lines_that_fracture/1818_map_of_the_state_of_georgia.txt src/routes/lines-that-fracture/1818-map-of-the-state-of-georgia/+page.svelte
cp -vi time-to-make-a-mess/content/american_railroad_maps_1873_2012/1886_canadian_pacific.txt src/routes/american-railroad-maps-1873-2012/1886-canadian-pacific/+page.svelte
cp -vi time-to-make-a-mess/content/american_railroad_maps_1873_2012/1916_georgia_railroad.txt src/routes/american-railroad-maps-1873-2012/1916-georgia-railroad/+page.svelte
cp -vi time-to-make-a-mess/content/american_railroad_maps_1873_2012/1890_lake_superior.txt src/routes/american-railroad-maps-1873-2012/1890-lake-superior/+page.svelte
cp -vi time-to-make-a-mess/content/american_railroad_maps_1873_2012/1919_railroad_valuation_map_itasca.txt src/routes/american-railroad-maps-1873-2012/1919-railroad-valuation-map-itasca/+page.svelte
cp -vi time-to-make-a-mess/content/american_railroad_maps_1873_2012/1909_rand_mcnally_business_atlas_florida.txt src/routes/american-railroad-maps-1873-2012/1909-rand-mcnally-business-atlas-florida/+page.svelte
cp -vi time-to-make-a-mess/content/american_railroad_maps_1873_2012/1971_amtrak_passenger.txt src/routes/american-railroad-maps-1873-2012/1971-amtrak-passenger/+page.svelte
cp -vi time-to-make-a-mess/content/american_railroad_maps_1873_2012/1870_white_mountains.txt src/routes/american-railroad-maps-1873-2012/1870-white-mountains/+page.svelte
cp -vi time-to-make-a-mess/content/american_railroad_maps_1873_2012/1945_railroad_operations_map_industrial.txt src/routes/american-railroad-maps-1873-2012/1945-railroad-operations-map-industrial/+page.svelte
cp -vi time-to-make-a-mess/content/american_railroad_maps_1873_2012/1925_atlas_of_traffic_maps.txt src/routes/american-railroad-maps-1873-2012/1925-atlas-of-traffic-maps/+page.svelte
cp -vi time-to-make-a-mess/content/american_railroad_maps_1873_2012/1920_milwaukee_road.txt src/routes/american-railroad-maps-1873-2012/1920-milwaukee-road/+page.svelte
cp -vi time-to-make-a-mess/content/american_railroad_maps_1873_2012/1916_georgia_railroad.docx src/routes/american-railroad-maps-1873-2012/1916-georgia-railroad./+page.svelte
cp -vi time-to-make-a-mess/content/amercan_railroad_maps_1828_1876/1836_vision_us_rr.txt src/routes/amercan-railroad-maps-1828-1876/1836-vision-us-rr/+page.svelte
cp -vi time-to-make-a-mess/content/amercan_railroad_maps_1828_1876/1854_rr_us.txt src/routes/amercan-railroad-maps-1828-1876/1854-rr-us/+page.svelte
cp -vi time-to-make-a-mess/content/amercan_railroad_maps_1828_1876/1828_survey_boston.txt src/routes/amercan-railroad-maps-1828-1876/1828-survey-boston/+page.svelte
cp -vi time-to-make-a-mess/content/amercan_railroad_maps_1828_1876/1847_boston_worcester.txt src/routes/amercan-railroad-maps-1828-1876/1847-boston-worcester/+page.svelte
cp -vi time-to-make-a-mess/content/amercan_railroad_maps_1828_1876/1854_northern_rr.txt src/routes/amercan-railroad-maps-1828-1876/1854-northern-rr/+page.svelte
cp -vi time-to-make-a-mess/content/amercan_railroad_maps_1828_1876/1872_track_book.txt src/routes/amercan-railroad-maps-1828-1876/1872-track-book/+page.svelte
cp -vi time-to-make-a-mess/content/amercan_railroad_maps_1828_1876/1873_atsf_kansas.txt src/routes/amercan-railroad-maps-1828-1876/1873-atsf-kansas/+page.svelte
cp -vi time-to-make-a-mess/content/amercan_railroad_maps_1828_1876/1854_illinois_land_grant.txt src/routes/amercan-railroad-maps-1828-1876/1854-illinois-land-grant/+page.svelte
cp -vi time-to-make-a-mess/content/amercan_railroad_maps_1828_1876/1867_penn_rr.txt src/routes/amercan-railroad-maps-1828-1876/1867-penn-rr/+page.svelte
cp -vi time-to-make-a-mess/content/amercan_railroad_maps_1828_1876/1853_pacific_rr.txt src/routes/amercan-railroad-maps-1828-1876/1853-pacific-rr/+page.svelte
cp -vi time-to-make-a-mess/content/mapping_migration/1843_northwestern_states.txt src/routes/mapping-migration/1843-northwestern-states/+page.svelte
cp -vi time-to-make-a-mess/content/mapping_migration/1898_underground_railroad_routes.txt src/routes/mapping-migration/1898-underground-railroad-routes/+page.svelte
cp -vi time-to-make-a-mess/content/mapping_migration/1863_slave_populations.txt src/routes/mapping-migration/1863-slave-populations/+page.svelte
cp -vi time-to-make-a-mess/content/mapping_migration/1856_eastern_kansas.txt src/routes/mapping-migration/1856-eastern-kansas/+page.svelte
cp -vi time-to-make-a-mess/content/mapping_migration/1846_german_immigration.txt src/routes/mapping-migration/1846-german-immigration/+page.svelte
cp -vi time-to-make-a-mess/content/mapping_migration/1886_plat_book_clayton.txt src/routes/mapping-migration/1886-plat-book-clayton/+page.svelte
cp -vi time-to-make-a-mess/content/mapping_migration/1849_route_california.txt src/routes/mapping-migration/1849-route-california/+page.svelte
cp -vi time-to-make-a-mess/content/mapping_migration/1886_fire_insurance_chicago.txt src/routes/mapping-migration/1886-fire-insurance-chicago/+page.svelte
cp -vi time-to-make-a-mess/content/mapping_migration/1895_nationalities_chicago.txt src/routes/mapping-migration/1895-nationalities-chicago/+page.svelte
cp -vi time-to-make-a-mess/content/mapping_migration/1898_statistical_atlas_us.txt src/routes/mapping-migration/1898-statistical-atlas-us/+page.svelte
cp -vi time-to-make-a-mess/content/maps_movement_lit/1755_british_french.txt src/routes/maps-movement-lit/1755-british-french/+page.svelte
cp -vi time-to-make-a-mess/content/maps_movement_lit/1836_michigan_ouisconsin.txt src/routes/maps-movement-lit/1836-michigan-ouisconsin/+page.svelte
cp -vi time-to-make-a-mess/content/maps_movement_lit/1862_mississippi.txt src/routes/maps-movement-lit/1862-mississippi/+page.svelte
cp -vi time-to-make-a-mess/content/maps_movement_lit/1784_usa.txt src/routes/maps-movement-lit/1784-usa/+page.svelte
cp -vi time-to-make-a-mess/content/maps_movement_lit/1884_zigzag.txt src/routes/maps-movement-lit/1884-zigzag/+page.svelte
cp -vi time-to-make-a-mess/content/maps_movement_lit/1657_island_barbados.txt src/routes/maps-movement-lit/1657-island-barbados/+page.svelte
cp -vi time-to-make-a-mess/content/maps_movement_lit/1616_new_england.txt src/routes/maps-movement-lit/1616-new-england/+page.svelte
cp -vi time-to-make-a-mess/content/maps_movement_lit/1955_road_map_southwestern.txt src/routes/maps-movement-lit/1955-road-map-southwestern/+page.svelte
cp -vi time-to-make-a-mess/content/maps_movement_lit/1494_christopher_columbus.txt src/routes/maps-movement-lit/1494-christopher-columbus/+page.svelte
cp -vi time-to-make-a-mess/content/maps_movement_lit/1906_hollow_earth.txt src/routes/maps-movement-lit/1906-hollow-earth/+page.svelte
cp -vi time-to-make-a-mess/content/state_and_federal/1905_panama_canal.txt src/routes/state-and-federal/1905-panama-canal/+page.svelte
cp -vi time-to-make-a-mess/content/state_and_federal/1846_fremont_oregon.txt src/routes/state-and-federal/1846-fremont-oregon/+page.svelte
cp -vi time-to-make-a-mess/content/state_and_federal/1821_canal_lake_erie.txt src/routes/state-and-federal/1821-canal-lake-erie/+page.svelte
cp -vi time-to-make-a-mess/content/state_and_federal/1808_rapids_ohio.txt src/routes/state-and-federal/1808-rapids-ohio/+page.svelte
cp -vi time-to-make-a-mess/content/state_and_federal/1945_ancient_courses.txt src/routes/state-and-federal/1945-ancient-courses/+page.svelte
cp -vi time-to-make-a-mess/content/state_and_federal/1866_union_pacific_rr.txt src/routes/state-and-federal/1866-union-pacific-rr/+page.svelte
cp -vi time-to-make-a-mess/content/state_and_federal/1858_chicago_harbor.txt src/routes/state-and-federal/1858-chicago-harbor/+page.svelte
cp -vi time-to-make-a-mess/content/state_and_federal/1841_us_expeditionary_oregon.txt src/routes/state-and-federal/1841-us-expeditionary-oregon/+page.svelte
cp -vi time-to-make-a-mess/content/state_and_federal/1803_lewis_clark.txt src/routes/state-and-federal/1803-lewis-clark/+page.svelte
cp -vi time-to-make-a-mess/content/state_and_federal/1944_silk_chart.txt src/routes/state-and-federal/1944-silk-chart/+page.svelte
cp -vi time-to-make-a-mess/content/waterways_cartography_part_1/1829_canals_and_railroads.txt src/routes/waterways-cartography-part-1/1829-canals-and-railroads/+page.svelte
cp -vi time-to-make-a-mess/content/waterways_cartography_part_1/1852_proposed_dams_and_jetties.txt src/routes/waterways-cartography-part-1/1852-proposed-dams-and-jetties/+page.svelte
cp -vi time-to-make-a-mess/content/waterways_cartography_part_1/1869_proposed_canal_inland.txt src/routes/waterways-cartography-part-1/1869-proposed-canal-inland/+page.svelte
cp -vi time-to-make-a-mess/content/waterways_cartography_part_1/1820_plan_and_profile_erie_canal.txt src/routes/waterways-cartography-part-1/1820-plan-and-profile-erie-canal/+page.svelte
cp -vi time-to-make-a-mess/content/waterways_cartography_part_1/1862_canada_lakes_and_canals.txt src/routes/waterways-cartography-part-1/1862-canada-lakes-and-canals/+page.svelte
cp -vi time-to-make-a-mess/content/waterways_cartography_part_1/1817_navigation_guide_for_inland_rivers.txt src/routes/waterways-cartography-part-1/1817-navigation-guide-for-inland-rivers/+page.svelte
cp -vi time-to-make-a-mess/content/waterways_cartography_part_1/1878_longitudinal_sections.txt src/routes/waterways-cartography-part-1/1878-longitudinal-sections/+page.svelte
cp -vi time-to-make-a-mess/content/waterways_cartography_part_1/1848_travel_guide_hudson.txt src/routes/waterways-cartography-part-1/1848-travel-guide-hudson/+page.svelte
cp -vi time-to-make-a-mess/content/waterways_cartography_part_1/1808_proposed_canal_new_york.txt src/routes/waterways-cartography-part-1/1808-proposed-canal-new-york/+page.svelte
cp -vi time-to-make-a-mess/content/waterways_cartography_part_1/1862_canal_profiles_new_york.txt src/routes/waterways-cartography-part-1/1862-canal-profiles-new-york/+page.svelte
cp -vi time-to-make-a-mess/content/navigating_the_coasts/1859_frontiers_of_ocean.txt src/routes/navigating-the-coasts/1859-frontiers-of-ocean/+page.svelte
cp -vi time-to-make-a-mess/content/navigating_the_coasts/1891_algae_concentrations.txt src/routes/navigating-the-coasts/1891-algae-concentrations/+page.svelte
cp -vi time-to-make-a-mess/content/navigating_the_coasts/1833_stage_and_steamboat_routes_ohio.txt src/routes/navigating-the-coasts/1833-stage-and-steamboat-routes-ohio/+page.svelte
cp -vi time-to-make-a-mess/content/navigating_the_coasts/1732_variations_of_the_compass.txt src/routes/navigating-the-coasts/1732-variations-of-the-compass/+page.svelte
cp -vi time-to-make-a-mess/content/navigating_the_coasts/1732_coast_pilot_chart.txt src/routes/navigating-the-coasts/1732-coast-pilot-chart/+page.svelte
cp -vi time-to-make-a-mess/content/navigating_the_coasts/1838_us_coast_survey_chart_new_haven.txt src/routes/navigating-the-coasts/1838-us-coast-survey-chart-new-haven/+page.svelte
cp -vi time-to-make-a-mess/content/navigating_the_coasts/1934_us_coast_survey_chart_hawaii.txt src/routes/navigating-the-coasts/1934-us-coast-survey-chart-hawaii/+page.svelte
cp -vi time-to-make-a-mess/content/navigating_the_coasts/1820_ships_and_navigation.txt src/routes/navigating-the-coasts/1820-ships-and-navigation/+page.svelte
cp -vi time-to-make-a-mess/content/navigating_the_coasts/1807_trade_winds.txt src/routes/navigating-the-coasts/1807-trade-winds/+page.svelte
cp -vi time-to-make-a-mess/content/navigating_the_coasts/1877_austro_hungarian.txt src/routes/navigating-the-coasts/1877-austro-hungarian/+page.svelte
cp -vi time-to-make-a-mess/content/european_maps_for_exploration/1612_virginia.txt src/routes/european-maps-for-exploration/1612-virginia/+page.svelte
cp -vi time-to-make-a-mess/content/european_maps_for_exploration/1541_nautical_chart_of_the_pacific_coast_of_mexico.txt src/routes/european-maps-for-exploration/1541-nautical-chart-of-the-pacific-coast-of-mexico/+page.svelte
cp -vi time-to-make-a-mess/content/european_maps_for_exploration/1703_new_voyages_in_northern_america.txt src/routes/european-maps-for-exploration/1703-new-voyages-in-northern-america/+page.svelte
cp -vi time-to-make-a-mess/content/european_maps_for_exploration/1801_mackenzies_northern_voyages.txt src/routes/european-maps-for-exploration/1801-mackenzies-northern-voyages/+page.svelte
cp -vi time-to-make-a-mess/content/european_maps_for_exploration/1688_globe_gore_of_eastern_north_america.txt src/routes/european-maps-for-exploration/1688-globe-gore-of-eastern-north-america/+page.svelte
cp -vi time-to-make-a-mess/content/european_maps_for_exploration/1598_florida_and_the_apalachee_lands.txt src/routes/european-maps-for-exploration/1598-florida-and-the-apalachee-lands/+page.svelte
cp -vi time-to-make-a-mess/content/european_maps_for_exploration/1672_jesuit_map_of_lake_superior.txt src/routes/european-maps-for-exploration/1672-jesuit-map-of-lake-superior/+page.svelte
cp -vi time-to-make-a-mess/content/european_maps_for_exploration/1761_russian_voyages_in_the_pacific_northwest.txt src/routes/european-maps-for-exploration/1761-russian-voyages-in-the-pacific-northwest/+page.svelte
cp -vi time-to-make-a-mess/content/european_maps_for_exploration/1612_new_france.txt src/routes/european-maps-for-exploration/1612-new-france/+page.svelte
cp -vi time-to-make-a-mess/content/european_maps_for_exploration/1784_map_of_kentucky.txt src/routes/european-maps-for-exploration/1784-map-of-kentucky/+page.svelte
cp -vi time-to-make-a-mess/content/waterways_cartography_part_2/1964_commodities.txt src/routes/waterways-cartography-part-2/1964-commodities/+page.svelte
cp -vi time-to-make-a-mess/content/waterways_cartography_part_2/1894_water_transportation_routes_us.txt src/routes/waterways-cartography-part-2/1894-water-transportation-routes-us/+page.svelte
cp -vi time-to-make-a-mess/content/waterways_cartography_part_2/1908_freight_rates_inland.txt src/routes/waterways-cartography-part-2/1908-freight-rates-inland/+page.svelte
cp -vi time-to-make-a-mess/content/waterways_cartography_part_2/1931_buffalo_great_lakes.txt src/routes/waterways-cartography-part-2/1931-buffalo-great-lakes/+page.svelte
cp -vi time-to-make-a-mess/content/waterways_cartography_part_2/1949_intracoastal_waterway_nj.txt src/routes/waterways-cartography-part-2/1949-intracoastal-waterway-nj/+page.svelte
cp -vi time-to-make-a-mess/content/waterways_cartography_part_2/1970_nautical_charts_illinois.txt src/routes/waterways-cartography-part-2/1970-nautical-charts-illinois/+page.svelte
cp -vi time-to-make-a-mess/content/waterways_cartography_part_2/1904_canals_ohio.txt src/routes/waterways-cartography-part-2/1904-canals-ohio/+page.svelte
cp -vi time-to-make-a-mess/content/waterways_cartography_part_2/1936_us_lake_survey.txt src/routes/waterways-cartography-part-2/1936-us-lake-survey/+page.svelte
cp -vi time-to-make-a-mess/content/waterways_cartography_part_2/1882_inland_canals.txt src/routes/waterways-cartography-part-2/1882-inland-canals/+page.svelte
cp -vi time-to-make-a-mess/content/waterways_cartography_part_2/1898_water_routes_yukon.txt src/routes/waterways-cartography-part-2/1898-water-routes-yukon/+page.svelte
