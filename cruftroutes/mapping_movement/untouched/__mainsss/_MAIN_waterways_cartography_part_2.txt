Waterways Cartography, Part II: Landmarks and Exemplars in North America

Further Reading
The previous discussion of canal and waterways maps is in some ways unique in that it employs their cartography to explore North American history, especially how inland waters carried passengers and freight back and forth across the continent as economic, political, social, and cultural functions. There are literally thousands of excellent books and articles that tell the story of virtually every canal, waterway, river, lake, inland port, and navigation system that has served the continent's inhabitants. Maps have attended these studies in two ways: (1) as primary sources left behind after the planning, building, use, modification, and overall functioning of the particular facility and (2) as creations by later people who turned to cartography to narrate the tale or gauge the effectiveness of particular works, routes, or functions of these waterways.
      From the very start scholars at various times have surveyed the major existing works and systems and described them in a systematic way. The entry on “Canals” in both the original British and subsequent American edition of the Cyclopaedia compiled by Abraham Rees serve this function on a global scale. George Armroyd’s A Connected View of the Whole Internal Navigation of the United States appeared in two editions, 1826 and 1830 (reprinted, 1971), needing over 600 pages to complete its coverage.
      A major effort by the Division of Transportation of the Carnegie Institution of Washington in the early twentieth century led to several dozen studies by individual scholars which formed the base for a summary History of Transportation in the United States before 1860 (1917) prepared under the direction of Balthasar Henry Meyer by Caroline E. MacGill and her staff. This landmark volume was reprinted in 1948. The Preliminary Report of the Inland Waterways Commission published by the Government Printing Office as a Senate Document in 1908 had a different function, but ranged widely in gathering background information of great use to later scholars. It was reprinted in 1972.
      In contrast, to these lengthy tomes, most readers learned about Old Towpaths: The Story of the American Canal Era from the sprightly book by Alvin F. Harlow issued in 1926. It is a nostalgic account, ending in a lament for a world that lost out to “the neurotic whirl of our present-day business and social life.” Although it is comprehensive in its coverage, its usefulness is limited by the absence of notes and the lack of an index. Moreover, its tone has shaded the popular image of the canal era and suggested that waterways disappeared with the coming of the railroad. The cartographic record, of course, tells a different tale and one must convert to a divergent mindset to reap the full benefits of these maps.
      Going further will be a do-it-yourself project for the researcher, but let me suggest eight reading assignments that might be useful in getting started.  First, the researcher should use a survey of the Canal Era in the United States, 1790-1860 by a modern scholar such as Ronald E. Shaw’s Canals for a Nation (1990), a brief account that touches many topics and themes. The second and third suggestions are to read two books in tandem: Forest G. Hill, Roads, Rails, & Waterways: The Army Engineers and Early Transportation (1957) and Arthur Maass, Muddy Waters: The Army Engineers and the Nation’s Rivers (1951). This would be best followed by a book or an article in the continuing debate between natural free-flow advocates and those confident in engineering solutions to riverine problems.
      The fourth and fifth suggestions are to use some deeper history to illuminate the European context for America’s use of waterways as transportation arteries. The researcher would be well served to start with L. T.C. Rolt’s From Sea to Sea: The Canal du Midi (1973), then to read Chandra Mukerji's, Impossible Engineering (2009), the same topic seen through several different lenses, and conclude with her article, “Printing, Cartography, and Conceptions of Place in Renaissance Europe,” in Media, Culture, and Society, 28:5 (September, 2006), 651-669.
      The next two approaches involve much less arduous reading. The sixth, Towpaths to Tugboats: A History of American Canal Engineering (1985) is a pamphlet published by the American Canal and Transportation Center for a popular audience. It hails the achievements of the engineers as milestones on the road to progress. The other suggestion, the seventh, is a clearly written scientific study, Hydrology and Environmental Aspects of Erie Canal (1817-99). The illustrations alone in this ninety-two-page report, US Geological Survey Water-Supply Paper number 2018, are worth the price of admission.
      The eighth, and final, recommendation starts with Robert C. Post’s Technology, Transport, and Travel in American History (2003), a short volume in the Historical Perspectives on Technology, Society, and Culture series issued by the American Historical Association and the Society for the History of Technology. Three of its nine chapters deal with Canals, the Inland Seacoast, and Riverways. Brief essays, followed by several pages of useful notes are really a preface to a long bibliography.
      In the end, going researching further depends on where one's subject is planted. It will be necessary to use local sources and state or regional finding aids to discover materials of use. But one should not neglect general or related documents. Remember: Developing a context for the research will provide keys to its meaning and significance. Several of Post’s suggestions will probably be useful springboards, but I have my own favorites to add to his list:

Albion, Robert Greenhalgh, The Rise of New York Port, 1815-1860, (1939).

Becht, J. Edwin, Commodity Origins, Traffic and Markets Accessible to Chicago via the Illinois Waterway, (1952).

Cudahy, Brian J., Box Boats: How Container Ships Changed the World, (2006).

Dixon, Frank Haigh, A Traffic History of the Mississippi River System, (1909).

Harris, Robert, Canals and their Architecture, (1969).

Howe, Charles W. , et al., Inland Waterway Transportation: Studies in Public and Private Management and Investment Decisions, (1969).

Larson, John L., Internal Improvement: National Public Works and the Promise of Popular Government in the Early United States, (2001).

McCool, Daniel, River Republic: The Fall and Rise of America’s Rivers, (2012).

McCullough, Robert, and Walter Leuba, The Pennsylvania Main Line Canal, (1973).

Shallat, Todd, Structures in the Stream: Water, Science, and the Rise of the U. S. Army Corps of Engineers, (1994).

Sheriff, Carol, The Artificial River: The Erie Canal and the Paradox of Progress, 1817-1862, (1996).

Stine, Jeffrey K., Mixing the Waters: Environment, Politics, and the Building of the Tennessee-Tombigbee Waterway, (1993).

 
 