American Railroad Maps, 1873-2012

Further Reading
Among the special characteristics of maps is their ability to push readers into further searching and investigation. Something strikes us about a particular example and we would like to know more about it. The problem is that our quest starts out vague and confused, without a specific question in mind or even a general direction to arrange our thoughts. Maybe before jumping immediately into a search for context and understanding we might profit by reflecting on what it is about this map that attracts us. First read the “Further Reading” section of the unit on “American Railroad Maps, 1828-1876.” Then, if so inclined look at the focus maps in this unit and note our following responses to the question each one raised for us: “What particularly interests us about this map?” Our Responses:

1. Through the White Mountains, (1880). Reading a topographic map is always a challenge because a reader must start with the lay of the land. In this case the mountains create barriers, dividing the land into a series of valley. How will the railroad get through them, reaching to Portland on one end and to Ogdensburg on the other? How does the topography influence the route?

2. “The movement of Troops to quell the North West Trouble,” (1885). Upon first glance this seems like an ordinary map, but when we read its title, our interest in stimulated. What does this map mean? Although the cartography seems like an ordinary railroad map, the title spells out something extraordinary, pushing readers to find out more about it.

3. The Duluth, South Shore, & Atlantic Railway (1890). This is obviously a tourist map designed to stimulate passenger traffic. Its design and use of color attract our attention. Why is this piece so effective in its graphic design? How does “the look of the map” help it achieve its purpose?

4. A Business Atlas Map of Florida, 1909. How would one design a special atlas addressed to business concerns, especially people interested in sending goods or services from one place to another in the United States? In shaping an answer be sure to consider such elements as accuracy, providing all the necessary information, clarity, and the need for frequent revision.

5. Railroad Map of Georgia, 1916. Many states issued a railroad map each year. These became precursors of state highway maps which started about the time this map appeared. Both types of transportation maps also served other purposes. Who would use a railroad map? Why did they want to use a map that covered only one state?

6. Railroad Valuation Maps, 1919. Large-scale maps provided useful aids in totaling the value of a company’s physical assets. This was important in setting the rates they could charge. Like track charts, these maps often included other features along the right-of-way such as private buildings, roads, and parks. Use this sheet to reconstruct the past geography of Itasca.

7. A Time-Table Map, 1920. Passenger railroads in the United States from the very beginning published time tables in newspapers and on handbills or posters. Eventually they included a map of the route or the system along with the schedules. What value was added by including a map? What features on the map would appeal to readers?

8. A Railway Traffic Map, 1925. Traffic maps addressed people concerned with the operation of a railroad, especially students learning about freight rates and territories. How do they differ from railroad maps created for the general public?

9. A Track Chart: Kansas City, 1945. Large-scale track charts such as this one help us understand railroad operations. This example shows a key transportation node involving several railroads in a dense urban setting. Use it to compare with the small town in focus map 6.

10. Creating a National Railroad Network, 1971. When planners envisioned the national passenger railroad network that eventually became Amtrak, the goal was to cross each state with both a north-south and an east-west route. This ideal soon became impractical. The final choice for the initial network is shown here. Check it out for limitations and suggest possible changes.

 