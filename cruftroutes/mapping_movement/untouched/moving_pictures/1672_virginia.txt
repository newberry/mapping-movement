Territory of Virginia, 1672
Referenced by Essay: 
Moving Pictures: Maps and Imagination in Eighteenth-Century Anglo-America
Lederer’s map was attached to the published account of his journey, The Discoveries of John Lederer, published in London in 1672. Lederer’s original journal (in Latin) and map were transmitted and prepared for publication by William Talbot, Provincial Secretary for Maryland. Originally part of a company of Virginia colonists interested in exploring trade opportunities with Southeastern Indians, Lederer found himself alone after the Virginians abandoned him. He then continued his long trek southward accompanied by a Susquehannock Indian guide familiar with, but not native to, the region. Lederer’s map influenced the mapping of the Southeast for many years to come.

Selection Gallery
Territory of Virginia, 1672
Citation:
Lederer, John, "A Map of the Whole Territory Traversed by Iohn Lederer in His Three Marches", in The Discoveries of John Lederer, in Three Several Marches from Virginia, to the West of Carolina, and Other Parts of the Continent : Begun in March 1669, and Ended in September 1670 (London : Printed by J[ohn]. C[ottrell]. for S. Heyrick, 1672). VAULT Ayer 150.5 .V7 L4 1672