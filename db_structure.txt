miniessay
ID
    title
    path
    parent
    parentpath
    text (UNKNOWN LENGTH)
    mapids*
    ctxiiifmanifest

maps
ID
    title
    citation
    ogfn
    ctxiiifcanvas
    ctxiiifmanifest
    miniessay*

text
ID
    essayid
    miniessayid
    text
    htmltag
    order

essay
ID
    title
    path
    author
    text (UNKNOWN LENGTH)
    miniessays*
    bib*
    fr*
    images*
    text1?
    text1tag?

bibs
ID
    text
    parentid

fr
ID
    text
    parentid

