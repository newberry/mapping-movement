import db from '$lib/db';

export async function load() {
	const data = await db.essays.findMany();
	const toc = await data.map((d) => ({ title: d.essaytitle, slug: d.essayslug, id: d.essayno }));
	return { toc };
}
