mkdir -p american-railroad-maps-1828-1876
mkdir -p american-railroad-maps-1873-2012
mkdir -p around-over-through
mkdir -p european-maps-for-exploration
mkdir -p lines-that-fracture
mkdir -p mapping-communication
mkdir -p mapping-migration
mkdir -p maps-movement-lit
mkdir -p maps-of-trails-and-roads
mkdir -p moving-pictures
mkdir -p navigating-the-coasts-and-seas
mkdir -p planning-transportation
mkdir -p state-and-federal
mkdir -p waterways-cartography-part-1
mkdir -p waterways-cartography-part-2
mkdir -p planning-transportation/1898-property-chicago
mkdir -p planning-transportation/1895-road-map-ny
mkdir -p planning-transportation/1911-electric-la
mkdir -p planning-transportation/1969-plan-nyc
mkdir -p planning-transportation/1937-new-england-regional
mkdir -p planning-transportation/1876-railway-philadelphia
mkdir -p planning-transportation/1951-valparaiso
mkdir -p planning-transportation/1836-north-river
mkdir -p planning-transportation/1961-new-orleans
mkdir -p planning-transportation/1946-preliminary-chicago
mkdir -p moving-pictures/1755-british-colonies
mkdir -p moving-pictures/1757-south-carolina
mkdir -p moving-pictures/1730-charecke-nation
mkdir -p moving-pictures/1672-virginia
mkdir -p moving-pictures/1720-territorial-claims
mkdir -p moving-pictures/1755-virginia
mkdir -p moving-pictures/1780-south-carolina
mkdir -p moving-pictures/1777-charles-town
mkdir -p moving-pictures/1775-carolinas
mkdir -p moving-pictures/1775-war-new-england
mkdir -p maps-of-trails-and-roads-of-the-great-west/1857-overland-mail-route-to-california
mkdir -p maps-of-trails-and-roads-of-the-great-west/1859-route-from-kansas-city
mkdir -p maps-of-trails-and-roads-of-the-great-west/1875-best-and-shortest-cattle
mkdir -p maps-of-trails-and-roads-of-the-great-west/1870-territory-of-montana
mkdir -p maps-of-trails-and-roads-of-the-great-west/1846-new-map-of-texas
mkdir -p maps-of-trails-and-roads-of-the-great-west/1898-gold-and-coal-fields
mkdir -p maps-of-trails-and-roads-of-the-great-west/1811-road-from-capital-of-new-spain
mkdir -p maps-of-trails-and-roads-of-the-great-west/1779-map-of-the-province-of-new-mexico
mkdir -p maps-of-trails-and-roads-of-the-great-west/1863-military-road-from-fort-walla
mkdir -p maps-of-trails-and-roads-of-the-great-west/1830-map-of-texas-with-parts
mkdir -p mapping-communication/1773-colonial-post-roads
mkdir -p mapping-communication/1899-new-economy
mkdir -p mapping-communication/1889-standard-time
mkdir -p mapping-communication/1858-wire-across-ocean
mkdir -p mapping-communication/1932-rivers-roads-rates
mkdir -p mapping-communication/1866-wire-across-continent
mkdir -p mapping-communication/1768-chart-gulf-stream
mkdir -p mapping-communication/1849-routes-to-california
mkdir -p mapping-communication/1905-independent-telephone
mkdir -p mapping-communication/1926-radio-stations
mkdir -p around-over-through/1909-route-bogota
mkdir -p around-over-through/1726-louisiana-mexico
mkdir -p around-over-through/1933-ye-olde-spanish
mkdir -p around-over-through/1848-military-leavenworth
mkdir -p around-over-through/1908-united-brazil
mkdir -p around-over-through/1855-panama-rr
mkdir -p around-over-through/1929-airmail-argentina
mkdir -p around-over-through/1840-atlas-venezuela
mkdir -p around-over-through/1938-motorists-mexico
mkdir -p around-over-through/1950-panorama-american
mkdir -p lines-that-fracture/1930-motor-routes-to-augusta
mkdir -p lines-that-fracture/1863-usarmy-map-of-northwest-georgia
mkdir -p lines-that-fracture/1822-state-of-georgia
mkdir -p lines-that-fracture/1835-north-carolina-south
mkdir -p lines-that-fracture/1919-national-highways-proposed-in-georgia
mkdir -p lines-that-fracture/1869-georgia-central-railroad
mkdir -p lines-that-fracture/1968-principal-us-electric
mkdir -p lines-that-fracture/1976-mapping-the-travels-of-john
mkdir -p lines-that-fracture/1775-southern-indian-district-of-north-america
mkdir -p lines-that-fracture/1818-map-of-the-state-of-georgia
mkdir -p american-railroad-maps-1873-2012/1886-canadian-pacific
mkdir -p american-railroad-maps-1873-2012/1916-georgia-railroad
mkdir -p american-railroad-maps-1873-2012/1890-lake-superior
mkdir -p american-railroad-maps-1873-2012/1919-railroad-valuation-map-itasca
mkdir -p american-railroad-maps-1873-2012/1909-rand-mcnally-business-atlas-florida
mkdir -p american-railroad-maps-1873-2012/1971-amtrak-passenger
mkdir -p american-railroad-maps-1873-2012/1870-white-mountains
mkdir -p american-railroad-maps-1873-2012/1945-railroad-operations-map-industrial
mkdir -p american-railroad-maps-1873-2012/1925-atlas-of-traffic-maps
mkdir -p american-railroad-maps-1873-2012/1920-milwaukee-road
mkdir -p american-railroad-maps-1873-2012/1916-georgia-railroad.docx
mkdir -p amercan-railroad-maps-1828-1876/1836-vision-us-rr
mkdir -p amercan-railroad-maps-1828-1876/1854-rr-us
mkdir -p amercan-railroad-maps-1828-1876/1828-survey-boston
mkdir -p amercan-railroad-maps-1828-1876/1847-boston-worcester
mkdir -p amercan-railroad-maps-1828-1876/1854-northern-rr
mkdir -p amercan-railroad-maps-1828-1876/1872-track-book
mkdir -p amercan-railroad-maps-1828-1876/1873-atsf-kansas
mkdir -p amercan-railroad-maps-1828-1876/1854-illinois-land-grant
mkdir -p amercan-railroad-maps-1828-1876/1867-penn-rr
mkdir -p amercan-railroad-maps-1828-1876/1853-pacific-rr
mkdir -p mapping-migration/1843-northwestern-states
mkdir -p mapping-migration/1898-underground-railroad-routes
mkdir -p mapping-migration/1863-slave-populations
mkdir -p mapping-migration/1856-eastern-kansas
mkdir -p mapping-migration/1846-german-immigration
mkdir -p mapping-migration/1886-plat-book-clayton
mkdir -p mapping-migration/1849-route-california
mkdir -p mapping-migration/1886-fire-insurance-chicago
mkdir -p mapping-migration/1895-nationalities-chicago
mkdir -p mapping-migration/1898-statistical-atlas-us
mkdir -p maps-movement-lit/1755-british-french
mkdir -p maps-movement-lit/1836-michigan-ouisconsin
mkdir -p maps-movement-lit/1862-mississippi
mkdir -p maps-movement-lit/1784-usa
mkdir -p maps-movement-lit/1884-zigzag
mkdir -p maps-movement-lit/1657-island-barbados
mkdir -p maps-movement-lit/1616-new-england
mkdir -p maps-movement-lit/1955-road-map-southwestern
mkdir -p maps-movement-lit/1494-christopher-columbus
mkdir -p maps-movement-lit/1906-hollow-earth
mkdir -p state-and-federal/1905-panama-canal
mkdir -p state-and-federal/1846-fremont-oregon
mkdir -p state-and-federal/1821-canal-lake-erie
mkdir -p state-and-federal/1808-rapids-ohio
mkdir -p state-and-federal/1945-ancient-courses
mkdir -p state-and-federal/1866-union-pacific-rr
mkdir -p state-and-federal/1858-chicago-harbor
mkdir -p state-and-federal/1841-us-expeditionary-oregon
mkdir -p state-and-federal/1803-lewis-clark
mkdir -p state-and-federal/1944-silk-chart
mkdir -p waterways-cartography-part-1/1829-canals-and-railroads
mkdir -p waterways-cartography-part-1/1852-proposed-dams-and-jetties
mkdir -p waterways-cartography-part-1/1869-proposed-canal-inland
mkdir -p waterways-cartography-part-1/1820-plan-and-profile-erie-canal
mkdir -p waterways-cartography-part-1/1862-canada-lakes-and-canals
mkdir -p waterways-cartography-part-1/1817-navigation-guide-for-inland-rivers
mkdir -p waterways-cartography-part-1/1878-longitudinal-sections
mkdir -p waterways-cartography-part-1/1848-travel-guide-hudson
mkdir -p waterways-cartography-part-1/1808-proposed-canal-new-york
mkdir -p waterways-cartography-part-1/1862-canal-profiles-new-york
mkdir -p navigating-the-coasts/1859-frontiers-of-ocean
mkdir -p navigating-the-coasts/1891-algae-concentrations
mkdir -p navigating-the-coasts/1833-stage-and-steamboat-routes-ohio
mkdir -p navigating-the-coasts/1732-variations-of-the-compass
mkdir -p navigating-the-coasts/1732-coast-pilot-chart
mkdir -p navigating-the-coasts/1838-us-coast-survey-chart-new-haven
mkdir -p navigating-the-coasts/1934-us-coast-survey-chart-hawaii
mkdir -p navigating-the-coasts/1820-ships-and-navigation
mkdir -p navigating-the-coasts/1807-trade-winds
mkdir -p navigating-the-coasts/1877-austro-hungarian
mkdir -p european-maps-for-exploration/1612-virginia
mkdir -p european-maps-for-exploration/1541-nautical-chart-of-the-pacific-coast-of-mexico
mkdir -p european-maps-for-exploration/1703-new-voyages-in-northern-america
mkdir -p european-maps-for-exploration/1801-mackenzies-northern-voyages
mkdir -p european-maps-for-exploration/1688-globe-gore-of-eastern-north-america
mkdir -p european-maps-for-exploration/1598-florida-and-the-apalachee-lands
mkdir -p european-maps-for-exploration/1672-jesuit-map-of-lake-superior
mkdir -p european-maps-for-exploration/1761-russian-voyages-in-the-pacific-northwest
mkdir -p european-maps-for-exploration/1612-new-france
mkdir -p european-maps-for-exploration/1784-map-of-kentucky
mkdir -p waterways-cartography-part-2/1964-commodities
mkdir -p waterways-cartography-part-2/1894-water-transportation-routes-us
mkdir -p waterways-cartography-part-2/1908-freight-rates-inland
mkdir -p waterways-cartography-part-2/1931-buffalo-great-lakes
mkdir -p waterways-cartography-part-2/1949-intracoastal-waterway-nj
mkdir -p waterways-cartography-part-2/1970-nautical-charts-illinois
mkdir -p waterways-cartography-part-2/1904-canals-ohio
mkdir -p waterways-cartography-part-2/1936-us-lake-survey
mkdir -p waterways-cartography-part-2/1882-inland-canals
mkdir -p waterways-cartography-part-2/1898-water-routes-yukon
time-to-make-a-mess/content/planning_transportation/1898_property_chicago.txt
time-to-make-a-mess/content/planning_transportation/1895_road_map_ny.txt
time-to-make-a-mess/content/planning_transportation/1911_electric_la.txt
time-to-make-a-mess/content/planning_transportation/1969_plan_nyc.txt
time-to-make-a-mess/content/planning_transportation/1937_new_england_regional.txt
time-to-make-a-mess/content/planning_transportation/1876_railway_philadelphia.txt
time-to-make-a-mess/content/planning_transportation/1951_valparaiso.txt
time-to-make-a-mess/content/planning_transportation/1836_north_river.txt
time-to-make-a-mess/content/planning_transportation/1961_new_orleans.txt
time-to-make-a-mess/content/planning_transportation/1946_preliminary_chicago.txt
time-to-make-a-mess/content/moving_pictures/1755_british_colonies.txt
time-to-make-a-mess/content/moving_pictures/1757_south_carolina.txt
time-to-make-a-mess/content/moving_pictures/1730_charecke_nation.txt
time-to-make-a-mess/content/moving_pictures/1672_virginia.txt
time-to-make-a-mess/content/moving_pictures/1720_territorial_claims.txt
time-to-make-a-mess/content/moving_pictures/1755_virginia.txt
time-to-make-a-mess/content/moving_pictures/1780_south_carolina.txt
time-to-make-a-mess/content/moving_pictures/1777_charles_town.txt
time-to-make-a-mess/content/moving_pictures/1775_carolinas.txt
time-to-make-a-mess/content/moving_pictures/1775_war_new_england.txt
time-to-make-a-mess/content/maps_of_trails_and_roads_of_the_great_west/1857_overland_mail_route_to_california.txt
time-to-make-a-mess/content/maps_of_trails_and_roads_of_the_great_west/1859_route_from_kansas_city.txt
time-to-make-a-mess/content/maps_of_trails_and_roads_of_the_great_west/1875_best_and_shortest_cattle.txt
time-to-make-a-mess/content/maps_of_trails_and_roads_of_the_great_west/1870_territory_of_montana.txt
time-to-make-a-mess/content/maps_of_trails_and_roads_of_the_great_west/1846_new_map_of_texas.txt
time-to-make-a-mess/content/maps_of_trails_and_roads_of_the_great_west/1898_gold_and_coal_fields.txt
time-to-make-a-mess/content/maps_of_trails_and_roads_of_the_great_west/1811_road_from_capital_of_new_spain.txt
time-to-make-a-mess/content/maps_of_trails_and_roads_of_the_great_west/1779_map_of_the_province_of_new_mexico.txt
time-to-make-a-mess/content/maps_of_trails_and_roads_of_the_great_west/1863_military_road_from_fort_walla.txt
time-to-make-a-mess/content/maps_of_trails_and_roads_of_the_great_west/1830_map_of_texas_with_parts.txt
time-to-make-a-mess/content/mapping_communication/1773_colonial_post_roads.txt
time-to-make-a-mess/content/mapping_communication/1899_new_economy.txt
time-to-make-a-mess/content/mapping_communication/1889_standard_time.txt
time-to-make-a-mess/content/mapping_communication/1858_wire_across_ocean.txt
time-to-make-a-mess/content/mapping_communication/1932_rivers_roads_rates.txt
time-to-make-a-mess/content/mapping_communication/1866_wire_across_continent.txt
time-to-make-a-mess/content/mapping_communication/1768_chart_gulf_stream.txt
time-to-make-a-mess/content/mapping_communication/1849_routes_to_california.txt
time-to-make-a-mess/content/mapping_communication/1905_independent_telephone.txt
time-to-make-a-mess/content/mapping_communication/1926_radio_stations.txt
time-to-make-a-mess/content/around_over_through/1909_route_bogota.txt
time-to-make-a-mess/content/around_over_through/1726_louisiana_mexico.txt
time-to-make-a-mess/content/around_over_through/1933_ye_olde_spanish.txt
time-to-make-a-mess/content/around_over_through/1848_military_leavenworth.txt
time-to-make-a-mess/content/around_over_through/1908_united_brazil.txt
time-to-make-a-mess/content/around_over_through/1855_panama_rr.txt
time-to-make-a-mess/content/around_over_through/1929_airmail_argentina.txt
time-to-make-a-mess/content/around_over_through/1840_atlas_venezuela.txt
time-to-make-a-mess/content/around_over_through/1938_motorists_mexico.txt
time-to-make-a-mess/content/around_over_through/1950_panorama_american.txt
time-to-make-a-mess/content/lines_that_fracture/1930_motor_routes_to_augusta.txt
time-to-make-a-mess/content/lines_that_fracture/1863_usarmy_map_of_northwest_georgia.txt
time-to-make-a-mess/content/lines_that_fracture/1822_state_of_georgia.txt
time-to-make-a-mess/content/lines_that_fracture/1835_north_carolina_south.txt
time-to-make-a-mess/content/lines_that_fracture/1919_national_highways_proposed_in_georgia.txt
time-to-make-a-mess/content/lines_that_fracture/1869_georgia_central_railroad.txt
time-to-make-a-mess/content/lines_that_fracture/1968_principal_us_electric.txt
time-to-make-a-mess/content/lines_that_fracture/1976_mapping_the_travels_of_john.txt
time-to-make-a-mess/content/lines_that_fracture/1775_southern_indian_district_of_north_america.txt
time-to-make-a-mess/content/lines_that_fracture/1818_map_of_the_state_of_georgia.txt
time-to-make-a-mess/content/american_railroad_maps_1873_2012/1886_canadian_pacific.txt
time-to-make-a-mess/content/american_railroad_maps_1873_2012/1916_georgia_railroad.txt
time-to-make-a-mess/content/american_railroad_maps_1873_2012/1890_lake_superior.txt
time-to-make-a-mess/content/american_railroad_maps_1873_2012/1919_railroad_valuation_map_itasca.txt
time-to-make-a-mess/content/american_railroad_maps_1873_2012/1909_rand_mcnally_business_atlas_florida.txt
time-to-make-a-mess/content/american_railroad_maps_1873_2012/1971_amtrak_passenger.txt
time-to-make-a-mess/content/american_railroad_maps_1873_2012/1870_white_mountains.txt
time-to-make-a-mess/content/american_railroad_maps_1873_2012/1945_railroad_operations_map_industrial.txt
time-to-make-a-mess/content/american_railroad_maps_1873_2012/1925_atlas_of_traffic_maps.txt
time-to-make-a-mess/content/american_railroad_maps_1873_2012/1920_milwaukee_road.txt
time-to-make-a-mess/content/american_railroad_maps_1873_2012/1916_georgia_railroad.docx
time-to-make-a-mess/content/amercan_railroad_maps_1828_1876/1836_vision_us_rr.txt
time-to-make-a-mess/content/amercan_railroad_maps_1828_1876/1854_rr_us.txt
time-to-make-a-mess/content/amercan_railroad_maps_1828_1876/1828_survey_boston.txt
time-to-make-a-mess/content/amercan_railroad_maps_1828_1876/1847_boston_worcester.txt
time-to-make-a-mess/content/amercan_railroad_maps_1828_1876/1854_northern_rr.txt
time-to-make-a-mess/content/amercan_railroad_maps_1828_1876/1872_track_book.txt
time-to-make-a-mess/content/amercan_railroad_maps_1828_1876/1873_atsf_kansas.txt
time-to-make-a-mess/content/amercan_railroad_maps_1828_1876/1854_illinois_land_grant.txt
time-to-make-a-mess/content/amercan_railroad_maps_1828_1876/1867_penn_rr.txt
time-to-make-a-mess/content/amercan_railroad_maps_1828_1876/1853_pacific_rr.txt
time-to-make-a-mess/content/mapping_migration/1843_northwestern_states.txt
time-to-make-a-mess/content/mapping_migration/1898_underground_railroad_routes.txt
time-to-make-a-mess/content/mapping_migration/1863_slave_populations.txt
time-to-make-a-mess/content/mapping_migration/1856_eastern_kansas.txt
time-to-make-a-mess/content/mapping_migration/1846_german_immigration.txt
time-to-make-a-mess/content/mapping_migration/1886_plat_book_clayton.txt
time-to-make-a-mess/content/mapping_migration/1849_route_california.txt
time-to-make-a-mess/content/mapping_migration/1886_fire_insurance_chicago.txt
time-to-make-a-mess/content/mapping_migration/1895_nationalities_chicago.txt
time-to-make-a-mess/content/mapping_migration/1898_statistical_atlas_us.txt
time-to-make-a-mess/content/maps_movement_lit/1755_british_french.txt
time-to-make-a-mess/content/maps_movement_lit/1836_michigan_ouisconsin.txt
time-to-make-a-mess/content/maps_movement_lit/1862_mississippi.txt
time-to-make-a-mess/content/maps_movement_lit/1784_usa.txt
time-to-make-a-mess/content/maps_movement_lit/1884_zigzag.txt
time-to-make-a-mess/content/maps_movement_lit/1657_island_barbados.txt
time-to-make-a-mess/content/maps_movement_lit/1616_new_england.txt
time-to-make-a-mess/content/maps_movement_lit/1955_road_map_southwestern.txt
time-to-make-a-mess/content/maps_movement_lit/1494_christopher_columbus.txt
time-to-make-a-mess/content/maps_movement_lit/1906_hollow_earth.txt
time-to-make-a-mess/content/state_and_federal/1905_panama_canal.txt
time-to-make-a-mess/content/state_and_federal/1846_fremont_oregon.txt
time-to-make-a-mess/content/state_and_federal/1821_canal_lake_erie.txt
time-to-make-a-mess/content/state_and_federal/1808_rapids_ohio.txt
time-to-make-a-mess/content/state_and_federal/1945_ancient_courses.txt
time-to-make-a-mess/content/state_and_federal/1866_union_pacific_rr.txt
time-to-make-a-mess/content/state_and_federal/1858_chicago_harbor.txt
time-to-make-a-mess/content/state_and_federal/1841_us_expeditionary_oregon.txt
time-to-make-a-mess/content/state_and_federal/1803_lewis_clark.txt
time-to-make-a-mess/content/state_and_federal/1944_silk_chart.txt
time-to-make-a-mess/content/waterways_cartography_part_1/1829_canals_and_railroads.txt
time-to-make-a-mess/content/waterways_cartography_part_1/1852_proposed_dams_and_jetties.txt
time-to-make-a-mess/content/waterways_cartography_part_1/1869_proposed_canal_inland.txt
time-to-make-a-mess/content/waterways_cartography_part_1/1820_plan_and_profile_erie_canal.txt
time-to-make-a-mess/content/waterways_cartography_part_1/1862_canada_lakes_and_canals.txt
time-to-make-a-mess/content/waterways_cartography_part_1/1817_navigation_guide_for_inland_rivers.txt
time-to-make-a-mess/content/waterways_cartography_part_1/1878_longitudinal_sections.txt
time-to-make-a-mess/content/waterways_cartography_part_1/1848_travel_guide_hudson.txt
time-to-make-a-mess/content/waterways_cartography_part_1/1808_proposed_canal_new_york.txt
time-to-make-a-mess/content/waterways_cartography_part_1/1862_canal_profiles_new_york.txt
time-to-make-a-mess/content/navigating_the_coasts/1859_frontiers_of_ocean.txt
time-to-make-a-mess/content/navigating_the_coasts/1891_algae_concentrations.txt
time-to-make-a-mess/content/navigating_the_coasts/1833_stage_and_steamboat_routes_ohio.txt
time-to-make-a-mess/content/navigating_the_coasts/1732_variations_of_the_compass.txt
time-to-make-a-mess/content/navigating_the_coasts/1732_coast_pilot_chart.txt
time-to-make-a-mess/content/navigating_the_coasts/1838_us_coast_survey_chart_new_haven.txt
time-to-make-a-mess/content/navigating_the_coasts/1934_us_coast_survey_chart_hawaii.txt
time-to-make-a-mess/content/navigating_the_coasts/1820_ships_and_navigation.txt
time-to-make-a-mess/content/navigating_the_coasts/1807_trade_winds.txt
time-to-make-a-mess/content/navigating_the_coasts/1877_austro_hungarian.txt
time-to-make-a-mess/content/european_maps_for_exploration/1612_virginia.txt
time-to-make-a-mess/content/european_maps_for_exploration/1541_nautical_chart_of_the_pacific_coast_of_mexico.txt
time-to-make-a-mess/content/european_maps_for_exploration/1703_new_voyages_in_northern_america.txt
time-to-make-a-mess/content/european_maps_for_exploration/1801_mackenzies_northern_voyages.txt
time-to-make-a-mess/content/european_maps_for_exploration/1688_globe_gore_of_eastern_north_america.txt
time-to-make-a-mess/content/european_maps_for_exploration/1598_florida_and_the_apalachee_lands.txt
time-to-make-a-mess/content/european_maps_for_exploration/1672_jesuit_map_of_lake_superior.txt
time-to-make-a-mess/content/european_maps_for_exploration/1761_russian_voyages_in_the_pacific_northwest.txt
time-to-make-a-mess/content/european_maps_for_exploration/1612_new_france.txt
time-to-make-a-mess/content/european_maps_for_exploration/1784_map_of_kentucky.txt
time-to-make-a-mess/content/waterways_cartography_part_2/1964_commodities.txt
time-to-make-a-mess/content/waterways_cartography_part_2/1894_water_transportation_routes_us.txt
time-to-make-a-mess/content/waterways_cartography_part_2/1908_freight_rates_inland.txt
time-to-make-a-mess/content/waterways_cartography_part_2/1931_buffalo_great_lakes.txt
time-to-make-a-mess/content/waterways_cartography_part_2/1949_intracoastal_waterway_nj.txt
time-to-make-a-mess/content/waterways_cartography_part_2/1970_nautical_charts_illinois.txt
time-to-make-a-mess/content/waterways_cartography_part_2/1904_canals_ohio.txt
time-to-make-a-mess/content/waterways_cartography_part_2/1936_us_lake_survey.txt
time-to-make-a-mess/content/waterways_cartography_part_2/1882_inland_canals.txt
time-to-make-a-mess/content/waterways_cartography_part_2/1898_water_routes_yukon.txt
